package com.micro.magupe.jdbc.db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSource;

public class PoolUtil {

	private static BasicDataSource ds;
	
	static{
		Properties p = new Properties();
		try {
			p.load(PoolUtil.class.getClassLoader().getResourceAsStream("db.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String driver = p.getProperty("driver");
		String url = p.getProperty("url");
		String user = p.getProperty("user");
		String pwd = p.getProperty("pwd");
		String initialSize = p.getProperty("init.size");
		String maxIdle = p.getProperty("max.idle");
		String minIdle = p.getProperty("min.idle");
		
		ds = new BasicDataSource();
		ds.setDriverClassName(driver);
		ds.setUrl(url);
		ds.setUsername(user);
		ds.setPassword(pwd);
		ds.setInitialSize(Integer.valueOf(initialSize));
		ds.setMaxIdle(Integer.valueOf(maxIdle));
		ds.setMinIdle(Integer.valueOf(minIdle));
		ds.setMaxActive(10);
	}
	
	public static Connection getConnection() throws SQLException{
		return ds.getConnection();
	}
	
	/**
	 * 该连接由链接池创建，实际上该连接的实现类也是由连接池重写了。他所提供的close方法，
	 * 不再是关闭链接，而是改为归还连接。
	 * @param conn
	 */
	public static void close(Connection conn){
		if(conn!=null){
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException("归还连接失败",e);
			}
		}
	}
}