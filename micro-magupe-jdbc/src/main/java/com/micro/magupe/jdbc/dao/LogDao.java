package com.micro.magupe.jdbc.dao;

import java.util.List;

import com.micro.magupe.jdbc.bean.Query;
import com.micro.magupe.jdbc.entity.Log;

public interface LogDao {

	public Log get(Long id);
	
	public int save(Log entity);
	
	public int update(Log entity);
	
	public int remove(String id);
	
	public int deleteByDelFlag(String id);
	
	public int batchRemove(String[] ids);
	
	List<Log> list(Log log);

	List<Log> listQuery(Query query);
	
	int count(Query query);
}
