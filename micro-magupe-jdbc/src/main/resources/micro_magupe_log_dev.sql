SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sys_log0`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log0`;
CREATE TABLE `sys_log0` (
  `id` bigint NOT NULL,
  `user_id` varchar(128) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(128) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(64) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(128) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` varchar(128) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `sys_log1`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log1`;
CREATE TABLE `sys_log1` (
  `id` bigint NOT NULL,
  `user_id` varchar(128) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(128) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(64) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(128) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` varchar(128) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `sys_log2`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log2`;
CREATE TABLE `sys_log2` (
  `id` bigint NOT NULL,
  `user_id` varchar(128) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(128) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(64) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(128) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` varchar(128) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `sys_log3`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log3`;
CREATE TABLE `sys_log3` (
  `id` bigint NOT NULL,
  `user_id` varchar(128) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(128) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(64) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(128) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` varchar(128) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `sys_log4`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log4`;
CREATE TABLE `sys_log4` (
  `id` bigint NOT NULL,
  `user_id` varchar(128) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(128) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(64) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(128) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` varchar(128) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `sys_log5`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log5`;
CREATE TABLE `sys_log5` (
  `id` bigint NOT NULL,
  `user_id` varchar(128) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(128) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(64) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(128) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` varchar(128) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `sys_log6`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log6`;
CREATE TABLE `sys_log6` (
  `id` bigint NOT NULL,
  `user_id` varchar(128) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(128) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(64) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(128) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` varchar(128) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `sys_log7`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log7`;
CREATE TABLE `sys_log7` (
  `id` bigint NOT NULL,
  `user_id` varchar(128) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(128) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(64) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(128) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` varchar(128) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `sys_log8`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log8`;
CREATE TABLE `sys_log8` (
  `id` bigint NOT NULL,
  `user_id` varchar(128) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(128) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(64) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(128) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` varchar(128) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `sys_log9`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log9`;
CREATE TABLE `sys_log9` (
  `id` bigint NOT NULL,
  `user_id` varchar(128) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(128) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(64) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(128) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` varchar(128) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
