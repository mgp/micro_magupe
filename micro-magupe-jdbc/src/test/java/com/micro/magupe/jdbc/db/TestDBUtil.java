package com.micro.magupe.jdbc.db;

import java.sql.SQLException;

import org.junit.Test;

public class TestDBUtil {

	@Test
    public void testSqlCrud() {
    	DBUtil.sqlCrud();
	}
	
	@Test
	public void testSqlCrudByMybatis() {
		DBUtil.sqlCrudByMybatis();
	}
	
	@Test
	public void testSqlCrudBySpringJdbc() {
		DBUtil.sqlCrudBySpringJdbc();
	}
	
	@Test
	public void testSqlCrudByPool() {
		DBUtil.sqlCrudByPool();
	}
	
	@Test
	public void testSqlCrudByShardingJdbc() throws SQLException {
		DBUtil.sqlCrudByShardingJdbc();
	}
}
