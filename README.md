# micro-magupe
## 一，介绍  
  该项目是为了学习spring cloud security oauth2而创建的，项目中用户登录使用的是oauth2的密码模式，项目简单，本人开发经验有限，若有错误的地方，还请指出纠正，谢谢
## 二，使用的spring cloud 组件及前端框架  
  * eureka  
  * zuul  
  * security oauth2  
  * vue
## 三，项目介绍  
  * micro-magupe-gateway（网关）  
    * cms，auth项目都有context-path，配置网关时需要添加strip-prefix: false  
    * oauth2在网关认证时，需要配置CustomRemoteTokenServices.ResourceServerTokenServices，项目中有定义的类，该类需要的oauth2信息需要在配置文件中添加  
      ```
      security:
        basic:
          enabled: false
        oauth2:
          client:
            access-token-uri: http://localhost:8888/microauth/oauth/token
            user-authorization-uri: http://localhost:8888/microauth/oauth/authorize
            client-id: client_micro_magupe_test
            client-secret: 880204
          resource:
            user-info-uri: http://localhost:8888/microauth/user
            token-info-uri: http://localhost:8888/microauth/oauth/check_token
            prefer-token-info: false 
      ```  
    * 为了不在前端登录时输入client-id和client-secret，在项目中创建了一个过滤器，这些信息会在过滤器中统一添加  
    * CustomOAuth2Authentication
      集成OAuth2Authentication类，该类主要是为了保存userId信息从认证服务获取的token认证信息中的userId信息，然后把userId通过过滤器放在request的参数中，让其他的微服务可以获取用户唯一标识信息。
  * micro-magupe-eureka（服务发现与注册）  
    代码较少  
  * micro-magupe-cms（内容管理）  
    * cms内容管理  
    * 验证cms的resourceId，定义了一个类CustomOAuth2AuthenticationManager，就是继承了OAuth2AuthenticationManager
  * micro-magupe-auth（oauth2认证与Role-Based Access Control）  
    * 该项目下有数据库文件信息及相关知识点信息
    * tokenStoren及ClientDetailsService都是使用的JDBC，相关数据库信息都有
    * 如果认证服务和用户权限信息不再同一个服务的话，认证服务需要通过feign组件去获取用户权限信息
    * 可以通过get方式获取token，该限制没有关闭
    * ResourceServerConfigurerAdapter需要在WebSecurityConfigurerAdapter之前执行，配置文件需要配置  
      ```
      security:
        oauth2:
          resource:
            filter-order: 3
      ```
  * micro-magupe-common（公共类）  
    * 公共类  
    * 有统一的VO对象，避免污染Entity对象  
    * com.micro.magupe.common.aspect.LogAspect
      该类是aop切面编程，其会拦截带有@Log注解的方法，在@Log中记录操作日志，从而实现日志的记录。在该类中还可以开启消息中间件服务
  * micro-magupe-ui(前端)
    * 该项目是vue项目
    * 可以在当前目录中的dist目录里运行http-server -p 8080命令启动前端服务，当前命令需要安装http-server组件及nodejs环境
    * 前端token传输都在header中
      ```
      Vue.http.headers.common['Authorization'] = "Bearer " + tokenMessage.access_token;
      ```
      ```
      $.ajax({
          headers: {
              Authorization: "Bearer " + tokenMessage.access_token
          },
      });
      ```
## 四，项目借鉴
  * https://github.com/almasaeed2010/AdminLTE
  * bootdo：http://www.bootdo.com/
  * jeesite：http://www.jeesite.com/
  
