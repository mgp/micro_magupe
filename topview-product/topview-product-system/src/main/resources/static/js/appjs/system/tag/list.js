var selectTagId = "0";
$(document).ready(function () {
	treeReLoad();
});

function add() {
    layer.open({
        type : 2,
        title : '添加',
        offset: "30px",
        maxmin : true,
        shadeClose : false, // 点击遮罩关闭层
        area : [ '400px', '300px' ],
        content : ctx + 'a/tag/add'
    });
}

function edit(object){
    layer.open({
        type : 2,
        title : '编辑',
        offset: "30px",
        maxmin : true,
        shadeClose : false,
        area : [ '400px', '300px' ],
        content : ctx + 'a/tag/edit/' + $(object).parent().parent().attr("id")
    });
}

function remove(object){
	$.post(ctx + 'a/tag/remove', {id: $(object).parent().parent().attr('id')}, function(data){
		if(data.result == "success"){
			$(object).parent().parent().remove();
			treeReLoad();
		} else {
			layer.alert("操作失败");
		}
	}); 
}

function treeReLoad(){
	$.ajax({
		type : "POST",
		url : ctx + 'a/tag/tree',
		success : function(tree) {
			if(tree.length > 0){
				selectTagId = tree[0].id;
			}
			
			$('#jstree').jstree("destroy");
			load();
			
			$('#jstree').jstree({
				'core': {'data': tree},
				"plugins": ["search"]
			});
			$('#jstree').jstree().open_all();
			$("#jstree").on("ready.jstree", function(e, data){ // jstree加载完成时调用
				var html = "<span class='treeitem-ed'>";
				html = html + "<a href='javascript:;' onclick='edit(this);'><i class='fa fa-pencil-square' aria-hidden='true'></i></a>";
				html = html + "<a href='javascript:;' onclick='remove(this);'><i class='fa fa-trash' aria-hidden='true'></i></a>";
				html = html + "</span>";
				
				$("#jstree ul li").append(html);
				$("#jstree ul li a").first().addClass("jstree-clicked");
            });
			
			$('#jstree').on("changed.jstree", function(e, data) {
				if (data.selected == -1) {
					selectTagId = "0";
					var opt = {query : {id : selectTagId,}}
					$('#table-entity').bootstrapTable('refresh', opt);
				} else {
					selectTagId = data.selected[0];
					var opt = {query : {id : selectTagId,}}
					$('#table-entity').bootstrapTable('refresh', opt);
				}
			});	
		}
	});
}

/**
 * 标签对应的成员（成员包括用户和机构）
 * *****************************************************************/

function load(){
    $('#table-entity').bootstrapTable({
        method : 'get', // 服务器数据的请求方式 get or post
        url: ctx + 'a/tag/list', // 请求数据的ajax的url
        striped : true, // 设置为true会有隔行变色效果
        iconSize : 'outline',
        dataType : "json", // 服务器返回的数据类型
        pagination : true, // 设置为true会在底部显示分页条
        singleSelect : false, // 设置为true将禁止多选
        showColumns : false, // 是否显示内容下拉框（选择显示的列）
        sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者
        queryParams : function(params) {
            return {
                // 说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
                name : $('#searchName').val()
            };
        },
        columns: [
			{
                title: '名称',
                field: 'name',
                align: 'center',
                valign: 'center',
                width: '10%'
            },
            {
                title: '机构',
                field: 'officeName',
                align: 'center',
                valign: 'center',
                width : '10%',
            },
            {
                title: '状态',
                field: 'status',
                align: 'center',
                valign: 'center',
                width : '10%',
                formatter: function (item, index) {
                    if (item === '0') {
                        return '<span class="label label-primary">正常</span>';
                    }
                    if (item === '1') {
                        return '<span class="label label-success">不正常</span>';
                    }
                }
            },
            {
                title: '操作',
                field: 'memberId',
                align: 'center',
                valign: 'center',
                width : '10%',
                formatter: function (item, index) {
                    var d = '<button class="btn btn-warning btn-sm s_remove_h" title="删除" onclick="removeMember(\''
                        + item + '\')"><i class="fa fa-remove"></i></button> ';
                    return d;
                }
            }        
        ]
    });	
}

function addMember(){
    layer.open({
        type : 2,
        title : '添加',
        offset: "30px",
        maxmin : true,
        shadeClose : false, // 点击遮罩关闭层
        area : [ '600px', '500px' ],
        btn: ['确定'],
        yes: function(index, layero){
        	var iframeWindow = window["layui-layer-iframe" + index];
        	var array = iframeWindow.getMember();
        	var VO = {};
        	VO.id = selectTagId;
        	VO.memberUserIds = Array.from(array[0]).toString();
        	VO.memberOfficeIds = Array.from(array[1]).toString();
			$.post(ctx + 'a/tag/saveMember', VO, function(data){
				if(data.result == "success"){
		        	layer.close(index);
				} else {
		        	layer.close(index);
				}
				
				var opt = {query : {id : selectTagId,}}
				$('#table-entity').bootstrapTable('refresh', opt);
			}); 
        },
        content : ctx + 'a/tag/member'
    });
}

function removeMember(memberId){
	$.post(ctx + 'a/tag/removeMember', {id : selectTagId, memberId: memberId}, function(data){
		var opt = {query : {id : selectTagId,}}
		$('#table-entity').bootstrapTable('refresh', opt);
	}); 
}