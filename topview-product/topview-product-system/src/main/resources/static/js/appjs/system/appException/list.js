$(document).ready(function () {
	load();
});

function load(){
    $('#table-entity').bootstrapTable({
        method : 'get', // 服务器数据的请求方式 get or post
        url: ctx + 'a/appException/list', // 请求数据的ajax的url
        striped : true, // 设置为true会有隔行变色效果
        iconSize : 'outline',
        dataType : "json", // 服务器返回的数据类型
        pagination : true, // 设置为true会在底部显示分页条
        singleSelect : false, // 设置为true将禁止多选
        pageSize : 10, // 如果设置了分页，每页数据条数
        pageNumber : 1, // 如果设置了分布，首页页码
        showColumns : false, // 是否显示内容下拉框（选择显示的列）
        sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者
        queryParams : function(params) {
            return {
                // 说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
                limit : params.limit,
                offset : params.offset,
                loginName : $('#loginName').val(),
            };
        },
        columns: [
			{
                title: '异常code',
                field: 'code',
                align: 'center',
                valign: 'center',
                width: '2%'
            },
            {
                title: '异常信息',
                field: 'message',
                align: 'center',
                valign: 'center',
                width : '20%',
            },
            {
            	title: '应用版本',
            	field: 'version',
            	align: 'center',
            	valign: 'center',
            	width : '2%',
            },
            {
            	title: '设备信息',
            	field: 'phoneInfo',
            	align: 'center',
            	valign: 'center',
            	width : '20%',
            },
            {
            	title: '异常发生时间',
            	field: 'createExceptionTime',
            	align: 'center',
            	valign: 'center',
            	width : '10%',
            },
            {
                title: '操作',
                field: 'id',
                align: 'center',
                valign: 'center',
                width : '10%',
                formatter: function (item, index) {
                    var e = '<button class="btn btn-primary btn-sm s_edit_h" title="查看异常堆栈" onclick="edit(\'' + item + '\')">查看异常堆栈</button> ';
                    return e;
                }
            }        
        ]
    });	
}

function reLoad() {
	$('#table-entity').bootstrapTable('refresh');
}

function edit(id) {
    layer.open({
        type : 2,
        title : '异常堆栈',
        offset: "30px",
        maxmin : true,
        shadeClose : false,
        area : [ '800px', '540px' ],
        content : ctx + 'a/appException/edit/'+id
    });
}