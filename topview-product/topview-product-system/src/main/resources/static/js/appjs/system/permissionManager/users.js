var permissionUserIds = new Set();
var permissionUserNames = new Set();

$(document).ready(function () {
	if(parent.permissions.permissionUser != undefined){
		for(let key of parent.permissions.permissionUser){
			if(key.id != ""){
				permissionUserIds.add(key.id);
				permissionUserNames.add(key.name);
				createElement(key.id, key.name, key.type);
			}
		}
	}
	
	getListAll("");
	
	$.ajax({
		type : "POST",
		url : ctx + 'a/office/tree',
		success : function(tree) {
			$('#jstree2').jstree({'core' : {'data' : tree},"plugins" : [ "search" ]});
		}
	});
	
	$("#jstree2").on("loaded.jstree", function (event, data) {
		$('#jstree2').jstree('open_all');
	});
	
	$('#jstree2').on("changed.jstree", function(e, data) {
		getListAll(data.node.id);
	});	
});

function createElement(id, name, type){
	var img = "search-dep.png";
	if(type == "1") img = "tags.png";
	if(type == "2") img = "default_user.png";
	
	var html = 	"<div class='tag-permission-item'>" +
					"<span class='tag-permission-item-left'>" + 
						"<span class='tab-permission-item-head'>" + 
							"<img class='tag-permission-item-img' src='/topview/img/" + img + "'/>" + 
							"<span class='tag-permission-item-name'>" + name + "</span>" + 
						"</span>" + 
						"<span class='tab-permission-item-nbsp'>&nbsp;</span>" + 
					"</span>" + 
					"<span class='tag-permission-item-x' permissionId='"+id+"' permissionName='"+name+"' onclick='removePermission(this);'>×</span>" + 
				"</div>";
	$(".tag-permission").append(html);
}

function getListAll(officeId){
	$.ajax({
		type : "GET",
		url : ctx + 'a/appUser/listAll',
        data: {'officeId': officeId, 'userType': '0'},
		success : function(list) {
			$(".permission-right").children().remove();
			for(var i = 0; i < list.length; i ++){
				var bg = "transparent";
				var className = "";
				
				if(permissionUserIds.has(list[i].id)){
					bg = "rgb(236, 236, 236)";
					className = "ant-checkbox-checked";
				}
				
				var html = 	"<div class='icon_box_content'>" +
							"<div class='box_box icon_box' style='background-color: " + bg + ";' onclick='addPermission(this, "+ '"' + list[i].id + '"' + ", " + '"' + list[i].name + '"' + ", " + '"' + "user" + '"' + ");'>" +
								"<img src='/topview/img/default_user.png' class='box_vam icon_user' style='width: 28px; height: 28px; margin-left: 2px;'>" + 
								"<span class='box_vam icon_name' id='" + list[i].id + "'>" + list[i].name + "</span>" + 
								"<label class='box_vam icon_label ant-checkbox-wrapper'>" + 
									"<span class='ant-checkbox " + className + "'>" + 
										"<span class='ant-checkbox-inner'></span>" + 
										"<input type='checkbox' class='ant-checkbox-input' value='on'>" + 
									"</span>" + 
								"</label>" + 
							"</div>" + 
							"<div style='height: 1px; background-color: rgb(229, 229, 229);'></div>" + 
							"</div>";
				$(".permission-right").append(html);
			}
		}
	});
}

function addPermission(obj, id, name, type){
	if(!permissionUserIds.has(id)){
		permissionUserIds.add(id);
		permissionUserNames.add(name);
		$(obj).css("background-color", "rgb(236, 236, 236)");
		$(obj).find(".ant-checkbox").addClass("ant-checkbox-checked");
		createElement(id, name, "2");
	}
}

function removePermission(obj){
	var id = $(obj).attr("permissionId");
	var name = $(obj).attr("permissionName");
	permissionUserIds.delete(id);
	permissionUserNames.delete(name);
	
	$(".icon_box_content .icon_name").each(function(index, item){
		if($(this).attr("id") == id){
			$(this).next().find(".ant-checkbox").removeClass("ant-checkbox-checked");
			$(this).parent().css("background-color", "transparent");
		}
	});
	
	$(obj).parent().remove();
}

function getPermission(){
	var array = new Array();
	array.push(permissionUserIds);
	array.push(permissionUserNames);
	return array;
}