$(document).ready(function () {
    var icon = "<i class='fa fa-times-circle'></i> ";
    $("#signupForm").validate({
        rules : {
            name : {required : true,},
            enname : {required : true,},
        },
        messages : {
            name : {required : icon + "请输入名称"},
            enname : {required : icon + "请输入英文名称"},
        }
    });
    
	$.ajax({
		type : "GET",
		url : ctx + "a/menu/tree",
		success : function(tree) {
			$('#menuTree').jstree({
				'core' : {'data' : tree},
				"checkbox" : {"three_state" : true,},
				"plugins" : [ "wholerow", "checkbox" ]
			});
		}
	});
});

$.validator.setDefaults({
	submitHandler : function() {
		var ref = $('#menuTree').jstree(true); // 获得整个树
		var menuIds = ref.get_selected(); // 获得所有选中节点的，返回值为数组
		$("#menuTree").find(".jstree-undetermined").each(function(i, element) {
			menuIds.push($(element).closest('.jstree-node').attr("id"));
		});
		$('#menuIds').val(menuIds);
		
        $.ajax({
            cache : false,
            type : "POST",
            url : ctx + "a/role/save",
            data : $('#signupForm').serialize(),// 你的formid
            async : false,
            error : function(request) {parent.layer.alert("Connection error", {offset: "100px"});},
            success : function(data) {
                if (data.result == "success") {
                    parent.layer.msg("操作成功");
                    parent.reLoad();
                    var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                    parent.layer.close(index);
                } else {
                    parent.layer.alert("操作失败", {offset: "100px"})
                }
            }
        });
	}
});