$(document).ready(function () {
    var icon = "<i class='fa fa-times-circle'></i> ";
    $("#signupForm").validate({
        rules : {
        	entityName : {required : true,},
        	tableName : {required : true,},
        	packageName : {required : true,},
        	module : {required : true,},
        	permission : {required : true,},
        	filePrefix : {required : true,},
        	remark : {required : true,},
        },
        messages : {
        	entityName : {required : icon + "请输入实体名"},
        	tableName : {required : icon + "请输入表名"},
        	packageName : {required : icon + "请输入包名前缀"},
        	module : {required : icon + "请输入模块名"},
        	permission : {required : icon + "请输入权限标识"},
        	filePrefix : {required : icon + "请输入文件路径"},
        	remark : {required : icon + "请输入备注"},
        }
    });
});
$.validator.setDefaults({
	submitHandler : function() {
        $.ajax({
            cache : false,
            type : "POST",
            url : ctx + "a/gen/update",
            data : $('#signupForm').serialize(),// 你的formid
            async : false,
            error : function(request) {parent.layer.alert("Connection error", {offset: "100px"});},
            success : function(data) {
                if (data.result == "success") {
                    parent.layer.msg("操作成功");
                    parent.reLoad();
                    var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                    parent.layer.close(index);
                } else {
                    parent.layer.alert(data.msg, {offset: "100px"})
                }
            }
        });
	}
});