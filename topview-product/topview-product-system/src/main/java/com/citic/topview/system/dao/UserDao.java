package com.citic.topview.system.dao;

import java.util.List;
import java.util.Map;

import com.citic.topview.common.annotation.MyBatisDao;
import com.citic.topview.common.persistence.CrudDao;
import com.citic.topview.common.system.entity.User;
import com.citic.topview.common.system.vo.UserVO;

@MyBatisDao
public interface UserDao extends CrudDao<User>{

	List<UserVO> list(Map<String, Object> params);
	
	List<UserVO> listQuery(Map<String, Object> params);

	int count(Map<String, Object> params);
	
	User getByUser(Map<String, Object> params);
	
	List<UserVO> listByIN(Long[] ids);
}
