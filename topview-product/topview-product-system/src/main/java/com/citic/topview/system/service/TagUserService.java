package com.citic.topview.system.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.citic.topview.common.system.entity.TagUser;
import com.citic.topview.common.system.entity.User;
import com.citic.topview.common.system.vo.TagUserVO;
import com.citic.topview.system.dao.TagUserDao;

@Service
@Transactional(readOnly = true)
public class TagUserService {

	@Autowired
	TagUserDao tagUserDao;
	
	public TagUser get(Long id) {
		return tagUserDao.get(id);
	}
	
	@Transactional(readOnly = false)
	public int save(TagUser entity) {
		return tagUserDao.save(entity);
	}
	
	@Transactional(readOnly = false)
	public void batchSave(List<TagUser> list) {
		tagUserDao.batchSave(list);
	}
	
	@Transactional(readOnly = false)
	public int update(TagUser entity) {
		return tagUserDao.save(entity);
	}
	
	@Transactional(readOnly = false)
	public int remove(Long id) {
		return tagUserDao.remove(id);
	}
	
	public List<TagUserVO> list(Map<String, Object> params) {
		return tagUserDao.list(params);
	}

	public Set<String> getTagIdsByUser(User u) {
		Set<String> set = new HashSet<>();
		
		Map<String, Object> params = new HashMap<>();
		params.put("userId", u.getId());
		List<TagUserVO> list = list(params);
		
		if(!CollectionUtils.isEmpty(list)) {
			for (TagUserVO vo : list) {
				set.add(vo.getTagId() + "");
			}
		}
		
		return set;
	}
}
