package com.citic.topview.system.config;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.Filter;

import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.codec.Base64;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.session.mgt.eis.JavaUuidSessionIdGenerator;
import org.apache.shiro.session.mgt.eis.SessionIdGenerator;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.MethodInvokingFactoryBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.ErrorPage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import com.citic.topview.system.security.FormAuthenticationFilter;
import com.citic.topview.system.security.UserRealm;
import com.citic.topview.system.security.manager.RedisCacheManager;
import com.citic.topview.system.security.manager.RedisSessionDao;
import com.citic.topview.system.security.manager.RedisSessionFactory;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;

@Configuration
@ConditionalOnExpression("${system.shiro.enabled:true}")
public class ShiroConfigurationByRedis {
    
    @Autowired
    private RedisSessionDao sessionDao;
    
    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(@Qualifier("securityManager")SecurityManager securityManager) {
        Map<String, String> filterChainDefinitionMap = new LinkedHashMap<String, String>(); // 设置拦截器
        filterChainDefinitionMap.put("/static/**", "anon");
        filterChainDefinitionMap.put("/druid/**", "anon");
        filterChainDefinitionMap.put("/app/login/li", "anon");								
        filterChainDefinitionMap.put("/app/login/checkLogin", "anon");
        
        filterChainDefinitionMap.put("/a/validateCode", "anon");
        filterChainDefinitionMap.put("/a/login", "authc");
        filterChainDefinitionMap.put("/a/logout", "logout");
        filterChainDefinitionMap.put("/a/**", "user");		
        
        filterChainDefinitionMap.put("/app/exthost/list", "anon");			
        filterChainDefinitionMap.put("/app/exception/save", "anon");			
        filterChainDefinitionMap.put("/app/permission/getPermission", "anon");			
        filterChainDefinitionMap.put("/app/appUpdateManager/checkUpdate", "anon");			
        filterChainDefinitionMap.put("/app/appUpdateManager/updatePage", "anon");			
        
        Map<String, Filter> filters = new LinkedHashMap<String, Filter>(); 			// 设置拦截器
        filters.put("authc", new FormAuthenticationFilter());
        
    	ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager); 				// 必须设置 SecurityManager
        shiroFilterFactoryBean.setLoginUrl("/a/login"); 								// setLoginUrl 如果不设置值，默认会自动寻找Web工程根目录下的"/login.jsp"页面 或 "/login" 映射
        shiroFilterFactoryBean.setSuccessUrl("/a/index");
        shiroFilterFactoryBean.setUnauthorizedUrl("/a/unauthorized"); 				// 设置无权限时跳转的 url;
        shiroFilterFactoryBean.setFilters(filters);
        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        
        return shiroFilterFactoryBean;
    }
    
    @Bean
    public SecurityManager securityManager() {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRememberMeManager(rememberMeManager());
        securityManager.setCacheManager(redisCacheManager());
        securityManager.setSessionManager(sessionManager());
        securityManager.setRealm(userRealm());
        return securityManager;
    }
    
    @Bean
    public UserRealm userRealm() {
    	UserRealm userRealm = new UserRealm();
    	return userRealm;
    }

    /**
     * 开启shiro aop注解支持 使用代理方式所以需要开启代码支持
     * 一定要写入advisorAutoProxyCreator（）自动代理。不然AOP注解不会生效
     * 
     * @return
     */
    @Bean
    public DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator(){
        DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        advisorAutoProxyCreator.setProxyTargetClass(true);
        return advisorAutoProxyCreator;
    }
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager){
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }
    
	@Bean
    public static LifecycleBeanPostProcessor getLifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }
	
    // 静态注入，相当于调用SecurityUtils.setSecurityManager(securityManager)
    @Bean
    public MethodInvokingFactoryBean getMethodInvokingFactoryBean(){
        MethodInvokingFactoryBean factoryBean = new MethodInvokingFactoryBean();
        factoryBean.setStaticMethod("org.apache.shiro.SecurityUtils.setSecurityManager");
        factoryBean.setArguments(new Object[]{securityManager()});
        return factoryBean;
    }
    
	// thymeleaf页面使用shiro标签控制按钮是否显示
	@Bean
    public ShiroDialect shiroDialect() {
        return new ShiroDialect();
    }
	
	/**
	 * 访问没有权限的地址时，返回定制的页面，而不是springboot提供的错误页面（Whitelabel Error Page）
	 * templates目录下添加unauthorized.html，并且在LoginController中添加unauthorized的映射
	 */
	@Bean
    public SimpleMappingExceptionResolver simpleMappingExceptionResolver() {
        SimpleMappingExceptionResolver simpleMappingExceptionResolver=new SimpleMappingExceptionResolver();
        Properties properties=new Properties();
        // 这里的 /unauthorized 是页面，不是访问的路径
        properties.setProperty("org.apache.shiro.authz.UnauthorizedException","/unauthorized");
        properties.setProperty("org.apache.shiro.authz.UnauthenticatedException","/unauthorized");
        simpleMappingExceptionResolver.setExceptionMappings(properties);
        return simpleMappingExceptionResolver;
    }
	/**
	 * 访问不存在的地址时，返回定制的页面，而不是springboot提供的错误页面（Whitelabel Error Page）
	 * static目录下添加unauthorized.html，404.html，500.html
	 */
    @Bean
    public EmbeddedServletContainerCustomizer containerCustomizer() {
        return new EmbeddedServletContainerCustomizer() {
            @Override
            public void customize(ConfigurableEmbeddedServletContainer container) {
                ErrorPage error401Page = new ErrorPage(HttpStatus.UNAUTHORIZED, "/unauthorized.html");
                ErrorPage error404Page = new ErrorPage(HttpStatus.NOT_FOUND, "/404.html");
                ErrorPage error500Page = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/500.html");
                container.addErrorPages(error401Page, error404Page, error500Page);
            }
        };
    }
    
    /**
     * 记住我功能，主要就是利用cookie来实现
     * cookie名称与SERVLET容器名冲突，重新定义为sid或rememberMe（SimpleCookie name）
     */
    @Bean("rememberMeCookie")
    public SimpleCookie rememberMeCookie(){
        SimpleCookie simpleCookie = new SimpleCookie("rememberMe"); // 这个参数是cookie的名称，对应前端的checkbox的name = rememberMe
        simpleCookie.setHttpOnly(true);								// 设为true后，只能通过http访问，javascript无法访问
        simpleCookie.setPath("/");
        simpleCookie.setMaxAge(60 * 6);							// 记住我cookie生效时间30天 ,单位秒
        return simpleCookie;
    }
    @Bean
    public CookieRememberMeManager rememberMeManager(){
        CookieRememberMeManager cookieRememberMeManager = new CookieRememberMeManager();
        cookieRememberMeManager.setCookie(rememberMeCookie());
        cookieRememberMeManager.setCipherKey(Base64.decode("4AvVhmFLUs0KTA3Kprsdag=="));
        return cookieRememberMeManager;
    }
    
    @Bean
    public CacheManager redisCacheManager() {
        return new RedisCacheManager();
    }
    
    @Bean
    public SessionIdGenerator sessionIdGenerator() {
        return new JavaUuidSessionIdGenerator();
    }
    
    @Bean("sessionFactory")
    public RedisSessionFactory sessionFactory(){
    	RedisSessionFactory sessionFactory = new RedisSessionFactory();
        return sessionFactory;
    }
    
    @Bean("sessionManager")
    public SessionManager sessionManager() {
    	DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
        //配置session监听
        //Collection<SessionListener> listeners = new ArrayList<SessionListener>();
        //listeners.add(sessionListener());
        //sessionManager.setSessionListeners(listeners);
        sessionManager.setSessionIdCookie(sessionIdCookie());
        sessionManager.setSessionFactory(sessionFactory());
        sessionManager.setSessionDAO(sessionDao);
        sessionManager.setCacheManager(redisCacheManager());
        sessionManager.setDeleteInvalidSessions(true);				//是否开启删除无效的session对象  默认为true
        sessionManager.setSessionValidationSchedulerEnabled(true);	//是否开启定时调度器进行检测过期session 默认为true
        //设置session失效的扫描时间, 清理用户直接关闭浏览器造成的孤立会话 默认为 1个小时
        //设置该属性 就不需要设置 ExecutorServiceSessionValidationScheduler 底层也是默认自动调用ExecutorServiceSessionValidationScheduler
        //暂时设置为 5秒 用来测试
        sessionManager.setSessionValidationInterval(1000 * 60 * 2);
        sessionManager.setSessionIdUrlRewritingEnabled(false);		//取消url 后面的 JSESSIONID
        return sessionManager;
    }
    @Bean("sessionIdCookie")
    public SimpleCookie sessionIdCookie(){
        SimpleCookie simpleCookie = new SimpleCookie("sid");
        simpleCookie.setHttpOnly(true);
        simpleCookie.setPath("/");
        simpleCookie.setMaxAge(60 * 4);
        return simpleCookie;
    }
    
}
