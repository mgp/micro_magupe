package com.citic.topview.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.citic.topview.common.system.entity.RoleMenu;
import com.citic.topview.common.system.vo.RoleMenuVO;
import com.citic.topview.system.dao.RoleMenuDao;

@Service
@Transactional(readOnly = true)
public class RoleMenuService {

	@Autowired
	RoleMenuDao roleMenuDao;
	
	public RoleMenu get(Long id) {
		return roleMenuDao.get(id);
	}
	
	@Transactional(readOnly = false)
	public int save(RoleMenu entity) {
		return roleMenuDao.save(entity);
	}
	
	@Transactional(readOnly = false)
	public void batchSave(List<RoleMenu> list) {
		roleMenuDao.batchSave(list);
	}
	
	@Transactional(readOnly = false)
	public int update(RoleMenu entity) {
		return roleMenuDao.update(entity);
	}
	
	@Transactional(readOnly = false)
	public int remove(Long id) {
		return roleMenuDao.remove(id);
	}
	
	@Transactional(readOnly = false)
	public int removeByRoleId(Long roleId) {
		return roleMenuDao.removeByRoleId(roleId);
	}
	
	@Transactional(readOnly = false)
	public int removeByMenuId(Long menuId) {
		return roleMenuDao.removeByMenuId(menuId);
	}
	
	public List<RoleMenuVO> list(Map<String, Object> params) {
		return roleMenuDao.list(params);
	}
	
	public List<Long> listMenuIdByRoleId(Long roleId){
		return roleMenuDao.listMenuIdByRoleId(roleId);
	}
}
