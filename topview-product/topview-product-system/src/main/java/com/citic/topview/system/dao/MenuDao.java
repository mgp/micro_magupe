package com.citic.topview.system.dao;

import java.util.List;
import java.util.Map;

import com.citic.topview.common.annotation.MyBatisDao;
import com.citic.topview.common.persistence.CrudDao;
import com.citic.topview.common.system.entity.Menu;
import com.citic.topview.common.system.vo.MenuVO;

@MyBatisDao
public interface MenuDao extends CrudDao<Menu>{

	List<MenuVO> list(Map<String, Object> params);
	
	List<MenuVO> listQuery(Map<String, Object> params);

	int count(Map<String, Object> params);

	List<MenuVO> listByRoleIds(List<Long> ids);
}
