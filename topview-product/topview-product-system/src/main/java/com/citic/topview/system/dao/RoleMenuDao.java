package com.citic.topview.system.dao;

import java.util.List;
import java.util.Map;

import com.citic.topview.common.annotation.MyBatisDao;
import com.citic.topview.common.system.entity.RoleMenu;
import com.citic.topview.common.system.vo.RoleMenuVO;

@MyBatisDao
public interface RoleMenuDao {
	
	public RoleMenu get(Long id);
	
	public int save(RoleMenu entity);
	
	public int batchSave(List<RoleMenu> list);
	
	public int update(RoleMenu entity);
	
	public int remove(Long id);
	
	public int removeByMenuId(Long menuId);
	
	public int removeByRoleId(Long roleId);
	
	public List<RoleMenuVO> list(Map<String, Object> params);

	public List<Long> listMenuIdByRoleId(Long roleId);

}