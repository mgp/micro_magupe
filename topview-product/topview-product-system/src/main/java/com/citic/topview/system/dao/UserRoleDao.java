package com.citic.topview.system.dao;

import java.util.List;
import java.util.Map;

import com.citic.topview.common.annotation.MyBatisDao;
import com.citic.topview.common.system.entity.UserRole;
import com.citic.topview.common.system.vo.UserRoleVO;

@MyBatisDao
public interface UserRoleDao {

	public UserRole get(Long id);
	
	public int save(UserRole entity);
	
	public int batchSave(List<UserRole> list);
	
	public int update(UserRole entity);
	
	public int remove(Long id);
	
	public int removeByRoleId(Long roleId);
	
	public int removeByUserId(Long id);
	
	public List<UserRoleVO> list(Map<String, Object> params);
	
	public List<Long> listRoleId(Long userId);
}