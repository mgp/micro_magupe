package com.citic.topview.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.citic.topview.common.system.entity.UserRole;
import com.citic.topview.common.system.vo.UserRoleVO;
import com.citic.topview.system.dao.UserRoleDao;

@Service
@Transactional(readOnly = true)
public class UserRoleService {

	@Autowired
	UserRoleDao userRoleDao;
	
	public UserRole get(Long id) {
		return userRoleDao.get(id);
	}
	
	@Transactional(readOnly = false)
	public int save(UserRole entity) {
		return userRoleDao.save(entity);
	}
	
	@Transactional(readOnly = false)
	public void batchSave(List<UserRole> list) {
		userRoleDao.batchSave(list);
	}
	
	@Transactional(readOnly = false)
	public int update(UserRole entity) {
		return userRoleDao.save(entity);
	}
	
	@Transactional(readOnly = false)
	public int remove(Long id) {
		return userRoleDao.remove(id);
	}
	
	@Transactional(readOnly = false)
	public int removeByRoleId(Long roleId) {
		return userRoleDao.removeByRoleId(roleId);
	}
	
	@Transactional(readOnly = false)
	public int removeByUserId(Long id) {
		return userRoleDao.removeByUserId(id);
	}
	
	public List<UserRoleVO> list(Map<String, Object> params) {
		return userRoleDao.list(params);
	}
	
	public List<Long> listRoleId(Long userId){
		return userRoleDao.listRoleId(userId);
	}
}
