package com.citic.topview.system.gen;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;

import com.alibaba.druid.pool.DruidDataSource;
import com.citic.topview.common.util.StringUtil;

public final class Generate {

	private final String entityName; 		//类名
	private final String packageName;		//包名
	private final String filePrefix;		//存储文件的路径前缀
	private final String tableName;			//表明
	private final String module;			//模块名
	private final String remark;			//汉字表名
	private final String[] permission;		//权限标识
	private final DataSource dataSource;	//数据源
	
	private final List<String> fields = new ArrayList<String>();		// 字段集合
	private final List<String> types = new ArrayList<String>();			// 字段类型集合
	private final List<String> comments = new ArrayList<String>();		// 字段备注集合
	private List<String[]> columns = new ArrayList<String[]>();			// 字段集合
	
	public Generate(String entityName, String packageName, String filePrefix, DruidDataSource dataSource, String tableName, String module, String remark, String permission) {
		this.entityName = entityName;
		this.packageName = packageName;
		this.filePrefix = filePrefix;
		this.tableName = tableName;
		this.dataSource = dataSource;
		this.module = module;
		this.remark = remark.replace("表", "");
		this.permission = permission.split(",");
	}

	public void init() throws SQLException {
        PreparedStatement preparedStatement = null;
        String tableSql = "SELECT * FROM " + tableName;
        ResultSet rs = null;
        Connection connection = dataSource.getConnection();
        try {
        	preparedStatement = connection.prepareStatement(tableSql);
            rs = preparedStatement.executeQuery("show full columns from " + tableName);
            
            while (rs.next()) {
            	fields.add(rs.getString("Field"));
            	types.add(rs.getString("Type"));
            	comments.add(rs.getString("Comment"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                	connection.close();
                } catch (SQLException e) {
                    
                }
            }
        }
        
		List<String> fields = GenerateUtil.getFields(this.fields);
		List<String> types = GenerateUtil.getTypes(this.types);
		columns = GenerateUtil.getColumns(types, fields, this.fields, this.comments);
	}
	
	public void build() {
		Map<String, String> map = new HashMap<String, String>();
    	map.put("/templates/vm/template.vm", filePrefix + entityName + ".java");
    	map.put("/templates/vm/templateVO.vm", filePrefix + entityName + "VO.java");
    	map.put("/templates/vm/templateMapper.vm", filePrefix + entityName + "Mapper.xml");
    	map.put("/templates/vm/templateDao.vm", filePrefix + entityName + "Dao.java");
    	map.put("/templates/vm/templateService.vm", filePrefix + entityName + "Service.java");
    	map.put("/templates/vm/templateController.vm", filePrefix + entityName + "Controller.java");
    	map.put("/templates/vm/htmlAdd.vm", filePrefix + "html/add.html");
    	map.put("/templates/vm/htmlEdit.vm", filePrefix + "html/edit.html");
    	map.put("/templates/vm/htmlList.vm", filePrefix + "html/list.html");
    	map.put("/templates/vm/jsAdd.vm", filePrefix + "js/add.js");
    	map.put("/templates/vm/jsEdit.vm", filePrefix + "js/edit.js");
    	map.put("/templates/vm/jsList.vm", filePrefix + "js/list.js");
    	
        File file1 = new File(filePrefix + "html");
        if(!file1.exists()) {
        	file1.mkdirs();
        }
        File file2 = new File(filePrefix + "js");
        if(!file2.exists()) {
        	file2.mkdirs();
        }
    	
        VelocityContext context = new VelocityContext();
        context.put("entity", entityName);
        context.put("entity_", StringUtil.firstCharToLowerCase(entityName));
        context.put("packageName", packageName);
        context.put("tableName", tableName);
        context.put("columns", columns);
        context.put("module", module);
        context.put("remark", remark);
        context.put("permission", permission);
        
    	for(String templateFile : map.keySet()){
            Template t = GenerateUtil.getVelocityEngine().getTemplate(templateFile, "UTF-8");
            GenerateUtil.writeFile(context, t, map.get(templateFile));
    	}
	}

}
