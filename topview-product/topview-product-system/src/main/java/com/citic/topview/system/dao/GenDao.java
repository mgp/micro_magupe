package com.citic.topview.system.dao;

import java.util.List;
import java.util.Map;

import com.citic.topview.common.annotation.MyBatisDao;
import com.citic.topview.common.persistence.CrudDao;
import com.citic.topview.common.system.entity.Gen;
import com.citic.topview.common.system.vo.GenVO;

@MyBatisDao
public interface GenDao extends CrudDao<Gen>{

	List<GenVO> list(Map<String, Object> params);
	
	List<GenVO> listQuery(Map<String, Object> params);

	int count(Map<String, Object> params);
}
