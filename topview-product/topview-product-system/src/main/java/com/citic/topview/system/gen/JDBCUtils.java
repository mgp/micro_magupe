package com.citic.topview.system.gen;

import java.sql.Connection;
import java.sql.SQLException;

import com.alibaba.druid.pool.DruidDataSource;

public class JDBCUtils {

	private static DruidDataSource datasource = null;
	 
    static {
        try {
            datasource = new DruidDataSource();
            datasource.setUrl("jdbc:mysql://127.0.0.1:3306/topview_product_dev?useUnicode=true&characterEncoding=utf8&useSSL=false");
            datasource.setUsername("root");
            datasource.setPassword("880204");
            datasource.setDriverClassName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
	public static Connection getConnection() throws SQLException{
		return datasource.getConnection();
	}
	
	public static DruidDataSource getDataSource() throws SQLException{
		return datasource;
	}
	
	public static void close(Connection conn){
		try {
			if(conn!=null && !conn.isClosed()){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
