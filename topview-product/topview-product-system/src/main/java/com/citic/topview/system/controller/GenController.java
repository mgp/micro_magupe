package com.citic.topview.system.controller;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citic.topview.common.annotation.Log;
import com.citic.topview.common.bean.Query;
import com.citic.topview.common.system.entity.Gen;
import com.citic.topview.common.system.vo.GenVO;
import com.citic.topview.common.vo.AjaxResultVO;
import com.citic.topview.common.vo.PageUtils;
import com.citic.topview.common.vo.ResultVO;
import com.citic.topview.system.gen.Generate;
import com.citic.topview.system.gen.JDBCUtils;
import com.citic.topview.system.service.GenService;

@Controller
@RequestMapping("/a/gen")
public class GenController {

	@Autowired
	private GenService genService;
	
	@ModelAttribute("gen")
	public Gen get(@RequestParam(required=false) Long id) {
		Gen entity = null;
		if (id != null && id > 0L){
			entity = genService.get(id);
		}
		if (entity == null){
			entity = new Gen();
		}
		return entity;
	}
	
	@RequiresPermissions("gen:list")
	@RequestMapping("")
	public String gen(Model model) {
		return "system/gen/list";
	}
	
	@Log("代码生成配置列表")
	@RequiresPermissions("gen:list")
	@GetMapping("/list")
	@ResponseBody
	PageUtils list(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		List<GenVO> gens = genService.listQuery(query);
		int total = genService.count(query);
		
		PageUtils pageUtil = new PageUtils(gens, total);
		return pageUtil;
	}
	
	@Log("代码生成配置添加")
	@RequiresPermissions("gen:edit")
	@GetMapping("/add")
	public String add(Model model) {
		return "system/gen/add";
	}
	
	@Log("代码生成配置编辑")
	@RequiresPermissions("gen:edit")
	@GetMapping("/edit/{id}")
	public String edit(Model model, @PathVariable("id") Long id) {
		Gen gen = genService.get(id);
		model.addAttribute("entity", gen);
		return "system/gen/edit";
	}
	
	@Log("代码生成配置保存")
	@RequiresPermissions("gen:edit")
	@PostMapping("/save")
	@ResponseBody
	public AjaxResultVO<Object> save(@ModelAttribute("gen") Gen gen) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(genService.save(gen) > 0) {
			generate(gen);
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@Log("代码生成配置更新")
	@RequiresPermissions("gen:edit")
	@PostMapping("/update")
	@ResponseBody
	public AjaxResultVO<Object> update(@ModelAttribute("gen") Gen gen) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(genService.update(gen) > 0) {
			generate(gen);
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@Log("代码生成配置删除")
	@RequiresPermissions("gen:edit")
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResultVO<Object> remove(Long id) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(genService.remove(id) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	private void generate(Gen gen) {
    	String entityName = gen.getEntityName(); 								
    	String module = gen.getModule(); 								
        String packageName = gen.getPackageName();		
        String filePrefix = gen.getFilePrefix();					
        String tableName = gen.getTableName();					
        String remark = gen.getRemark();					
        String permission = gen.getPermission();					
    	Generate generate;
		try {
			generate = new Generate(entityName, packageName, filePrefix, JDBCUtils.getDataSource(), tableName, module, remark, permission);
	    	generate.init();
	    	generate.build();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
