package com.citic.topview.system.service;

import com.citic.topview.common.util.EncodesUtil;
import com.citic.topview.system.security.manager.Digests;

public class SystemService {

	public static final String HASH_ALGORITHM = "SHA-1";
	public static final int HASH_INTERATIONS = 1024;
	public static final int SALT_SIZE = 8;

	/**
	 * 生成安全的密码，生成随机的16位salt并经过1024次 sha-1 hash
	 */
	public static String entryptPassword(String plainPassword) {
		String plain = EncodesUtil.unescapeHtml(plainPassword);
		byte[] salt = Digests.generateSalt(SALT_SIZE);
		byte[] hashPassword = Digests.sha1(plain.getBytes(), salt, HASH_INTERATIONS);
		return EncodesUtil.encodeHex(salt) + EncodesUtil.encodeHex(hashPassword);
	}
	
	/**
	 * 生成安全的密码，生成随机的8位salt并经过512次 sha-1 hash
	 */
	public static String entryptPasswordByApp(String plainPassword) {
		String plain = EncodesUtil.unescapeHtml(plainPassword);
		byte[] salt = Digests.generateSalt(12);
		byte[] hashPassword = Digests.sha1(plain.getBytes(), salt, 512);
		return EncodesUtil.encodeHex(salt) + EncodesUtil.encodeHex(hashPassword);
	}
	
}
