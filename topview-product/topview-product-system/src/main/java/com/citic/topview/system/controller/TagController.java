package com.citic.topview.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citic.topview.common.annotation.Log;
import com.citic.topview.common.bean.Query;
import com.citic.topview.common.bean.Tree;
import com.citic.topview.common.system.entity.Tag;
import com.citic.topview.common.system.vo.TagVO;
import com.citic.topview.common.vo.AjaxResultVO;
import com.citic.topview.common.vo.PageUtils;
import com.citic.topview.common.vo.ResultVO;
import com.citic.topview.system.service.TagService;

@Controller
@RequestMapping("/a/tag")
public class TagController {

	@Autowired
	private TagService tagService;
	
	@ModelAttribute("tag")
	public Tag get(@RequestParam(required=false) Long id) {
		Tag entity = null;
		if (id != null && id > 0L){
			entity = tagService.get(id);
		}
		if (entity == null){
			entity = new Tag();
		}
		return entity;
	}
	
	@GetMapping("/listAll")
	@ResponseBody
	public AjaxResultVO<Object> listAll() {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		List<TagVO> tags = tagService.list(new HashMap<String, Object>());
		result.setBizVO(tags);
		result.setResult(ResultVO.SUCCESS);
		return result;
	}
	
	@PostMapping("/tree")
	@ResponseBody
	public List<Tree<TagVO>> tree() {
		List<Tree<TagVO>> trees = tagService.getTree();
		return trees;
	}
	
	@RequiresPermissions("tag:edit")
	@Log("标签添加")
	@GetMapping("/add")
	public String add(Model model) {
		return "system/tag/add";
	}
	
	@RequiresPermissions("tag:edit")
	@Log("标签编辑")
	@GetMapping("/edit/{id}")
	public String edit(Model model, @PathVariable("id") Long id) {
		Tag tag = tagService.get(id);
		model.addAttribute("entity", tag);
		return "system/tag/edit";
	}
	
	@RequiresPermissions("tag:edit")
	@Log("标签保存")
	@PostMapping("/save")
	@ResponseBody
	public AjaxResultVO<Object> save(@ModelAttribute("tag") Tag tag) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(tagService.save(tag) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@RequiresPermissions("tag:edit")
	@Log("标签更新")
	@PostMapping("/update")
	@ResponseBody
	public AjaxResultVO<Object> update(@ModelAttribute("tag") Tag tag) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(tagService.update(tag) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@RequiresPermissions("tag:edit")
	@Log("标签删除")
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResultVO<Object> remove(Long id) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(tagService.remove(id) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	/**
	 * 以下方法为
	 * 标签对应的成员（成员包括用户和机构）的相关操作
	 ******************************************************************************************************/
	@GetMapping("")
	public String list(Model model) {
		return "system/tag/list";
	}
	
	@GetMapping("/list")
	@ResponseBody
	public PageUtils list(@RequestParam Map<String, Object> params) {
		if(params.get("id") != null && StringUtils.isNotBlank(params.get("id").toString())) {
			Query query = new Query(params);
			List<TagVO> tags = tagService.listById(query);
			
			PageUtils pageUtil = new PageUtils(tags, tags.size());
			return pageUtil;
		} else {
			PageUtils pageUtil = new PageUtils(new ArrayList<>(), 0);
			return pageUtil;
		}
	}
	
	@GetMapping("/member")
	public String member(Model model) {
		return "system/tag/member";
	}
	
	@GetMapping("/listIds")
	@ResponseBody
	public AjaxResultVO<Object> listIds(Long id) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		List<TagVO> list = tagService.listIds(params);
		result.setBizVO(list);
		result.setResult(ResultVO.SUCCESS);
		return result;
	}
	
	@PostMapping("/saveMember")
	@ResponseBody
	public AjaxResultVO<Object> saveMember(TagVO tagVO) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(tagService.saveMember(tagVO) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@PostMapping("/removeMember")
	@ResponseBody
	public AjaxResultVO<Object> removeMember(TagVO tagVO) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(tagService.removeMember(tagVO) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
}
