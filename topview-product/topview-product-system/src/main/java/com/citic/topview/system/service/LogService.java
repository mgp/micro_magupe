package com.citic.topview.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.citic.topview.common.system.entity.Log;
import com.citic.topview.common.system.vo.LogVO;
import com.citic.topview.system.dao.LogDao;
import com.citic.topview.system.persistence.CrudService;

@Service
@Transactional(readOnly = true)
public class LogService extends CrudService<LogDao, Log>{

	public Log get(Long id) {
		return super.get(id);
	}

	@Transactional(readOnly = false)
	public int save(Log log) {
		return super.save(log);
	}

	@Transactional(readOnly = false)
	public int update(Log log) {
		return super.update(log);
	}

	@Transactional(readOnly = false)
	public int remove(Long id) {
		return super.remove(id);
	}

	@Transactional(readOnly = false)
	public int deleteByDelFlag(Long id) {
		return super.deleteByDelFlag(id);
	}

	@Transactional(readOnly = false)
	public int batchRemove(Long[] ids) {
		return super.batchRemove(ids);
	}
	
	public List<LogVO> list(Map<String, Object> params) {
		return dao.list(params);
	}
	
	public List<LogVO> listQuery(Map<String, Object> params) {
		return dao.listQuery(params);
	}
	
	public int count(Map<String, Object> params) {
		return dao.count(params);
	}
}
