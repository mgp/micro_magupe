package com.citic.topview.system.util;

import java.util.ArrayList;
import java.util.List;

import com.citic.topview.common.system.vo.MenuVO;

public class ListHelper {

	public static void processListToTree(MenuVO node, List<MenuVO> list) {
		List<MenuVO> children = new ArrayList<>();
		for (MenuVO vo : list) {
			if(node.getId().equals(vo.getParentId())) {
				processListToTree(vo, list);
				children.add(vo);
			}
		}
		node.setChildren(children);
	}
}
