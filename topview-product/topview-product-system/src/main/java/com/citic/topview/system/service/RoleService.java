package com.citic.topview.system.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.citic.topview.common.bean.Query;
import com.citic.topview.common.system.entity.Role;
import com.citic.topview.common.system.entity.RoleMenu;
import com.citic.topview.common.system.vo.RoleVO;
import com.citic.topview.common.util.IdGen;
import com.citic.topview.system.dao.RoleDao;
import com.citic.topview.system.dao.RoleMenuDao;
import com.citic.topview.system.dao.UserRoleDao;
import com.citic.topview.system.persistence.CrudService;

@Service
@Transactional(readOnly = true)
public class RoleService extends CrudService<RoleDao, Role>{

	@Autowired
	RoleMenuDao roleMenuDao;
	@Autowired
	UserRoleDao userRoleDao;
	
	public Role get(Long id) {
		return super.get(id);
	}

	@Transactional(readOnly = false)
	public int save(Role role) {
		return super.save(role);
	}

	@Transactional(readOnly = false)
	public int save(Role role, Long[] menuIds) {
		super.save(role);
		
		List<RoleMenu> list = new ArrayList<>();
		RoleMenu roleMenu = null;
		for (Long menuId : menuIds) {
			if(menuId > 0L) {
				roleMenu = new RoleMenu();
				roleMenu.setId(IdGen.snowFlakeId());
				roleMenu.setRoleId(role.getId());
				roleMenu.setMenuId(menuId);
				list.add(roleMenu);
			}
		}
		
		roleMenuDao.removeByRoleId(role.getId());
		if(list.size() > 0) {
			return roleMenuDao.batchSave(list);
		}
		
		return 0;
	}
	
	@Transactional(readOnly = false)
	public int update(Role role) {
		return super.update(role);
	}

	@Transactional(readOnly = false)
	public int update(Role role, Long[] menuIds) {
		super.update(role);
		
		List<RoleMenu> list = new ArrayList<>();
		RoleMenu roleMenu = null;
		for (Long menuId : menuIds) {
			if(menuId > 0L) {
				roleMenu = new RoleMenu();
				roleMenu.setId(IdGen.snowFlakeId());
				roleMenu.setRoleId(role.getId());
				roleMenu.setMenuId(menuId);
				list.add(roleMenu);
			}
		}
		
		roleMenuDao.removeByRoleId(role.getId());
		if(list.size() > 0) {
			return roleMenuDao.batchSave(list);
		}
		
		return 0;
	}
	
	@Transactional(readOnly = false)
	public int remove(Long id) {
		return super.remove(id);
	}

	@Transactional(readOnly = false)
	public int deleteByDelFlag(Long id) {
		return super.deleteByDelFlag(id);
	}

	@Transactional(readOnly = false)
	public int batchRemove(Long[] ids) {
		return super.batchRemove(ids);
	}
	
	public List<RoleVO> list(Map<String, Object> params) {
		return dao.list(params);
	}
	
	public List<RoleVO> listQuery(Map<String, Object> params) {
		return dao.listQuery(params);
	}
	
	public int count(Query query) {
		return dao.count(query);
	}

	public List<RoleVO> listByUserId(Long userId) {
		List<RoleVO> roles1 = dao.list(new HashMap<String, Object>());
		List<RoleVO> roles2 = dao.listByUserId(userId);
		if(!CollectionUtils.isEmpty(roles2)) {
			for (RoleVO roleVO1 : roles1) {
				roleVO1.setShow(false);
				for (RoleVO roleVO2 : roles2) {
					if(roleVO1.getName().equals(roleVO2.getName())) {
						roleVO1.setShow(true);
					} 
				}
			}
		}
		
		return roles1;
	}

	@Transactional(readOnly = false)
	public int removeAll(Long id) {
		userRoleDao.removeByRoleId(id);
		roleMenuDao.removeByRoleId(id);
		return super.remove(id);
	}
}
