package com.citic.topview.system.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citic.topview.common.annotation.Log;
import com.citic.topview.common.bean.Query;
import com.citic.topview.common.system.entity.AppException;
import com.citic.topview.common.system.vo.AppExceptionVO;
import com.citic.topview.common.vo.PageUtils;
import com.citic.topview.system.service.AppExceptionService;

@Controller
@RequestMapping("/a/appException")
public class AppExceptionController {

	@Autowired
	private AppExceptionService appExceptionService;
	
	@RequiresPermissions("appException:list")
	@RequestMapping("")
	public String appException(Model model) {
		return "system/appException/list";
	}
	
	@Log("app异常信息列表")
	@RequiresPermissions("appException:list")
	@GetMapping("/list")
	@ResponseBody
	PageUtils list(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		List<AppExceptionVO> appexceptions = appExceptionService.listQuery(query);
		
		for (AppExceptionVO appExceptionVO : appexceptions) {
			String text = appExceptionVO.getMessage();
			if(text.length() > 35) {
				appExceptionVO.setMessage(text.substring(0, 35) + "...");
			}
			String info = appExceptionVO.getPhoneInfo();
			if(info.length() > 35) {
				appExceptionVO.setPhoneInfo(info.substring(0, 35) + "...");
			}
		}
		
		int total = appExceptionService.count(query);
		
		PageUtils pageUtil = new PageUtils(appexceptions, total);
		return pageUtil;
	}
	
	@Log("app异常信息编辑")
	@RequiresPermissions("appException:edit")
	@GetMapping("/edit/{id}")
	public String edit(Model model, @PathVariable("id") Long id) {
		AppException appexception = appExceptionService.get(id);
		model.addAttribute("entity", appexception);
		return "system/appException/edit";
	}
}
