package com.citic.topview.system.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citic.topview.common.annotation.Log;
import com.citic.topview.common.bean.Query;
import com.citic.topview.common.system.entity.User;
import com.citic.topview.common.system.vo.UserVO;
import com.citic.topview.common.vo.AjaxResultVO;
import com.citic.topview.common.vo.PageUtils;
import com.citic.topview.common.vo.ResultVO;
import com.citic.topview.system.service.OfficeService;
import com.citic.topview.system.service.SystemService;
import com.citic.topview.system.service.UserService;

@Controller
@RequestMapping("/a/appUser")
public class AppUserController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private OfficeService officeService;
	
	@ModelAttribute("user")
	public User get(@RequestParam(required=false) Long id) {
		User entity = null;
		if (id != null && id > 0L){
			entity = userService.get(id);
		}
		if (entity == null){
			entity = new User();
		}
		return entity;
	}
	
	@RequiresPermissions("appUser:list")
	@RequestMapping("")
	public String user(Model model) {
		return "system/appUser/list";
	}
	
	@GetMapping("/listAll")
	@ResponseBody
	public List<UserVO> listAll(@RequestParam Map<String, Object> params) {
		List<UserVO> users = userService.listQuery(params);
		return users;
	}
	
	@Log("app用户列表")
	@RequiresPermissions("appUser:list")
	@GetMapping("/list")
	@ResponseBody
	PageUtils list(@RequestParam Map<String, Object> params) {
		params.put("userType", "0");
		Query query = new Query(params);
		List<UserVO> users = userService.listQuery(query);
		int total = userService.count(query);
		
		PageUtils pageUtil = new PageUtils(users, total);
		return pageUtil;
	}
	
	@Log("app用户添加")
	@RequiresPermissions("appUser:edit")
	@GetMapping("/add")
	public String add(Model model) {
		return "system/appUser/add";
	}
	
	@Log("app用户编辑")
	@RequiresPermissions("appUser:edit")
	@GetMapping("/edit/{id}")
	public String edit(Model model, @PathVariable("id") Long id) {
		User user = userService.get(id);
		
		if(user.getOfficeId() > 0L) {
			model.addAttribute("officeId", user.getOfficeId());
			model.addAttribute("officeName", officeService.get(user.getOfficeId()).getName());
		}
		
		model.addAttribute("entity", user);
		return "system/appUser/edit";
	}
	
	@Log("app用户保存")
	@RequiresPermissions("appUser:edit")
	@PostMapping("/save")
	@ResponseBody
	public AjaxResultVO<Object> save(@ModelAttribute("user") User user) {
		user.setPassword(SystemService.entryptPasswordByApp(user.getPassword()));
		
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(userService.save(user) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@Log("app用户更新")
	@RequiresPermissions("appUser:edit")
	@PostMapping("/update")
	@ResponseBody
	public AjaxResultVO<Object> update(@ModelAttribute("user") User user) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(userService.update(user) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@Log("app用户删除")
	@RequiresPermissions("appUser:edit")
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResultVO<Object> remove(Long id) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(userService.remove(id) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
}
