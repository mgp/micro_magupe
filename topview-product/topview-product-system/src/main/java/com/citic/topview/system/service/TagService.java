package com.citic.topview.system.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.citic.topview.common.bean.Query;
import com.citic.topview.common.bean.Tree;
import com.citic.topview.common.system.entity.Tag;
import com.citic.topview.common.system.entity.TagOffice;
import com.citic.topview.common.system.entity.TagUser;
import com.citic.topview.common.system.vo.TagVO;
import com.citic.topview.common.util.IdGen;
import com.citic.topview.system.dao.TagDao;
import com.citic.topview.system.dao.TagOfficeDao;
import com.citic.topview.system.dao.TagUserDao;
import com.citic.topview.system.persistence.CrudService;

@Service
@Transactional(readOnly = true)
public class TagService extends CrudService<TagDao, Tag>{

	@Autowired
	TagUserDao tagUserDao;
	
	@Autowired
	TagOfficeDao tagOfficeDao;
	
	public Tag get(Long id) {
		return super.get(id);
	}

	@Transactional(readOnly = false)
	public int save(Tag tag) {
		return super.save(tag);
	}

	@Transactional(readOnly = false)
	public int update(Tag tag) {
		return super.update(tag);
	}

	@Transactional(readOnly = false)
	public int remove(Long id) {
		return super.remove(id);
	}

	@Transactional(readOnly = false)
	public int deleteByDelFlag(Long id) {
		return super.deleteByDelFlag(id);
	}

	@Transactional(readOnly = false)
	public int batchRemove(Long[] ids) {
		return super.batchRemove(ids);
	}
	
	public List<TagVO> list(Map<String, Object> params) {
		return dao.list(params);
	}
	
	public List<TagVO> listQuery(Map<String, Object> params) {
		return dao.listQuery(params);
	}
	
	public int count(Map<String, Object> params) {
		return dao.count(params);
	}

	public List<Tree<TagVO>> getTree() {
		List<Tree<TagVO>> trees = new ArrayList<Tree<TagVO>>();
		List<TagVO> list = dao.list(new HashMap<String, Object>());
		
		for (TagVO tagVO : list) {
			Tree<TagVO> tree = new Tree<TagVO>();
			tree.setId(tagVO.getId());
			tree.setParentId(0L);
			tree.setIcon("tag");
			tree.setText(tagVO.getName());
			Map<String, Object> state = new HashMap<>(16);
			state.put("opened", true);
			tree.setState(state);
			trees.add(tree);
		}
		
		return trees;
	}

	public String listByIN(String[] ids) {
		Long[] ls = new Long[ids.length];
		for (int i = 0; i < ids.length; i++) {
			ls[i] = Long.valueOf(ids[i]);
		}
		
		List<TagVO> list = dao.listByIN(ls);
		StringBuffer buffer = new StringBuffer(StringUtils.EMPTY);
		for (TagVO tagVO : list) {
			buffer.append(tagVO.getName()).append(",");
		}
		
		return buffer.toString();
	}
	
	public List<TagVO> listIds(Map<String, Object> params) {
		return dao.listIds(params);
	}

	public List<TagVO> listById(Query query) {
		return dao.listById(query);
	}

	@Transactional(readOnly = false)
	public int saveMember(TagVO tagVO) {
		int value = 0;
		if(tagVO.getMemberUserIds().length > 0) {
			List<TagUser> list = new ArrayList<TagUser>();
			TagUser tagUser = null;
			for (String userId : tagVO.getMemberUserIds()) {
				tagUser = new TagUser();
				tagUser.setId(IdGen.snowFlakeId());
				tagUser.setTagId(tagVO.getId());
				tagUser.setUserId(Long.valueOf(userId));
				
				list.add(tagUser);
			}
			
			value = tagUserDao.removeByTagId(tagVO.getId());
			value = tagUserDao.batchSave(list);
		}
		
		if(tagVO.getMemberOfficeIds().length > 0) {
			List<TagOffice> list = new ArrayList<TagOffice>();
			TagOffice tagOffice = null;
			for (String officeId : tagVO.getMemberOfficeIds()) {
				tagOffice = new TagOffice();
				tagOffice.setId(IdGen.snowFlakeId());
				tagOffice.setTagId(tagVO.getId());
				tagOffice.setOfficeId(Long.valueOf(officeId));
				
				list.add(tagOffice);
			}
			
			value = tagOfficeDao.removeByTagId(tagVO.getId());
			value = tagOfficeDao.batchSave(list);
		}
		
		if(tagVO.getMemberUserIds().length == 0 && tagVO.getMemberOfficeIds().length == 0) {
			return 1;
		}
		
		return value;
	}

	@Transactional(readOnly = false)
	public int removeMember(TagVO tagVO) {
		int i = 0;
		int j = 0;
		i = tagOfficeDao.removeByVO(tagVO);
		j = tagUserDao.removeByVO(tagVO);
		
		return i + j;
	}
}
