package com.citic.topview.system.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/a/permissionManager")
public class PermissionManagerController {
	
	@GetMapping("/permission")
	public String member(Model model) {
		return "system/permissionManager/permission";
	}
	
	@GetMapping("/users")
	public String users(Model model) {
		return "system/permissionManager/users";
	}
}
