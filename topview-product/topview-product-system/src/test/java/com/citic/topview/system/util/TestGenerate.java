package com.citic.topview.system.util;

import java.sql.SQLException;

import org.junit.Test;

import com.citic.topview.system.gen.Generate;
import com.citic.topview.system.gen.JDBCUtils;

public class TestGenerate {

	@Test
	public void test() throws SQLException {
    	String entityName = "Gen"; 								
    	String module = "system"; 								
        String packageName = "com.citic.topview";		
        String filePrefix = "F:/workspace/";					
        String tableName = "sys_gen";					
        String remark = "代码生成配置表";					
        String permission = "gen:list,gen:edit";					
    	Generate generate = new Generate(entityName, packageName, filePrefix, JDBCUtils.getDataSource(), tableName, module, remark, permission);
    	generate.init();
    	generate.build();
	}
}
