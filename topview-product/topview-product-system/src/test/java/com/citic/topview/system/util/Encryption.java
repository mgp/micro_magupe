package com.citic.topview.system.util;

import org.apache.shiro.codec.Base64;
import org.apache.shiro.crypto.hash.Md5Hash;

public class Encryption {

	// 加密解密次数
	private static int number = 6;
	
	// shiro加密
	public static String encode(String souce){
		for(int i = 0 ; i < number ;i++){
			souce = encBase64(souce);
		}
		return souce;
	}
	
	public static String encBase64(String souce) {
		return Base64.encodeToString(souce.getBytes());
	}
	
	// 封装好的shiro解密
	public static String decode(String souce){
		for(int i = 0 ; i < number ;i++){
			souce = decBase64(souce);
		}
		return souce;
	}
	
	public static String decBase64(String souce){
		return Base64.decodeToString(souce);
	}

	public static String md5(String souce, String salt){
		return new Md5Hash(souce, salt).toString();
	}
	
	public static void main(String[] args) {
		String password="1234";
		System.out.println(Encryption.encBase64(password));
		System.out.println(Encryption.decBase64("MTIzNA=="));
		System.out.println(Encryption.md5(password, "java"));
	}

}
