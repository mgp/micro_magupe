package com.citic.topview.system.util;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class TestCollectionUtils {

	@Test
	public void test() {
		List<Long> list1 = new ArrayList<>();
		list1.add(123456L);
		list1.add(234567L);
		
		List<Long> list2 = new ArrayList<>();
		list2.add(234567L);
		list2.add(123456L);
		
		System.err.println(CollectionUtils.isEqual(list1, list2));
	}
}
