package com.citic.topview.system.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.citic.topview.TopviewProductSystemApplication;
import com.citic.topview.common.system.entity.Menu;
import com.citic.topview.common.system.vo.MenuVO;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes={TopviewProductSystemApplication.class, TestMenuService.class})
public class TestMenuService {

	@Autowired
	MenuService menuService;
	
	@Test
	public void testSave() {
		Menu menu = new Menu();
		menu.setName("系统管理");
		menu.setParentId(0L);
		menu.setType("0");
		menu.setIcon("fa fa-desktop fa-fw");
		menu.setSort(1);
		menu.setIsShow("0");
		
		
		System.err.println(menuService.save(menu));
	}
	
	@Test
	public void testList() {
		Map<String, Object> params = new HashMap<>();
		List<MenuVO> list = menuService.list(params);
		
		for (MenuVO menuVO : list) {
			System.err.println(menuVO);
		}
	}
}
