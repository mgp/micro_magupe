package com.citic.topview.system.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

public class StringTest {

	@Test
	public void test() {
		String id = "a4";
		
		Set<String> set = new HashSet<>();
		set.add(id);
		
		List<String> ids = new ArrayList<>();
		ids.add("a1,0");
		ids.add("a2,a1");
		ids.add("a3,a2");
		ids.add("a4,a3");
		ids.add("a5,0");
		
		processData(set, ids, id);
		
		System.err.println(set);
	}
	
	private void processData(Set<String> set, List<String> ids, String id) {
		boolean isTrue = false;
		for (String string : ids) {
			if(string.indexOf(id) == 0) {
				id = string.replace(id + ",", "");
				set.add(id);
				isTrue = true;
			}
		}
		
		if(isTrue) {
			processData(set, ids, id);
		}
	}

	@Test
	public void test2() {
		String id1 = "a4";
		String id2 = "a4,a3";
		
		if(id2.indexOf(id1) == 0) {
			id1 = id2.replace(id1 + ",", "");
			System.err.println(id1);
		}
		
	}
	
	@Test
	public void test3() {
		
	}
}
