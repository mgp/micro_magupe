package com.citic.topview.app.util;

import java.text.ParseException;
import java.util.Date;

import org.junit.Test;

import com.citic.topview.common.util.DateUtils;

public class TestDateUtil {

	@Test
	public void test() throws ParseException {
		String string = "2019-04-24 17:11:10";
		
		Date date = DateUtils.parseDate(string, "yyyy-MM-dd HH:mm:ss");
		System.err.println(date);
	}
}
