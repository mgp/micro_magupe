$(document).ready(function () {
    var icon = "<i class='fa fa-times-circle'></i> ";
    $("#signupForm").validate({
        rules : {
        	userId : {required : true,}
        },
        messages : {
        	userId : {required : icon + "请输入用户"}
        }
    });
});
$.validator.setDefaults({
	submitHandler : function() {
        $.ajax({
            cache : false,
            type : "POST",
            url : ctx + "a/avoidLogin/update",
            data : $('#signupForm').serialize(),// 你的formid
            async : false,
            error : function(request) {parent.layer.alert("Connection error", {offset: "100px"});},
            success : function(data) {
                if (data.result == "success") {
                    parent.layer.msg("操作成功");
                    parent.reLoad();
                    var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                    parent.layer.close(index);
                } else {
                    parent.layer.alert(data.msg, {offset: "100px"})
                }
            }
        });
	}
});