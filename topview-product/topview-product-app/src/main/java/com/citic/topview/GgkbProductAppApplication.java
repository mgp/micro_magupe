package com.citic.topview;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GgkbProductAppApplication {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(GgkbProductAppApplication.class, args);
	}
}

