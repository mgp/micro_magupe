package com.citic.topview.app.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citic.topview.common.bean.Query;
import com.citic.topview.common.annotation.Log;
import com.citic.topview.common.app.entity.AvoidLogin;
import com.citic.topview.common.app.vo.AvoidLoginVO;
import com.citic.topview.common.vo.AjaxResultVO;
import com.citic.topview.common.vo.PageUtils;
import com.citic.topview.common.vo.ResultVO;
import com.citic.topview.app.service.AvoidLoginService;

@Controller
@RequestMapping("/a/avoidLogin")
public class AvoidLoginController {

	@Autowired
	private AvoidLoginService avoidLoginService;
	
	@ModelAttribute("avoidLogin")
	public AvoidLogin get(@RequestParam(required=false) Long id) {
		AvoidLogin entity = null;
		if (id != null && id > 0L){
			entity = avoidLoginService.get(id);
		}
		if (entity == null){
			entity = new AvoidLogin();
		}
		return entity;
	}
	
	@RequiresPermissions("avoidLogin:list")
	@RequestMapping("")
	public String avoidLogin(Model model) {
		return "app/avoidLogin/list";
	}
	
	@Log("设备列表")
	@RequiresPermissions("avoidLogin:list")
	@GetMapping("/list")
	@ResponseBody
	PageUtils list(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		List<AvoidLoginVO> avoidLogins = avoidLoginService.listQuery(query);
		int total = avoidLoginService.count(query);
		
		PageUtils pageUtil = new PageUtils(avoidLogins, total);
		return pageUtil;
	}
	
	@RequiresPermissions("avoidLogin:edit")
	@Log("设备编辑")
	@GetMapping("/edit/{id}")
	public String edit(Model model, @PathVariable("id") Long id) {
		AvoidLogin avoidLogin = avoidLoginService.get(id);
		model.addAttribute("entity", avoidLogin);
		return "app/avoidLogin/edit";
	}
	
	@RequiresPermissions("avoidLogin:edit")
	@Log("设备保存")
	@PostMapping("/save")
	@ResponseBody
	public AjaxResultVO<Object> save(@ModelAttribute("avoidLogin") AvoidLogin avoidLogin) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(avoidLoginService.save(avoidLogin) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@RequiresPermissions("avoidLogin:edit")
	@Log("设备更新")
	@PostMapping("/update")
	@ResponseBody
	public AjaxResultVO<Object> update(@ModelAttribute("avoidLogin") AvoidLogin avoidLogin) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(avoidLoginService.update(avoidLogin) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@RequiresPermissions("avoidLogin:edit")
	@Log("设备删除")
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResultVO<Object> remove(Long id) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(avoidLoginService.remove(id) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
}
