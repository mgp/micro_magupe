package com.citic.topview.app.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citic.topview.common.bean.Query;
import com.citic.topview.common.app.entity.ExtHost;
import com.citic.topview.common.app.vo.ExtHostVO;
import com.citic.topview.common.vo.AjaxResultVO;
import com.citic.topview.common.vo.PageUtils;
import com.citic.topview.common.vo.ResultVO;
import com.citic.topview.app.service.ExtHostService;

@Controller
@RequestMapping("/a/extHost")
public class ExtHostController {

	@Autowired
	private ExtHostService extHostService;
	
	@ModelAttribute("extHost")
	public ExtHost get(@RequestParam(required=false) Long id) {
		ExtHost entity = null;
		if (id != null && id > 0L){
			entity = extHostService.get(id);
		}
		if (entity == null){
			entity = new ExtHost();
		}
		return entity;
	}
	
	@RequiresPermissions("extHost:list")
	@RequestMapping("")
	public String exthost(Model model) {
		return "app/extHost/list";
	}
	
	@GetMapping("/list")
	@RequiresPermissions("extHost:list")
	@ResponseBody
	PageUtils list(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		List<ExtHostVO> extHosts = extHostService.listQuery(query);
		int total = extHostService.count(query);
		
		PageUtils pageUtil = new PageUtils(extHosts, total);
		return pageUtil;
	}
	
	@RequiresPermissions("extHost:edit")
	@GetMapping("/add")
	public String add(Model model) {
		return "app/extHost/add";
	}
	
	@RequiresPermissions("extHost:edit")
	@GetMapping("/edit/{id}")
	public String edit(Model model, @PathVariable("id") Long id) {
		ExtHost extHost = extHostService.get(id);
		model.addAttribute("entity", extHost);
		return "app/extHost/edit";
	}
	
	@RequiresPermissions("extHost:edit")
	@PostMapping("/save")
	@ResponseBody
	public AjaxResultVO<Object> save(@ModelAttribute("extHost") ExtHost extHost) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(extHostService.save(extHost) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@RequiresPermissions("extHost:edit")
	@PostMapping("/update")
	@ResponseBody
	public AjaxResultVO<Object> update(@ModelAttribute("extHost") ExtHost extHost) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(extHostService.update(extHost) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@RequiresPermissions("extHost:edit")
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResultVO<Object> remove(Long id) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(extHostService.remove(id) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
}
