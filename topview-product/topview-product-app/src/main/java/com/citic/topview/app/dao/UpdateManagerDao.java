package com.citic.topview.app.dao;

import java.util.List;
import java.util.Map;

import com.citic.topview.common.annotation.MyBatisDao;
import com.citic.topview.common.persistence.CrudDao;
import com.citic.topview.common.app.entity.UpdateManager;
import com.citic.topview.common.app.vo.UpdateManagerVO;

@MyBatisDao
public interface UpdateManagerDao extends CrudDao<UpdateManager>{

	List<UpdateManagerVO> list(Map<String, Object> params);
	
	List<UpdateManagerVO> listQuery(Map<String, Object> params);

	int count(Map<String, Object> params);
}
