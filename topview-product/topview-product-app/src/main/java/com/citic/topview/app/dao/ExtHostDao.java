package com.citic.topview.app.dao;

import java.util.List;
import java.util.Map;

import com.citic.topview.common.annotation.MyBatisDao;
import com.citic.topview.common.persistence.CrudDao;
import com.citic.topview.common.app.entity.ExtHost;
import com.citic.topview.common.app.vo.ExtHostVO;

@MyBatisDao
public interface ExtHostDao extends CrudDao<ExtHost>{

	List<ExtHostVO> list(Map<String, Object> params);
	
	List<ExtHostVO> listQuery(Map<String, Object> params);

	int count(Map<String, Object> params);
}
