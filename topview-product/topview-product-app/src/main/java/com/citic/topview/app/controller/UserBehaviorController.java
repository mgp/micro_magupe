package com.citic.topview.app.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citic.topview.common.bean.Query;
import com.citic.topview.common.annotation.Log;
import com.citic.topview.common.app.entity.UserBehavior;
import com.citic.topview.common.app.vo.UserBehaviorVO;
import com.citic.topview.common.vo.AjaxResultVO;
import com.citic.topview.common.vo.PageUtils;
import com.citic.topview.common.vo.ResultVO;
import com.citic.topview.app.service.UserBehaviorService;

@Controller
@RequestMapping("/a/userBehavior")
public class UserBehaviorController {

	@Autowired
	private UserBehaviorService userBehaviorService;
	
	@ModelAttribute("userBehavior")
	public UserBehavior get(@RequestParam(required=false) Long id) {
		UserBehavior entity = null;
		if (id != null && id > 0L){
			entity = userBehaviorService.get(id);
		}
		if (entity == null){
			entity = new UserBehavior();
		}
		return entity;
	}
	
	@RequestMapping("")
	public String userBehavior(Model model) {
		return "app/userBehavior/list";
	}
	
	@RequiresPermissions("userBehavior:list")
	@GetMapping("/list")
	@Log("用户行为列表")
	@ResponseBody
	PageUtils list(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		List<UserBehaviorVO> userBehaviors = userBehaviorService.listQuery(query);
		int total = userBehaviorService.count(query);
		
		PageUtils pageUtil = new PageUtils(userBehaviors, total);
		return pageUtil;
	}
	
	@RequiresPermissions("userBehavior:edit")
	@Log("用户行为编辑")
	@GetMapping("/edit/{id}")
	public String edit(Model model, @PathVariable("id") Long id) {
		UserBehavior userBehavior = userBehaviorService.get(id);
		model.addAttribute("entity", userBehavior);
		return "app/userBehavior/edit";
	}
	
	@RequiresPermissions("userBehavior:edit")
	@Log("用户行为保存")
	@PostMapping("/save")
	@ResponseBody
	public AjaxResultVO<Object> save(@ModelAttribute("userBehavior") UserBehavior userBehavior) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(userBehaviorService.save(userBehavior) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@RequiresPermissions("userBehavior:edit")
	@Log("用户行为更新")
	@PostMapping("/update")
	@ResponseBody
	public AjaxResultVO<Object> update(@ModelAttribute("userBehavior") UserBehavior userBehavior) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(userBehaviorService.update(userBehavior) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@RequiresPermissions("userBehavior:edit")
	@Log("用户行为删除")
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResultVO<Object> remove(Long id) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(userBehaviorService.remove(id) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
}
