package com.citic.topview.app.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.citic.topview.app.dao.UpdateManagerDao;
import com.citic.topview.common.app.entity.UpdateManager;
import com.citic.topview.common.app.vo.UpdateManagerVO;
import com.citic.topview.system.persistence.CrudService;

@Service
@Transactional(readOnly = true)
public class UpdateManagerService extends CrudService<UpdateManagerDao, UpdateManager>{

	public UpdateManager get(Long id) {
		return super.get(id);
	}

	@Transactional(readOnly = false)
	public int save(UpdateManager updateManager) {
		return super.save(updateManager);
	}

	@Transactional(readOnly = false)
	public int update(UpdateManager updateManager) {
		return super.update(updateManager);
	}

	@Transactional(readOnly = false)
	public int remove(Long id) {
		return super.remove(id);
	}

	@Transactional(readOnly = false)
	public int deleteByDelFlag(Long id) {
		return super.deleteByDelFlag(id);
	}

	@Transactional(readOnly = false)
	public int batchRemove(Long[] ids) {
		return super.batchRemove(ids);
	}
	
	public List<UpdateManagerVO> list(Map<String, Object> params) {
		return dao.list(params);
	}
	
	public List<UpdateManagerVO> listQuery(Map<String, Object> params) {
		return dao.listQuery(params);
	}
	
	public int count(Map<String, Object> params) {
		return dao.count(params);
	}
}
