package com.citic.topview.app.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.citic.topview.app.dao.ExtHostDao;
import com.citic.topview.common.app.entity.ExtHost;
import com.citic.topview.common.app.vo.ExtHostVO;
import com.citic.topview.system.persistence.CrudService;

@Service
@Transactional(readOnly = true)
public class ExtHostService extends CrudService<ExtHostDao, ExtHost>{

	public ExtHost get(Long id) {
		return super.get(id);
	}

	@Transactional(readOnly = false)
	public int save(ExtHost extHost) {
		return super.save(extHost);
	}

	@Transactional(readOnly = false)
	public int update(ExtHost extHost) {
		return super.update(extHost);
	}

	@Transactional(readOnly = false)
	public int remove(Long id) {
		return super.remove(id);
	}

	@Transactional(readOnly = false)
	public int deleteByDelFlag(Long id) {
		return super.deleteByDelFlag(id);
	}

	@Transactional(readOnly = false)
	public int batchRemove(Long[] ids) {
		return super.batchRemove(ids);
	}
	
	public List<ExtHostVO> list(Map<String, Object> params) {
		return dao.list(params);
	}
	
	public List<ExtHostVO> listQuery(Map<String, Object> params) {
		return dao.listQuery(params);
	}
	
	public int count(Map<String, Object> params) {
		return dao.count(params);
	}
}
