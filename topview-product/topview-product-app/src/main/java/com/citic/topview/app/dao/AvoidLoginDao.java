package com.citic.topview.app.dao;

import java.util.List;
import java.util.Map;

import com.citic.topview.common.annotation.MyBatisDao;
import com.citic.topview.common.persistence.CrudDao;
import com.citic.topview.common.app.entity.AvoidLogin;
import com.citic.topview.common.app.vo.AvoidLoginVO;

@MyBatisDao
public interface AvoidLoginDao extends CrudDao<AvoidLogin>{

	List<AvoidLoginVO> list(Map<String, Object> params);
	
	List<AvoidLoginVO> listQuery(Map<String, Object> params);

	int count(Map<String, Object> params);
}
