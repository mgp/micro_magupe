package com.citic.topview.app.dao;

import java.util.List;
import java.util.Map;

import com.citic.topview.common.annotation.MyBatisDao;
import com.citic.topview.common.persistence.CrudDao;
import com.citic.topview.common.app.entity.UserBehaviorFilter;
import com.citic.topview.common.app.vo.UserBehaviorFilterVO;

@MyBatisDao
public interface UserBehaviorFilterDao extends CrudDao<UserBehaviorFilter>{

	List<UserBehaviorFilterVO> list(Map<String, Object> params);
	
	List<UserBehaviorFilterVO> listQuery(Map<String, Object> params);

	int count(Map<String, Object> params);

	int batchSave(List<UserBehaviorFilter> list);

	void removeAll();
}
