package com.citic.topview.app.security;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;

import com.citic.topview.common.util.EncodesUtil;
import com.citic.topview.system.security.manager.Digests;

public class CustomCredentialsMatcher extends SimpleCredentialsMatcher {

	@Override
	public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
		String accountCredentials = getCredentials(info).toString();
		byte[] salt = EncodesUtil.decodeHex(accountCredentials.substring(0, 16));
		String accountPassword = accountCredentials.substring(16);
		
		String tokenCredentials = new String((char[]) getCredentials(token));
		byte[] tokenHashPassword = Digests.sha1(tokenCredentials.getBytes(), salt, 512);
		tokenCredentials = EncodesUtil.encodeHex(tokenHashPassword);
		
        return accountPassword.equals(tokenCredentials);
	}
}
