package com.citic.topview.app.security;

public class AppUsernamePasswordToken extends org.apache.shiro.authc.UsernamePasswordToken{

	private static final long serialVersionUID = 1L;

	private String captcha;
	private boolean mobileLogin;
	private String agent;				//客户端
	
	public AppUsernamePasswordToken() {
		super();
	}
	
	public AppUsernamePasswordToken(String username, String password, boolean rememberMe, String host, boolean mobileLogin,String agent) {
		super(username, password, rememberMe, host);
		this.agent = agent;
		this.mobileLogin = mobileLogin;
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	public boolean isMobileLogin() {
		return mobileLogin;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public void setMobileLogin(boolean mobileLogin) {
		this.mobileLogin = mobileLogin;
	}
}
