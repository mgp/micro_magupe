package com.citic.topview.app.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import com.citic.topview.app.service.AvoidLoginService;
import com.citic.topview.common.app.entity.AvoidLogin;
import com.citic.topview.common.app.vo.AvoidLoginVO;
import com.citic.topview.common.system.entity.User;

public class LoginUtil {

	public static void saveAvoidLogin(AvoidLoginService avoidLoginService, User user, String username, String password, String uniqueId) {
		Map<String, Object> params = new HashMap<>();
		params.put("uniqueId", uniqueId);
		params.put("userId", String.valueOf(user.getId()));
		List<AvoidLoginVO> list = avoidLoginService.list(params);
		if(CollectionUtils.isEmpty(list)) {
			AvoidLogin avoidLogin = new AvoidLogin();
			avoidLogin.setUserId(user.getId());
			avoidLogin.setUniqueId(uniqueId);
			avoidLogin.setIsFingerLogin("1");
			avoidLogin.setIsAvoidLogin("1");
			avoidLogin.setIsBinding("1");
			avoidLoginService.save(avoidLogin);
		}
		
	}

}
