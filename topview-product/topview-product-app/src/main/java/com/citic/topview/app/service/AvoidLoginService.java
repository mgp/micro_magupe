package com.citic.topview.app.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.citic.topview.app.dao.AvoidLoginDao;
import com.citic.topview.common.app.entity.AvoidLogin;
import com.citic.topview.common.app.vo.AvoidLoginVO;
import com.citic.topview.system.persistence.CrudService;

@Service
@Transactional(readOnly = true)
public class AvoidLoginService extends CrudService<AvoidLoginDao, AvoidLogin>{

	public AvoidLogin get(Long id) {
		return super.get(id);
	}

	@Transactional(readOnly = false)
	public int save(AvoidLogin avoidLogin) {
		return super.save(avoidLogin);
	}

	@Transactional(readOnly = false)
	public int update(AvoidLogin avoidLogin) {
		return super.update(avoidLogin);
	}

	@Transactional(readOnly = false)
	public int remove(Long id) {
		return super.remove(id);
	}

	@Transactional(readOnly = false)
	public int deleteByDelFlag(Long id) {
		return super.deleteByDelFlag(id);
	}

	@Transactional(readOnly = false)
	public int batchRemove(Long[] ids) {
		return super.batchRemove(ids);
	}
	
	public List<AvoidLoginVO> list(Map<String, Object> params) {
		return dao.list(params);
	}
	
	public List<AvoidLoginVO> listQuery(Map<String, Object> params) {
		return dao.listQuery(params);
	}
	
	public int count(Map<String, Object> params) {
		return dao.count(params);
	}
}
