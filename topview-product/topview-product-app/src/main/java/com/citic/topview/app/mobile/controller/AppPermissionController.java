package com.citic.topview.app.mobile.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citic.topview.app.service.PermissionService;
import com.citic.topview.common.app.vo.PermissionVO;
import com.citic.topview.common.vo.AppResultVO;

@Controller
@RequestMapping("/app/permission")
public class AppPermissionController {

	@Autowired
	private PermissionService permissionService;
	
	@RequestMapping("getPermission")
	@ResponseBody
	public AppResultVO<Object> getMenu(@RequestBody PermissionVO permissionVO, HttpServletRequest request, HttpServletResponse response) {
		AppResultVO<Object> result = new AppResultVO<Object>();
		try {
			List<PermissionVO> permissions = new ArrayList<PermissionVO>();
			Map<String, Object> params = new HashMap<>();
			params.put("parentId", 0L);
			params.put("type", permissionVO.getType());
			List<PermissionVO> list = permissionService.list(params);
			for (PermissionVO vo : list) {
				vo.setShow(permissionService.hasPermission(vo));
				if (vo.getPermissionStatus().equals("0")) {
					vo.setRunning(false);
				}
				permissions.add(vo);
			}
			result.setResult(AppResultVO.SUCCESS);
			result.setBizVO(permissions);
		} catch (Exception e) {
			result.setResult(AppResultVO.ERROR);
			result.setMessage("获取菜单失败");
		}
		return result;
	}
}
