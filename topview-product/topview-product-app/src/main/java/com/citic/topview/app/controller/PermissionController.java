package com.citic.topview.app.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citic.topview.app.service.PermissionService;
import com.citic.topview.common.annotation.Log;
import com.citic.topview.common.app.entity.Permission;
import com.citic.topview.common.app.vo.PermissionVO;
import com.citic.topview.common.vo.AjaxResultVO;
import com.citic.topview.common.vo.ResultVO;
import com.citic.topview.system.service.OfficeService;
import com.citic.topview.system.service.TagService;
import com.citic.topview.system.service.UserService;

@Controller
@RequestMapping("/a/permission")
public class PermissionController {

	@Autowired
	private PermissionService permissionService;
	
	@Autowired
	private OfficeService officeService;
	
	@Autowired
	private TagService tagService;
	
	@Autowired
	private UserService userService;
	
	@ModelAttribute("permission")
	public Permission get(@RequestParam(required=false) Long id) {
		Permission entity = null;
		if (id != null && id > 0L){
			entity = permissionService.get(id);
		}
		if (entity == null){
			entity = new Permission();
		}
		return entity;
	}
	
	@RequiresPermissions("permission:list")
	@RequestMapping("")
	public String permission(Model model) {
		return "app/permission/list";
	}
	
	@RequiresPermissions("permission:list")
	@Log("app菜单列表")
	@RequestMapping("/list")
	@ResponseBody
	public List<PermissionVO> list(@RequestParam Map<String, Object> params) {
		List<PermissionVO> permissions = permissionService.listQuery(params);
		return permissions;
	}
	
	@RequiresPermissions("permission:edit")
	@Log("app菜单添加")
	@GetMapping("/add/{parentId}")
	public String add(Model model, @PathVariable("parentId") Long parentId) {
		model.addAttribute("parentId", parentId);
		if (parentId == 0) {
			model.addAttribute("parentName", "根目录");
		} else {
			model.addAttribute("parentName", permissionService.get(parentId).getName());
		}
		
		return "app/permission/add";
	}
	
	@RequiresPermissions("permission:edit")
	@Log("app菜单编辑")
	@GetMapping("/edit/{id}")
	public String edit(Model model, @PathVariable("id") Long id) {
		Permission permission = permissionService.get(id);
		model.addAttribute("parentId", String.valueOf(permission.getParentId()));
		if (permission.getParentId() == 0) {
			model.addAttribute("parentName", "根目录");
		} else {
			model.addAttribute("parentName", permission.getName());
		}
		model.addAttribute("entity", permission);
		
		if(permission.getPermissionOrg() != null && permission.getPermissionOrg().length() > 0) {
			model.addAttribute("permissionOrgNames", officeService.listByIN(permission.getPermissionOrg().split(",")));
		}
		if(permission.getPermissionTag() != null && permission.getPermissionTag().length() > 0) {
			model.addAttribute("permissionTagNames", tagService.listByIN(permission.getPermissionTag().split(",")));
		}
		if(permission.getPermissionUser() != null && permission.getPermissionUser().length() > 0) {
			model.addAttribute("permissionUserNames", userService.listByIN(permission.getPermissionUser().split(",")));
		}
		
		return "app/permission/edit";
	}
	
	@RequiresPermissions("permission:edit")
	@Log("app菜单保存")
	@PostMapping("/save")
	@ResponseBody
	public AjaxResultVO<Object> save(@ModelAttribute("permission") Permission permission) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		
		if(permissionService.save(permission) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@RequiresPermissions("permission:edit")
	@Log("app菜单更新")
	@PostMapping("/update")
	@ResponseBody
	public AjaxResultVO<Object> update(@ModelAttribute("permission") Permission permission) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(permissionService.update(permission) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@RequiresPermissions("permission:edit")
	@Log("app菜单删除")
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResultVO<Object> remove(Long id) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(permissionService.remove(id) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
}
