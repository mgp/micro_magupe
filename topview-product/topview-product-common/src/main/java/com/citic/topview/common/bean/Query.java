package com.citic.topview.common.bean;

import java.util.LinkedHashMap;
import java.util.Map;

public class Query extends LinkedHashMap<String, Object> {

	private static final long serialVersionUID = 1L;
	// 
	private int offset;
	// 每页条数
	private int limit;

	public Query(Map<String, Object> params) {
		this.putAll(params);
		String o = params.get("offset") != null ? params.get("offset").toString() : "0";
		String l = params.get("limit") != null ? params.get("limit").toString() : "10";
		// 分页参数
		this.offset = Integer.parseInt(o);
		this.limit = Integer.parseInt(l);
		this.put("offset", offset);
		this.put("page", offset / limit + 1);
		this.put("limit", limit);
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.put("offset", offset);
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}
}
