package com.citic.topview.common.system.vo;

import com.citic.topview.common.json.LongJsonDeserializer;
import com.citic.topview.common.json.LongJsonSerializer;
import com.citic.topview.common.persistence.DataEntity;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TagVO extends DataEntity<TagVO>{

	private static final long serialVersionUID = 8047768429373875446L;
	
	@JsonSerialize(using = LongJsonSerializer.class)
	@JsonDeserialize(using = LongJsonDeserializer.class)
	private Long memberId;
	
	private String memberType;
	private String officeName;
	private String status;
	
	private String name;					// 标签名称  
	private String memberUserIds[];					
	private String memberOfficeIds[];					

	public TagVO() {
		
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String[] getMemberUserIds() {
		return memberUserIds;
	}

	public void setMemberUserIds(String[] memberUserIds) {
		this.memberUserIds = memberUserIds;
	}

	public String[] getMemberOfficeIds() {
		return memberOfficeIds;
	}

	public void setMemberOfficeIds(String[] memberOfficeIds) {
		this.memberOfficeIds = memberOfficeIds;
	}

	public String getOfficeName() {
		return officeName;
	}

	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "TagVO{" +
				"id=" + id +
				", name='" + name + '\'' +
				'}';
	}
}
