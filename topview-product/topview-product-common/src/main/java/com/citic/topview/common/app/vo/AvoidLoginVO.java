package com.citic.topview.common.app.vo;

import com.citic.topview.common.json.LongJsonDeserializer;
import com.citic.topview.common.json.LongJsonSerializer;
import com.citic.topview.common.persistence.DataEntity;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class AvoidLoginVO extends DataEntity<AvoidLoginVO>{

	private static final long serialVersionUID = 8047768429373875446L;
	
	@JsonSerialize(using = LongJsonSerializer.class)
	@JsonDeserialize(using = LongJsonDeserializer.class)
	private Long userId;					// 用户ID  
	private String username;					// 设备ID  
	private String uniqueId;					// 设备ID  
	private String isAvoidLogin;					// 是否免登录  
	private String isFingerLogin;					// 是否开启指纹登录  
	private String isBinding;					// 绑定：1表示未绑定，0表示绑定   

	public AvoidLoginVO() {
		
	}
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}	
	public String getUniqueId() {
		return uniqueId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}	
	public String getIsAvoidLogin() {
		return isAvoidLogin;
	}

	public void setIsAvoidLogin(String isAvoidLogin) {
		this.isAvoidLogin = isAvoidLogin;
	}	
	public String getIsFingerLogin() {
		return isFingerLogin;
	}

	public void setIsFingerLogin(String isFingerLogin) {
		this.isFingerLogin = isFingerLogin;
	}	
	public String getIsBinding() {
		return isBinding;
	}

	public void setIsBinding(String isBinding) {
		this.isBinding = isBinding;
	}	

	@Override
	public String toString() {
		return "AvoidLogin{" +
				"id=" + id +
				", userId='" + userId + '\'' +
				", uniqueId='" + uniqueId + '\'' +
				", isAvoidLogin='" + isAvoidLogin + '\'' +
				", isFingerLogin='" + isFingerLogin + '\'' +
				", isBinding='" + isBinding + '\'' +
				'}';
	}
}
