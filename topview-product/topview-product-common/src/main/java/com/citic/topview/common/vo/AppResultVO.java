package com.citic.topview.common.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AppResultVO<T> extends ResultVO {

	private static final long serialVersionUID = -4026654833188890705L;
	public static final String ERROR = "error";
	public static final String SUCCESS = "success";

	private String redirect; 	// 手机应用要跳转的活动
	private T bizVO; 			// 业务查询结果VO

	public AppResultVO() {
		super();
		this.setResult(SUCCESS);
	}

	public T getBizVO() {
		return bizVO;
	}

	public void setBizVO(T bizVO) {
		this.bizVO = bizVO;
	}

	public String getRedirect() {
		return redirect;
	}

	public void setRedirect(String redirect) {
		this.redirect = redirect;
	}

	@JsonIgnore
	public boolean isSuccess() {
		return SUCCESS.equals(this.getResult());
	}

	@JsonIgnore
	public boolean isError() {
		return ERROR.equals(this.getResult());
	}
}
