package com.citic.topview.common.system.vo;

import com.citic.topview.common.json.LongJsonDeserializer;
import com.citic.topview.common.json.LongJsonSerializer;
import com.citic.topview.common.persistence.DataEntity;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OfficeVO extends DataEntity<OfficeVO>{

	private static final long serialVersionUID = 8047768429373875446L;
	
	@JsonSerialize(using = LongJsonSerializer.class)
	@JsonDeserialize(using = LongJsonDeserializer.class)
	private Long parentId;					// 父菜单ID，一级菜单为0  
	@JsonSerialize(using = LongJsonSerializer.class)
	@JsonDeserialize(using = LongJsonDeserializer.class)
	private Long companyId;					// 部门所属公司
	private String name;					// 名称  
	private String code;					// 构编码  
	private String type;					// 机构类型（1：公司；2：部门；3：小组）  
	private String grade;					// 机构等级（1：一级；2：二级；3：三级；4：四级）  
	private String address;					// 地址  
	private String phone;					// 电话  
	private String email;					// 邮件  
	private String useable;					// 是否可用  
	private Integer sort;					// 排序  
	@JsonSerialize(using = LongJsonSerializer.class)
	@JsonDeserialize(using = LongJsonDeserializer.class)
	private Long primaryPerson;					// 主负责人  
	@JsonSerialize(using = LongJsonSerializer.class)
	@JsonDeserialize(using = LongJsonDeserializer.class)
	private Long deputyPerson;					// 副负责人  

	public OfficeVO() {
		
	}
	
	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}	
	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}	
	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}	
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}	
	public String getUseable() {
		return useable;
	}

	public void setUseable(String useable) {
		this.useable = useable;
	}	
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}	
	public Long getPrimaryPerson() {
		return primaryPerson;
	}

	public void setPrimaryPerson(Long primaryPerson) {
		this.primaryPerson = primaryPerson;
	}	
	public Long getDeputyPerson() {
		return deputyPerson;
	}

	public void setDeputyPerson(Long deputyPerson) {
		this.deputyPerson = deputyPerson;
	}	

	@Override
	public String toString() {
		return "Office{" +
				"id=" + id +
				", parentId='" + parentId + '\'' +
				", companyId='" + companyId + '\'' +
				", name='" + name + '\'' +
				", code='" + code + '\'' +
				", type='" + type + '\'' +
				", grade='" + grade + '\'' +
				", address='" + address + '\'' +
				", phone='" + phone + '\'' +
				", email='" + email + '\'' +
				", useable='" + useable + '\'' +
				", sort='" + sort + '\'' +
				", primaryPerson='" + primaryPerson + '\'' +
				", deputyPerson='" + deputyPerson + '\'' +
				'}';
	}
}
