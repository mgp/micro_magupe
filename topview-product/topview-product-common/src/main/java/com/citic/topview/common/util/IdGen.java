package com.citic.topview.common.util;

import java.security.SecureRandom;
import java.util.UUID;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.IdGenerator;

@Service
@Lazy(false)
public class IdGen implements IdGenerator {

	private static SecureRandom random = new SecureRandom();
	private static SnowflakeIdWorker snowFlake = new SnowflakeIdWorker(0, 0);
	
	public static long snowFlakeId() {
		return snowFlake.nextId();
	}
	
	public static void main(String[] args) {
		System.err.println(snowFlakeId());
	}
	
	public static String uuid() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
	
	public static long randomLong() {
		return Math.abs(random.nextLong());
	}

	public static String randomBase62(int length) {
		byte[] randomBytes = new byte[length];
		random.nextBytes(randomBytes);
		return EncodesUtil.encodeBase62(randomBytes);
	}
	
	public String getNextId() {
		return IdGen.uuid();
	}

	@Override
	public UUID generateId() {
		return null;
	}
}
