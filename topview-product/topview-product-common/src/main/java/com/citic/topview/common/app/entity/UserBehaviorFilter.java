package com.citic.topview.common.app.entity;

import com.citic.topview.common.persistence.DataEntity;

public class UserBehaviorFilter extends DataEntity<UserBehaviorFilter>{

	private static final long serialVersionUID = 8047768429373875446L;
	
	private Long userId;					// 用户ID  
	private String username;					// 用户名  

	public UserBehaviorFilter() {
		
	}
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}	

	@Override
	public String toString() {
		return "UserBehaviorFilter{" +
				"id=" + id +
				", userId='" + userId + '\'' +
				", username='" + username + '\'' +
				'}';
	}
}
