package com.citic.topview.common.system.vo;

import java.util.List;

import com.citic.topview.common.json.LongJsonDeserializer;
import com.citic.topview.common.json.LongJsonSerializer;
import com.citic.topview.common.persistence.DataEntity;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MenuVO extends DataEntity<MenuVO>{

	private static final long serialVersionUID = 1L;
	
	@JsonSerialize(using = LongJsonSerializer.class)
	@JsonDeserialize(using = LongJsonDeserializer.class)
	private Long parentId;		// 父级菜单
	private String name; 		// 名称
	private String href; 		// 链接
	private String target; 		// 目标（ mainFrame、_blank、_self、_parent、_top）
	private String icon; 		// 图标
	private Integer sort; 		// 排序
	private String type; 		// 类型 0：目录 1：菜单 2：按钮
	private String isShow; 		// 是否在菜单中显示（1：显示；0：不显示）
	private String permission; 	// 权限标识
	private List<MenuVO> children;
	
	public MenuVO() {}
	
	public MenuVO(Long id, Long parentId, String name){
		this.id = id;
		this.parentId = parentId;
		this.name = name;
	}
	
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
	public String getIsShow() {
		return isShow;
	}
	public void setIsShow(String isShow) {
		this.isShow = isShow;
	}
	
	public String getPermission() {
		return permission;
	}
	public void setPermission(String permission) {
		this.permission = permission;
	}
	
	public List<MenuVO> getChildren() {
		return children;
	}
	public void setChildren(List<MenuVO> children) {
		this.children = children;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "Menu{" +
				"id=" + id +
				", parentId=" + parentId +
				", name='" + name + '\'' +
				", href='" + href + '\'' +
				", target='" + target + '\'' +
				", icon=" + icon +
				", sort='" + sort + '\'' +
				", isShow=" + isShow +
				", permission=" + permission +
				", type=" + type +
				'}';
	}
}
