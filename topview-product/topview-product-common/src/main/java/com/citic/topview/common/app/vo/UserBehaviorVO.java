package com.citic.topview.common.app.vo;

import java.util.Date;
import com.citic.topview.common.persistence.DataEntity;

public class UserBehaviorVO extends DataEntity<UserBehaviorVO>{

	private static final long serialVersionUID = 8047768429373875446L;
	
	private Long userId;					// 用户ID  
	private String username;					// 用户名  
	private String currentPage;					// 当前页面  
	private String action;					// 操作  
	private String gotoPage;					// 进入页面  
	private String module;					// 模块  
	private Date triggerTime;					// 触发时间  
	private String longitude;					// 经度  
	private String latitude;					// 纬度  
	private String address;					// 地址  
	private String version;					// 版本  
	private String platform;					// 平台  
	private String stayTime;					// 驻留时间  

	public UserBehaviorVO() {
		
	}
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}	
	public String getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}	
	public String getGotoPage() {
		return gotoPage;
	}

	public void setGotoPage(String gotoPage) {
		this.gotoPage = gotoPage;
	}	
	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}	
	public Date getTriggerTime() {
		return triggerTime;
	}

	public void setTriggerTime(Date triggerTime) {
		this.triggerTime = triggerTime;
	}	
	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}	
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}	
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}	
	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}	
	public String getStayTime() {
		return stayTime;
	}

	public void setStayTime(String stayTime) {
		this.stayTime = stayTime;
	}	

	@Override
	public String toString() {
		return "UserBehavior{" +
				"id=" + id +
				", userId='" + userId + '\'' +
				", username='" + username + '\'' +
				", currentPage='" + currentPage + '\'' +
				", action='" + action + '\'' +
				", gotoPage='" + gotoPage + '\'' +
				", module='" + module + '\'' +
				", triggerTime='" + triggerTime + '\'' +
				", longitude='" + longitude + '\'' +
				", latitude='" + latitude + '\'' +
				", address='" + address + '\'' +
				", version='" + version + '\'' +
				", platform='" + platform + '\'' +
				", stayTime='" + stayTime + '\'' +
				'}';
	}
}
