package com.citic.topview.common.app.vo;

import com.citic.topview.common.persistence.DataEntity;
import com.citic.topview.common.persistence.IPermissionEntity;

public class PermissionVO extends DataEntity<PermissionVO> implements IPermissionEntity{

	private static final long serialVersionUID = 8047768429373875446L;
	
	private Long parentId;					// PID  
	private String name;					// 菜单名  
	private String keyFun;					// 菜单的唯一标识  
	private String icon;					// 图标  
	private String type;					// 类型：0：tab，1：pageMenu，2：mainTab  
	private Integer sort;					// 排序  
	private String permissionStatus;					// 权限状态0：全部不可见，1：全部可见，2部分可见  
	private String permissionOrg;					// 机构权限  
	private String permissionTag;					// 标签权限  
	private String permissionUser;					// 用户权限  
	private boolean show = true; // 根据权限判断是否显示
	private boolean running = true;
	
	private String inputPermissionJson;	// org、tag、user权限初始化json数据 (非数据库字段)  
	
	public PermissionVO() {
		
	}
	
	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	
	public String getKeyFun() {
		return keyFun;
	}

	public void setKeyFun(String keyFun) {
		this.keyFun = keyFun;
	}	
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}	
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}	
	public String getPermissionStatus() {
		return permissionStatus;
	}

	public void setPermissionStatus(String permissionStatus) {
		this.permissionStatus = permissionStatus;
	}	
	public String getPermissionOrg() {
		return permissionOrg;
	}

	public void setPermissionOrg(String permissionOrg) {
		this.permissionOrg = permissionOrg;
	}	
	public String getPermissionTag() {
		return permissionTag;
	}

	public void setPermissionTag(String permissionTag) {
		this.permissionTag = permissionTag;
	}	
	public String getPermissionUser() {
		return permissionUser;
	}

	public void setPermissionUser(String permissionUser) {
		this.permissionUser = permissionUser;
	}	
	
	public boolean isShow() {
		return show;
	}

	public void setShow(boolean show) {
		this.show = show;
	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Permission{" +
				"id=" + id +
				", parentId='" + parentId + '\'' +
				", name='" + name + '\'' +
				", keyFun='" + keyFun + '\'' +
				", icon='" + icon + '\'' +
				", sort='" + sort + '\'' +
				", permissionStatus='" + permissionStatus + '\'' +
				", permissionOrg='" + permissionOrg + '\'' +
				", permissionTag='" + permissionTag + '\'' +
				", permissionUser='" + permissionUser + '\'' +
				'}';
	}

	@Override
	public String getInputPermissionJson() {
		return inputPermissionJson;
	}

	@Override
	public void setInputPermissionJson(String inputPermissionJson) {
		this.inputPermissionJson = inputPermissionJson;
	}
}
