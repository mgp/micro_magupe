package com.citic.topview.common.system.entity;

import com.citic.topview.common.persistence.DataEntity;

public class Tag extends DataEntity<Tag>{

	private static final long serialVersionUID = 8047768429373875446L;
	
	private String name;					// 标签名称  

	public Tag() {
		
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	

	@Override
	public String toString() {
		return "Tag{" +
				"id=" + id +
				", name='" + name + '\'' +
				'}';
	}
}
