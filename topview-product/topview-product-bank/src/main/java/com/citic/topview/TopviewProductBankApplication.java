package com.citic.topview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TopviewProductBankApplication {

	public static void main(String[] args) {
		SpringApplication.run(TopviewProductBankApplication.class, args);
	}
}
