package com.micro.magupe;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@EnableGlobalMethodSecurity(prePostEnabled = true) // 开启prePostEnabled，securedEnabled，jsr250Enabled的支持
@EnableEurekaClient
@ComponentScan(basePackages="com.micro.magupe")
@MapperScan("com.micro.magupe.cms.dao")
@SpringBootApplication
@EnableHystrix
public class MicroMagupeCmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroMagupeCmsApplication.class, args);
	}
}
