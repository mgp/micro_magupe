package com.micro.magupe.cms.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ExcelUtils {

	public static HSSFCellStyle getHSSFCellStyle(HSSFWorkbook wb){
		return wb.createCellStyle();
	}
	
	public static HSSFFont getHSSFFont(HSSFWorkbook wb){
		HSSFFont fontStyle = wb.createFont();
		fontStyle.setFontName("宋体");  
		fontStyle.setFontHeightInPoints((short)12);
		
		return fontStyle;
	}
	
	private static void forReadlog(HSSFRow row, String[] colunms, HSSFCellStyle cellStyle) {
		for (int i = 0; i < colunms.length; i++) {
			HSSFCell cell = row.createCell(i);
			cell.setCellStyle(cellStyle);
			cell.setCellValue(colunms[i]);
		}
	}
	
	public static void exportExcel(Map<String, Double> tfIdf, Map<String,Double>  idfMap, Map<String,Double> docuementTf) throws IOException {
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet("Sheet1");
		sheet.setDefaultRowHeightInPoints(12);//设置缺省列高
		sheet.setDefaultColumnWidth(20);//设置缺省列宽
		
		HSSFCellStyle cellStyle = getHSSFCellStyle(wb);
		cellStyle.setFont(getHSSFFont(wb));
		
		String[] strings = {"词语", "词频", "逆文档词频IDF", "逆文档频率"};
		HSSFRow row0 = sheet.createRow(0);
		forReadlog(row0, strings, cellStyle);
		
		sheet.setColumnWidth(2, 256*50);
		
		int i = 0;
        for (Entry<String, Double> item : tfIdf.entrySet()) {
			HSSFRow row = sheet.createRow(i + 1);
			String[] colunms = {item.getKey(), String.valueOf(docuementTf.get(item.getKey())), String.valueOf(item.getValue()), String.valueOf(idfMap.get(item.getKey()))};
			//String[] colunms = {item.getKey(), String.valueOf(item.getValue())};
			forReadlog(row, colunms, cellStyle);
			
			i ++;
		}
        
        //输出Excel文件
        File file = new File(System.getProperty("user.dir") + "/analysis-report.xlsx");
	    OutputStream output = new FileOutputStream(file);
	    wb.write(output);
	    output.flush();
	    output.close();
	}
	
	public static void exportExcel(List<Map<String, Double>> tfidfRes, Map<String,Double>  idfMap, Map<String,Double> docuementTf) throws IOException {
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet("Sheet1");
		sheet.setDefaultRowHeightInPoints(12);//设置缺省列高
		sheet.setDefaultColumnWidth(20);//设置缺省列宽
		
		HSSFCellStyle cellStyle = getHSSFCellStyle(wb);
		cellStyle.setFont(getHSSFFont(wb));
		
		String[] strings = {"词语", "词频", "逆文档词频IDF", "逆文档频率"};
		HSSFRow row0 = sheet.createRow(0);
		forReadlog(row0, strings, cellStyle);
		
		sheet.setColumnWidth(2, 256*50);
		
		int i = 0;
		for (Map<String, Double> map : tfidfRes) {
			for (Entry<String, Double> item : map.entrySet()) {
				HSSFRow row = sheet.createRow(i + 1);
				//String[] colunms = {item.getKey(), String.valueOf(docuementTf.get(item.getKey())), String.valueOf(item.getValue()), String.valueOf(idfMap.get(item.getKey()))};
				String[] colunms = {item.getKey(), String.valueOf(item.getValue())};
				forReadlog(row, colunms, cellStyle);
				
				i ++;
			}
		}
		
		//输出Excel文件
		File file = new File(System.getProperty("user.dir") + "/analysis-report.xlsx");
		OutputStream output = new FileOutputStream(file);
		wb.write(output);
		output.flush();
		output.close();
	}
}
