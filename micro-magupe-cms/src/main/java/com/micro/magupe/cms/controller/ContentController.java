package com.micro.magupe.cms.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.micro.magupe.cms.service.ContentService;
import com.micro.magupe.common.bean.Query;
import com.micro.magupe.common.util.PageUtils;
import com.micro.magupe.common.vo.ContentVO;

@RequestMapping("/content")
@Controller
public class ContentController {

	@Autowired
	ContentService contentService;
	
	@PreAuthorize("hasRole('ROLE_menu:list')")
	@ResponseBody
	@GetMapping("/list")
	public PageUtils list(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		List<ContentVO> bContentList = contentService.listQuery(query);
		int total = contentService.count(query);
		PageUtils pageUtils = new PageUtils(bContentList, total);
		return pageUtils;
	}
	
	@ResponseBody
	@GetMapping("/test")
	public String test() {
		return "test";
	}
}
