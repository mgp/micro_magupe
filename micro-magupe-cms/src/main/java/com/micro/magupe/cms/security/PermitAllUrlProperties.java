package com.micro.magupe.cms.security;

import java.util.ArrayList;
import java.util.List;

public class PermitAllUrlProperties {

    public String[] getPermitall() {
    	List<String> permitall = new ArrayList<String>();
    	permitall.add("/hystrix/**");
    	permitall.add("/webjars/**");
    	permitall.add("/actuator/hystrix.stream/**");
    	permitall.add("/proxy.stream/**");
        
        return permitall.toArray(new String[0]);
    }
}

