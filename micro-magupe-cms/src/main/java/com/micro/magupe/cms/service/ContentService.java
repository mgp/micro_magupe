package com.micro.magupe.cms.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.micro.magupe.cms.dao.ContentDao;
import com.micro.magupe.cms.entity.Content;
import com.micro.magupe.common.bean.Query;
import com.micro.magupe.common.mysql.CrudService;
import com.micro.magupe.common.vo.ContentVO;

@Service
@Transactional(readOnly = true)
public class ContentService extends CrudService<ContentDao, Content>{

	public Content get(String id) {
		return super.get(id);
	}

	@Transactional(readOnly = false)
	public int save(Content content) {
		return super.save(content);
	}

	@Transactional(readOnly = false)
	public int update(Content content) {
		return super.update(content);
	}

	@Transactional(readOnly = false)
	public int remove(String id) {
		return super.remove(id);
	}

	@Transactional(readOnly = false)
	public int deleteByDelFlag(String id) {
		return super.deleteByDelFlag(id);
	}

	@Transactional(readOnly = false)
	public int batchRemove(String[] ids) {
		return super.batchRemove(ids);
	}
	
	public List<ContentVO> list(Content content) {
		return dao.list(content);
	}

	public List<ContentVO> listQuery(Query query) {
		return dao.listQuery(query);
	}

	public int count(Query query) {
		return dao.count(query);
	}
}
