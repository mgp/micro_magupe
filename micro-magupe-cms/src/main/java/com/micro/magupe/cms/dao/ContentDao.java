package com.micro.magupe.cms.dao;

import java.util.List;

import com.micro.magupe.cms.entity.Content;
import com.micro.magupe.common.annotation.MyBatisDao;
import com.micro.magupe.common.bean.Query;
import com.micro.magupe.common.mysql.CrudDao;
import com.micro.magupe.common.vo.ContentVO;

@MyBatisDao
public interface ContentDao extends CrudDao<Content>{

	List<ContentVO> list(Content content);

	List<ContentVO> listQuery(Query query);
	
	int count(Query query);
}