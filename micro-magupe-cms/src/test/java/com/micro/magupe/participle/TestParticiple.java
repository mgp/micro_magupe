package com.micro.magupe.participle;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.micro.magupe.cms.participle.TfIdfParticiple;

public class TestParticiple {

	@Test
	public void testParticiple() throws IOException {
		List<String> list = new ArrayList<String>();
		list.add(readToString("beiying1.txt"));
		list.add(readToString("beiying2.txt"));
		list.add(readToString("beiying3.txt"));
		list.add(readToString("beiying4.txt"));
		list.add(readToString("beiying5.txt"));
		list.add(readToString("beiying6.txt"));
		list.add(readToString("beiying7.txt"));
		TfIdfParticiple tfIdf = new TfIdfParticiple(list);
		
		tfIdf.eval();
	}
	
	@Test
	public void testPath() {
		System.err.println(System.getProperty("user.dir"));
	}
	
	public String readToString(String filename) {  
		String path = TestParticiple.class.getClassLoader().getResource(filename).getPath();
        String encoding = "UTF-8";  
        File file = new File(path);  
        Long filelength = file.length();  
        byte[] filecontent = new byte[filelength.intValue()];  
        try {  
            FileInputStream in = new FileInputStream(file);  
            in.read(filecontent);  
            in.close();  
        } catch (FileNotFoundException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        
        try {  
            return new String(filecontent, encoding);  
        } catch (UnsupportedEncodingException e) {  
            e.printStackTrace();  
            return null;  
        }  
    } 
}
