package com.micro.magupe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class MicroMagupeEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroMagupeEurekaApplication.class, args);
	}
}
