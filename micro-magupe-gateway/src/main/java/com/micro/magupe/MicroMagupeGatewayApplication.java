package com.micro.magupe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import com.micro.magupe.gateway.config.ExcludeFromComponentScan;
import com.micro.magupe.gateway.filter.GatewayFilter;

@EnableGlobalMethodSecurity(prePostEnabled = true)
@Configuration
@EnableEurekaClient
@EnableZuulProxy
@EnableHystrix
@SpringBootApplication//(scanBasePackages={"com.netflix.client.config.IClientConfig"})
@ComponentScan(excludeFilters={@ComponentScan.Filter(type=FilterType.ANNOTATION, value=ExcludeFromComponentScan.class)})
public class MicroMagupeGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroMagupeGatewayApplication.class, args);
	}
	
	@Bean
	public GatewayFilter gatewayFilter(){
		return new GatewayFilter();
	}
	
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
    
    /**
     * 前后端分离 ajax跨域调用处理
     * @return
     */
    @Bean
    public CorsFilter corsFilter() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        final CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true); 	// 允许cookies跨域
        config.addAllowedOrigin("*");		// 允许向该服务器提交请求的URI，*表示全部允许。。这里尽量限制来源域，比如http://xxxx:8080, 以降低安全风险。。
        config.addAllowedHeader("*");		// 允许访问的头信息,*表示全部
        config.setMaxAge(18000L);			// 预检请求的缓存时间（秒），即在这个时间段里，对于相同的跨域请求不会再预检了
        config.addAllowedMethod("*");		// 允许提交请求的方法，*表示全部允许，也可以单独设置GET、PUT等
        
        config.addAllowedMethod("HEAD"); 
        config.addAllowedMethod("GET");
        config.addAllowedMethod("PUT");
        config.addAllowedMethod("POST"); 
        config.addAllowedMethod("DELETE");
        config.addAllowedMethod("PATCH");

        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }
}
