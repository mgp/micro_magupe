package com.micro.magupe.gateway.security;

import java.util.ArrayList;
import java.util.List;

public class PermitAllUrlProperties {

    public String[] getPermitall() {
    	List<String> permitall = new ArrayList<String>();
    	permitall.add("/microauth/**");
    	permitall.add("/microcms/**");
    	permitall.add("/zipkin/**");
    	permitall.add("/hystrix/**");
    	permitall.add("/webjars/**");
    	permitall.add("/actuator/hystrix.stream/**");
    	permitall.add("/proxy.stream/**");
    	permitall.add("/favicon.ico/**");
        
        return permitall.toArray(new String[0]);
    }
}

