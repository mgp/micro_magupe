package com.micro.magupe.gateway.filter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.micro.magupe.gateway.security.CustomOAuth2Authentication;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

public class GatewayFilter extends ZuulFilter{

	protected final Log logger = LogFactory.getLog(getClass());
	
	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();
    	HttpServletRequest request = ctx.getRequest();
    	
    	//String url = request.getServletPath();
    	//logger.info(url);
    	
    	// 前端获取token时，不需要输入client_id,client_secret等信息
    	if(request.getServletPath().equals("/microauth/oauth/token")) {
        	request.getParameterMap();
        	Map<String, List<String>> requestQueryParams = ctx.getRequestQueryParams();
        	
        	if (requestQueryParams == null) {
        		requestQueryParams = new HashMap<String, List<String>>();
        	}

        	ArrayList<String> array1 = new ArrayList<String>();
        	array1.add("client_micro_magupe_test");
        	ArrayList<String> array2 = new ArrayList<String>();
        	array2.add("880204");
        	ArrayList<String> array3 = new ArrayList<String>();
        	array3.add("password");
        	ArrayList<String> array4 = new ArrayList<String>();
        	array4.add("read");
        	requestQueryParams.put("client_id", array1);
        	requestQueryParams.put("client_secret", array2);
        	requestQueryParams.put("grant_type", array3);
        	requestQueryParams.put("scope", array4);
        	
        	ctx.setRequestQueryParams(requestQueryParams);
    	}
    	
    	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    	
    	// 验证token成功后，把username和userId放在request的参数中，供其他微服务使用
    	if(!authentication.getPrincipal().equals("anonymousUser")) {
    		CustomOAuth2Authentication customOAuth2Authentication = (CustomOAuth2Authentication) authentication;
    		Map<String, List<String>> requestQueryParams = new HashMap<String, List<String>>();
        	ArrayList<String> array1 = new ArrayList<String>();
        	array1.add(authentication.getPrincipal().toString());
        	requestQueryParams.put("username_", array1);
        	ArrayList<String> array2 = new ArrayList<String>();
        	array2.add(customOAuth2Authentication.getUserId());
        	requestQueryParams.put("userId_", array2);
        	ctx.setRequestQueryParams(requestQueryParams);
    		ctx.setRequest(request);
    	}
    	
		return null;
	}

	@Override
	public String filterType() {
		return "pre";
	}

	@Override
	public int filterOrder() {
		return 0;
	}

}
