package com.micro.magupe.gateway.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.BestAvailableRule;
import com.netflix.loadbalancer.IPing;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.PingUrl;
import com.netflix.loadbalancer.Server;
import com.netflix.loadbalancer.ServerList;
import com.netflix.loadbalancer.ServerListSubsetFilter;

@Configuration
@ExcludeFromComponentScan
public class RibbonConfiguration {

	protected final Log logger = LogFactory.getLog(getClass());
	
	@Bean
	public IRule ribbonRule() {
		return new BestAvailableRule();
	}

	@Bean
	public IPing ribbonPing() {
		return new PingUrl();
	}

	@Bean
	public ServerList<Server> ribbonServerList(IClientConfig config) {
		return new RibbonClientDefaultConfiguration.BazServiceList(config);
	}

	@Bean
	public ServerListSubsetFilter<Server> serverListFilter() {
		ServerListSubsetFilter<Server> filter = new ServerListSubsetFilter<Server>();
		return filter;
	}
}
