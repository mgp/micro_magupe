package com.micro.magupe.gateway.config;

import org.springframework.cloud.netflix.ribbon.RibbonClients;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.ConfigurationBasedServerList;

@RibbonClients(defaultConfiguration = RibbonConfiguration.class)
@ExcludeFromComponentScan
public class RibbonClientDefaultConfiguration {

	public static class BazServiceList extends ConfigurationBasedServerList {
		public BazServiceList(IClientConfig config) {
			super.initWithNiwsConfig(config);
		}
	}
}
