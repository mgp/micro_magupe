package com.micro.magupe.common.vo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.micro.magupe.common.mysql.DataEntity;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MenuVO extends DataEntity<MenuVO> {
	
	private static final long serialVersionUID = 1L;

	private MenuVO parent;

    private String parentId;;
    private String parentName;;
    private String menuName;

    private String url;

    private String perms;

    private Integer type;

    private String icon;

    private Integer orderNum;
    
    private String roleId; // 非数据库字段
    private String userId; // 非数据库字段
    private String username; // 非数据库字段
    private String name; // 非数据库字段
    private String password; // 非数据库字段

    public MenuVO() {
    	
	}
    
    public MenuVO(String id) {
    	this.id = id;
    }

	public MenuVO getParent() {
		return parent;
	}

	public void setParent(MenuVO parent) {
		this.parent = parent;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

    public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    public String getPerms() {
        return perms;
    }

    public void setPerms(String perms) {
        this.perms = perms == null ? null : perms.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon == null ? null : icon.trim();
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
    
    public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
    public String toString() {
        return "Menu{" +
                "id='" + id + '\'' +
                ", parentId='" + parent.getId() + '\'' +
                ", menuName='" + menuName + '\'' +
                ", url='" + url + '\'' +
                ", perms=" + perms +
                ", type='" + type + '\'' +
                ", icon='" + icon + '\'' +
                ", orderNum='" + orderNum + '\'' +
                ", createTime='" + createTime + '\'' +
                ", modifiedTime='" + modifiedTime + '\'' +
                '}';
    }
    
    public static class MenuBuilder {
        private MenuVO menu = new MenuVO();

        public MenuBuilder withId(String id) {
        	menu.setId(id);
            return this;
        }
        
        public MenuBuilder withParentId(String parentId) {
        	menu.setParent(new MenuVO(parentId));
        	return this;
        }
        
        public MenuBuilder withRoleId(String roleId) {
        	menu.setRoleId(roleId);
        	return this;
        }

        public MenuBuilder withMenuName(String menuName) {
        	menu.setMenuName(menuName);
            return this;
        }

        public MenuVO build() {
            return menu;
        }
    }
}