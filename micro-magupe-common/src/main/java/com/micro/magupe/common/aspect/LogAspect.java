package com.micro.magupe.common.aspect;

import java.lang.reflect.Method;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.micro.magupe.common.annotation.Log;
import com.micro.magupe.common.util.IPUtils;
import com.micro.magupe.common.util.IdGen;
import com.micro.magupe.common.util.JSONUtils;
import com.micro.magupe.common.vo.LogVO;

@Aspect
@Component
@Order(2)
public class LogAspect {

	public static HttpServletRequest getHttpServletRequest() {
		return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
	}
	
	public static final Logger logger = LoggerFactory.getLogger(ReadWriteAspect.class);
	
/*	@Autowired
	KafkaTemplate<String, String> kafkaTemplate;*/
	
	private Gson gson = new GsonBuilder().create();
	
	@Pointcut("@annotation(com.micro.magupe.common.annotation.Log)")
	public void logPointCut() {
		
	}

	@Around("logPointCut()")
	public Object around(ProceedingJoinPoint point) throws Throwable {
		long beginTime = System.currentTimeMillis();
		// 执行方法
		Object result = point.proceed();
		// 执行时长(毫秒)
		long time = System.currentTimeMillis() - beginTime;
		// 保存日志
		saveLog(point, time);
		return result;
	}

	private void saveLog(ProceedingJoinPoint joinPoint, long time) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();
		LogVO logVO = new LogVO();
		Log log = method.getAnnotation(Log.class);
		if (log != null) {
			// 注解上的描述
			logVO.setOperation(log.value());
		}
		// 请求的方法名
		String className = joinPoint.getTarget().getClass().getName();
		String methodName = signature.getName();
		logVO.setMethod(className + "." + methodName + "()");
		// 请求的参数
		Object[] args = joinPoint.getArgs();
		try {
			String params = JSONUtils.beanToJson(args[0]).substring(0, 4999);
			logVO.setParams(params);
		} catch (Exception e) {

		}

		// 获取request 设置IP地址
		HttpServletRequest request = LogAspect.getHttpServletRequest();
		logVO.setIp(IPUtils.getIpAddr(request));
		String username = "";
		String userId = "";
		try {
			username = request.getParameter("username_");
			userId = request.getParameter("userId_");
		} catch (Exception e) {
			e.printStackTrace();
		}
		logVO.setUsername(username.toString());
		logVO.setUserId(userId.toString());
		logVO.setTime((int) time);
		logVO.setId(IdGen.snowFlakeId());
		// 系统当前时间
		Date date = new Date();
		logVO.setCreateTime(date);
		
		System.out.println(gson.toJson(logVO));
		
		// 保存系统日志到kafka中
		//kafkaTemplate.send("magupe-visit-Log", gson.toJson(logVO));
	}
}
