package com.micro.magupe.common.mysql;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.micro.magupe.common.util.IdGen;

public abstract class MicroCrudService<D extends MicroCrudDao<T>, T extends MicroDataEntity<T>> {

	@Autowired
	protected D dao;
	
	public T get(Long id) {
		return dao.get(id);
	}
	
	public int save(T entity) {
		if(null == entity.getId()) {
			Date date = new Date();
			entity.setId(IdGen.snowFlakeId());
			entity.setCreateTime(date);
			entity.setDelFlag(new Byte("0"));
			entity.setModifiedTime(date);
		}
		
		return dao.save(entity);
	}
	
	public int update(T entity) {
		return dao.update(entity);
	}
	
	public int deleteByDelFlag(String id) {
		return dao.deleteByDelFlag(id);
	}
	
	public int remove(String id) {
		return dao.remove(id);
	}
	
	public int batchRemove(String[] ids) {
		return dao.batchRemove(ids);
	}
}
