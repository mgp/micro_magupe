package com.micro.magupe.common.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.micro.magupe.common.thread.DbContextHolder;

@Aspect
@Component
@Order(1)
public class ReadWriteAspect { 

	public static final Logger logger = LoggerFactory.getLogger(ReadWriteAspect.class);
	
/*	@Pointcut("@annotation(com.micro.magupe.common.annotation.MicroWrite)")
	public void writePointCut() {
		
	}
	
	@Pointcut("@annotation(com.micro.magupe.common.annotation.MicroRead)")
	public void readPointCut() {
		
	}*/
	
	@Pointcut("execution(* com.micro.magupe.*.controller.*Controller.save*(..)) || "
			+ "execution(* com.micro.magupe.*.controller.*Controller.update*(..)) || "
			+ "execution(* com.micro.magupe.*.controller.*Controller.remove*(..)) || "
			+ "execution(* com.micro.magupe.*.controller.*Controller.batch*(..)) || "
			+ "execution(* com.micro.magupe.*.controller.*Controller.delete*(..))")
	public void writePointCut() {
		
	}
	
	@Pointcut("execution(* com.micro.magupe.*.controller.*Controller.get*(..)) || "
			+ "execution(* com.micro.magupe.*.controller.*Controller.list*(..)) || "
			+ "execution(* com.micro.magupe.*.controller.*Controller.exit*(..)) || "
			+ "execution(* com.micro.magupe.*.controller.*Controller.find*(..)) || "
			+ "execution(* com.micro.magupe.*.controller.*Controller.tree*(..)) || "
			+ "execution(* com.micro.magupe.*.controller.*Controller.search*(..))")
	public void readPointCut() {
		
	}
	
	@Before("writePointCut()")
    public void write() throws Throwable{
		DbContextHolder.write();
    }
	
	@Before("readPointCut()")
	public void read() throws Throwable{
		DbContextHolder.read();
	}
}
