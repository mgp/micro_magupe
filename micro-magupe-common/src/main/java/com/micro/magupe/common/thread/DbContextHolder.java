package com.micro.magupe.common.thread;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.micro.magupe.common.aspect.ReadWriteAspect;
import com.micro.magupe.common.bean.DataSourceType;

/**
 * ThreadLocal
 * 		如果一个资源会引起线程竞争，那就为每一个线程配备一个资源。这就是ThreadLocal需要做的事情，即绑定资源给当前线程。
 * 
 * 
 * @author magupe
 */
public class DbContextHolder {

	private static final ThreadLocal<String> local = new ThreadLocal<String>();
	public static final Logger logger = LoggerFactory.getLogger(ReadWriteAspect.class);
	
    public static ThreadLocal<String> getLocal() {
        return local;
    }

    public static void read() {
    	logger.debug("切换至[读]数据源");
        local.set(DataSourceType.read.getType());
    }

    public static void write() {
    	logger.debug("切换至[写]数据源");
        local.set(DataSourceType.write.getType());
    }

    public static String getJdbcType() {
    	if(local.get() == null) {
    		return DataSourceType.write.getType();
    	}
    	
        return local.get();
    }
}
