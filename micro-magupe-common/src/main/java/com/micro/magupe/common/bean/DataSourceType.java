package com.micro.magupe.common.bean;

public enum DataSourceType {

	read("read", "从库"), write("write", "主库");
	
    private String type;
    private String name;

    DataSourceType(String type, String name) {
        this.type = type;
        this.name = name;
    }

	public String getType() {
		return type;
	}
	
	public String getName() {
		return name;
	}
	
	public static void main(String[] args) {
		DataSourceType type = DataSourceType.read;
		System.err.println(type.getType());
		System.err.println(type.getName());
	}
}
