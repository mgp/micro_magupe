package com.micro.magupe.common.vo;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.micro.magupe.common.mysql.DataEntity;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoleVO extends DataEntity<RoleVO> {
	
	private static final long serialVersionUID = 1L;

    private String roleName;

    private String roleSign;

    private String remark;
    
    private Boolean check;

    private String userIdCreate;
    
    private String userId; // 用于获取角色列表根据用户id
    private List<String> menuIds;

	public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName == null ? null : roleName.trim();
    }

    public String getRoleSign() {
        return roleSign;
    }

    public void setRoleSign(String roleSign) {
        this.roleSign = roleSign == null ? null : roleSign.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getUserIdCreate() {
        return userIdCreate;
    }

    public void setUserIdCreate(String userIdCreate) {
        this.userIdCreate = userIdCreate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
    
    public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Boolean getCheck() {
		return check;
	}

	public void setCheck(Boolean check) {
		this.check = check;
	}

	public List<String> getMenuIds() {
		return menuIds;
	}

	public void setMenuIds(List<String> menuIds) {
		this.menuIds = menuIds;
	}

	@Override
    public String toString() {
        return "Role{" +
                "id='" + id + '\'' +
                ", roleName='" + roleName + '\'' +
                ", roleSign='" + roleSign + '\'' +
                ", remark='" + remark + '\'' +
                ", userIdCreate='" + userIdCreate + '\'' +
                ", createTime='" + createTime + '\'' +
                ", modifiedTime='" + modifiedTime + '\'' +
                '}';
    }
    
    public static class RoleBuilder {
        private RoleVO role = new RoleVO();

        public RoleBuilder withId(String id) {
        	role.setId(id);
            return this;
        }

        public RoleBuilder withRoleName(String roleName) {
        	role.setRoleName(roleName);
            return this;
        }
        
        public RoleBuilder withUserId(String userId) {
        	role.setUserId(userId);
        	return this;
        }

        public RoleVO build() {
            return role;
        }
    }
}