package com.micro.magupe.common.mysql;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.micro.magupe.common.util.IdGen;

public abstract class CrudService<D extends CrudDao<T>, T extends DataEntity<T>> {

	@Autowired
	protected D dao;
	
	public T get(String id) {
		return dao.get(id);
	}
	
	public int save(T entity) {
		if(!StringUtils.isNotBlank(entity.getId())) {
			entity.setUUID(IdGen.uuid());
		}
		
		return dao.save(entity);
	}
	
	public int update(T entity) {
		return dao.update(entity);
	}
	
	public int deleteByDelFlag(String id) {
		return dao.deleteByDelFlag(id);
	}
	
	public int remove(String id) {
		return dao.remove(id);
	}
	
	public int batchRemove(String[] ids) {
		return dao.batchRemove(ids);
	}
}
