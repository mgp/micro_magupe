package com.micro.magupe.common.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.micro.magupe.common.mysql.DataEntity;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeptVO extends DataEntity<DeptVO> {
	
	private static final long serialVersionUID = 1L;

	private DeptVO parent;

    private String parentId;
    private String parentName;
    private String name;

    private Integer orderNum;

    public DeptVO() {
		
	}
    
    public DeptVO(String id, String name) {
    	this.id = id;
    	this.name = name == null ? null : name.trim();
    }
    
    public DeptVO(String id) {
    	this.id = id;
    }
    
	public DeptVO getParent() {
		return parent;
	}

	public void setParent(DeptVO parent) {
		this.parent = parent;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

	@Override
    public String toString() {
        return "DeptVO{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", orderNum='" + orderNum + '\'' +
                ", delFlag='" + delFlag + '\'' +
                '}';
    }
	
    public static class DeptBuilder {
        private DeptVO menu = new DeptVO();

        public DeptBuilder withId(String id) {
        	menu.setId(id);
            return this;
        }
        
        public DeptBuilder withParentId(String parentId) {
        	menu.setParent(new DeptVO(parentId));
        	return this;
        }

        public DeptBuilder withName(String name) {
        	menu.setName(name);
            return this;
        }

        public DeptVO build() {
            return menu;
        }
    }
}