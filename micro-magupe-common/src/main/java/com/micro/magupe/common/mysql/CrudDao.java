package com.micro.magupe.common.mysql;

public interface CrudDao<T> {

	public T get(String id);
	
	public int save(T entity);
	
	public int update(T entity);
	
	public int remove(String id);
	
	public int deleteByDelFlag(String id);
	
	public int batchRemove(String[] ids);
}
