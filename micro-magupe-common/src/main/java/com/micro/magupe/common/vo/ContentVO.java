package com.micro.magupe.common.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.micro.magupe.common.mysql.DataEntity;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContentVO extends DataEntity<ContentVO> {

	private static final long serialVersionUID = -5328342435199352637L;

	private String title; // 标题
	private String slug; //
	private String content; // 内容
	private String type; // 类型
	private String tags; //标签
	private String categories; // 分类
	private Integer hits; 
	private Integer commentsNum; // 评论数量
	private Integer allowComment; // 开启评论
	private Integer allowPing; // 允许ping
	private Integer allowFeed; // 允许反馈
	private Integer status; // 状态

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public String getCategories() {
		return categories;
	}
	public void setCategories(String categories) {
		this.categories = categories;
	}
	public Integer getHits() {
		return hits;
	}
	public void setHits(Integer hits) {
		this.hits = hits;
	}
	public Integer getCommentsNum() {
		return commentsNum;
	}
	public void setCommentsNum(Integer commentsNum) {
		this.commentsNum = commentsNum;
	}
	public Integer getAllowComment() {
		return allowComment;
	}
	public void setAllowComment(Integer allowComment) {
		this.allowComment = allowComment;
	}
	public Integer getAllowPing() {
		return allowPing;
	}
	public void setAllowPing(Integer allowPing) {
		this.allowPing = allowPing;
	}
	public Integer getAllowFeed() {
		return allowFeed;
	}
	public void setAllowFeed(Integer allowFeed) {
		this.allowFeed = allowFeed;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return "ContentVO{" +
				", title='" + title + '\'' +
				", slug='" + slug + '\'' +
				", content='" + content + '\'' +
				", type='" + type + '\'' +
				", tags='" + tags + '\'' +
				", categories='" + categories + '\'' +
				", hits=" + hits +
				", commentsNum=" + commentsNum +
				", allowComment=" + allowComment +
				", allowPing=" + allowPing +
				", allowFeed=" + allowFeed +
				", status=" + status +
				'}';
	}
}
