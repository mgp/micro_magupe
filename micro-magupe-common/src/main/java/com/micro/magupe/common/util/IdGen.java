package com.micro.magupe.common.util;

import java.security.SecureRandom;
import java.util.UUID;

public class IdGen{

	private static SecureRandom random = new SecureRandom();
	private static SnowflakeIdWorker snowFlake = new SnowflakeIdWorker(0, 0);
	
	public static long snowFlakeId() {
		return snowFlake.nextId();
	}
	
	public static long randomLong() {
		return Math.abs(random.nextLong());
	}
	
	public static String randomBase62(int length) {
		byte[] randomBytes = new byte[length];
		random.nextBytes(randomBytes);
		return Encodes.encodeBase62(randomBytes);
	}
	
	/**
	 * 封装JDK自带的UUID, 通过Random数字生成, 中间无-分割.
	 */
	public static String uuid() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
}
