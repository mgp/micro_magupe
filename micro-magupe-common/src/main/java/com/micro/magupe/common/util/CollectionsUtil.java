package com.micro.magupe.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

/**
 * Collections工具集.
 * 在JDK的Collections和Guava的Collections2后, 命名为Collections3.
 */
public class CollectionsUtil {

	/**
	 * 转换Collection所有元素(通过toString())为String, 中间以 separator分隔。
	 */
	public static String convertToString(final Collection<?> collection, final String separator) {
		return StringUtils.join(collection, separator);
	}

	/**
	 * 转换Collection所有元素(通过toString())为String, 每个元素的前面加入prefix，后面加入postfix，如<div>mymessage</div>。
	 */
	public static String convertToString(final Collection<?> collection, final String prefix, final String postfix) {
		StringBuilder builder = new StringBuilder();
		for (Object o : collection) {
			builder.append(prefix).append(o).append(postfix);
		}
		return builder.toString();
	}

	/**
	 * 判断是否为空.
	 */
	public static boolean isEmpty(Collection<?> collection) {
		return (collection == null || collection.isEmpty());
	}

	/**
	 * 取得Collection的第一个元素，如果collection为空返回null.
	 */
	public static <T> T getFirst(Collection<T> collection) {
		if (isEmpty(collection)) {
			return null;
		}

		return collection.iterator().next();
	}

	/**
	 * 获取Collection的最后一个元素 ，如果collection为空返回null.
	 */
	public static <T> T getLast(Collection<T> collection) {
		if (isEmpty(collection)) {
			return null;
		}

		//当类型为List时，直接取得最后一个元素 。
		if (collection instanceof List) {
			List<T> list = (List<T>) collection;
			return list.get(list.size() - 1);
		}

		//其他类型通过iterator滚动到最后一个元素.
		Iterator<T> iterator = collection.iterator();
		while (true) {
			T current = iterator.next();
			if (!iterator.hasNext()) {
				return current;
			}
		}
	}

	/**
	 * 返回a+b的新List.
	 */
	public static <T> List<T> union(final Collection<T> a, final Collection<T> b) {
		List<T> result = new ArrayList<T>(a);
		result.addAll(b);
		return result;
	}

	/**
	 * 返回a-b的新List.
	 */
	public static <T> List<T> subtract(final Collection<T> a, final Collection<T> b) {
		List<T> list = new ArrayList<T>(a);
		for (T element : b) {
			list.remove(element);
		}

		return list;
	}

	/**
	 * 返回a与b的交集的新List.
	 */
	public static <T> List<T> intersection(Collection<T> a, Collection<T> b) {
		List<T> list = new ArrayList<T>();

		for (T element : a) {
			if (b.contains(element)) {
				list.add(element);
			}
		}
		return list;
	}
	
	/**
     * 依据集合元素的属性，从list1中减去list2中存在的元素，
     * 
     * @param list1
     * @param list2
     * @param argumentName
     * @return
     */
    public static <T> List<T> subtractList(List<T> list1, List<T> list2, String argumentName) {
        List<T> result = new ArrayList<T>();
        if (CollectionUtils.isEmpty(list1)) {
            return result;
        }
        if (CollectionUtils.isEmpty(list2)) {
            return list1;
        }
        Map<Object, T> map = initMap(list2, argumentName);
        for (T t : list1) {
            BeanWrapper beanWrapper = new BeanWrapperImpl(t);
            Object value = beanWrapper.getPropertyValue(argumentName);
            if (map.get(value) == null) {
                result.add(t);
            }
        }
        return result;
    }
    
    private static <T> Map<Object, T> initMap(List<T> list2, String argumentName) {
        Map<Object, T> resultMap = new HashMap<Object, T>(list2.size());
        for (T t : list2) {
            BeanWrapper beanWrapper = new BeanWrapperImpl(t);
            if (beanWrapper.getPropertyValue(argumentName) == null) {
                throw new RuntimeException("argumentName is null");
            }
            resultMap.put(beanWrapper.getPropertyValue(argumentName), t);
        }
        return resultMap;
    }
}
