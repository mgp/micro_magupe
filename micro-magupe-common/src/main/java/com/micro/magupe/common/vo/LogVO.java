package com.micro.magupe.common.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.micro.magupe.common.mysql.MicroDataEntity;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class LogVO extends MicroDataEntity<MenuVO> {

	private static final long serialVersionUID = -7769515355144382609L;

	private String userId;

	private String username;

	private String operation;

	private Integer time;

	private String method;

	private String params;

	private String ip;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Integer getTime() {
		return time;
	}

	public void setTime(Integer time) {
		this.time = time;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
}
