package com.micro.magupe.common.mysql;

public interface MicroCrudDao<T> {

	public T get(Long id);
	
	public int save(T entity);
	
	public int update(T entity);
	
	public int remove(String id);
	
	public int deleteByDelFlag(String id);
	
	public int batchRemove(String[] ids);
}
