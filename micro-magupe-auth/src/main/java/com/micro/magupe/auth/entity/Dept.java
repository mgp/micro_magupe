package com.micro.magupe.auth.entity;

import com.micro.magupe.common.mysql.DataEntity;

public class Dept extends DataEntity<Dept> {
	
	private static final long serialVersionUID = 1L;

	private Dept parent;

    private String name;

    private Integer orderNum;

    public Dept() {
		
	}
    
    public Dept(String id) {
    	this.id = id;
    }
    
    public Dept(String id, String parentId) {
    	this.id = id;
    	this.parent = new Dept(parentId);
    }
    
	public Dept getParent() {
		return parent;
	}

	public void setParent(Dept parent) {
		this.parent = parent;
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

	@Override
    public String toString() {
        return "Dept{" +
                "id='" + id + '\'' +
                ", parentId='" + parent.getId() + '\'' +
                ", name='" + name + '\'' +
                ", orderNum='" + orderNum + '\'' +
                ", delFlag='" + delFlag + '\'' +
                '}';
    }
	
    public static class DeptBuilder {
        private Dept menu = new Dept();

        public DeptBuilder withId(String id) {
        	menu.setId(id);
            return this;
        }
        
        public DeptBuilder withParentId(String parentId) {
        	menu.setParent(new Dept(parentId));
        	return this;
        }

        public DeptBuilder withName(String name) {
        	menu.setName(name);
            return this;
        }

        public Dept build() {
            return menu;
        }
    }
}