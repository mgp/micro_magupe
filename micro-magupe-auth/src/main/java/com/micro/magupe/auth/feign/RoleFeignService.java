package com.micro.magupe.auth.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.micro.magupe.auth.feign.fallback.RoleFeignFallback;
import com.micro.magupe.common.vo.RoleVO;

@FeignClient(value ="micro-magupe-user", fallback=RoleFeignFallback.class)
public interface RoleFeignService {

	@ResponseBody
	@RequestMapping(value="/microuser/role/listByUserId",method=RequestMethod.GET)
	List<RoleVO> listByUserId(@RequestParam("id") String id);

}
