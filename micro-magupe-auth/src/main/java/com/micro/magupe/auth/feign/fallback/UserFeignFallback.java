package com.micro.magupe.auth.feign.fallback;

import org.springframework.stereotype.Component;

import com.micro.magupe.auth.feign.UserFeignService;
import com.micro.magupe.common.vo.UserVO;

@Component
public class UserFeignFallback implements UserFeignService{

	@Override
	public UserVO searchByUsername(String username) {
		return null;
	}

}
