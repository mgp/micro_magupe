package com.micro.magupe.auth.dao;

import java.util.List;

import com.micro.magupe.auth.entity.Role;
import com.micro.magupe.common.annotation.MyBatisDao;
import com.micro.magupe.common.bean.Query;
import com.micro.magupe.common.mysql.CrudDao;
import com.micro.magupe.common.vo.RoleVO;

@MyBatisDao
public interface RoleDao extends CrudDao<Role>{
	
	List<RoleVO> listByUserId(Role role);
 
	List<RoleVO> list(Role role);
	
	List<RoleVO> listQuery(Query query);

	int count(Query query);

	RoleVO getById(RoleVO roleVO);
}