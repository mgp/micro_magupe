package com.micro.magupe.auth.entity;

import java.util.Date;

import com.micro.magupe.common.mysql.DataEntity;

public class Role extends DataEntity<Role> {
	
	private static final long serialVersionUID = 1L;

    private String roleName;

    private String roleSign;

    private String remark;

    private String userId; // 用于获取角色列表根据用户id

	public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName == null ? null : roleName.trim();
    }

    public String getRoleSign() {
        return roleSign;
    }

    public void setRoleSign(String roleSign) {
        this.roleSign = roleSign == null ? null : roleSign.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
    
    public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
    public String toString() {
        return "Role{" +
                "id='" + id + '\'' +
                ", roleName='" + roleName + '\'' +
                ", roleSign='" + roleSign + '\'' +
                ", remark='" + remark + '\'' +
                ", createTime='" + createTime + '\'' +
                ", modifiedTime='" + modifiedTime + '\'' +
                '}';
    }
    
    public static class RoleBuilder {
        private Role role = new Role();

        public RoleBuilder withId(String id) {
        	role.setId(id);
            return this;
        }

        public RoleBuilder withRoleName(String roleName) {
        	role.setRoleName(roleName);
            return this;
        }
        
        public RoleBuilder withUserId(String userId) {
        	role.setUserId(userId);
        	return this;
        }

        public Role build() {
            return role;
        }
    }
}