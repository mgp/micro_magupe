package com.micro.magupe.auth.dao;

import java.util.LinkedHashMap;
import java.util.List;

import com.micro.magupe.auth.entity.Dept;
import com.micro.magupe.common.annotation.MyBatisDao;
import com.micro.magupe.common.bean.Query;
import com.micro.magupe.common.mysql.CrudDao;
import com.micro.magupe.common.vo.DeptVO;

@MyBatisDao
public interface DeptDao extends CrudDao<Dept>{

	List<DeptVO> list(Dept dept);

	int count(Query query);

	List<DeptVO> listQuery(LinkedHashMap<String, Object> query);

	DeptVO getById(DeptVO deptVO);
}