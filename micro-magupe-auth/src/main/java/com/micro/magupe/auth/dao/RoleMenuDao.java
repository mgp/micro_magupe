package com.micro.magupe.auth.dao;

import java.util.List;

import com.micro.magupe.auth.entity.RoleMenu;
import com.micro.magupe.common.annotation.MyBatisDao;

@MyBatisDao
public interface RoleMenuDao {
	
	public RoleMenu get(String id);
	
	public int save(RoleMenu entity);
	
	public int update(RoleMenu entity);
	
	public int remove(String id);
	
	public int removeByRoleId(String roleId);
	
	public List<RoleMenu> list(RoleMenu entity);
	
	List<String> listMenuIdByRoleId(RoleMenu roleMenu);

	public void batchSave(List<RoleMenu> list);

}