package com.micro.magupe.auth.security;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;

public class AuthAuthentication extends AbstractAuthenticationToken {

	private static final long serialVersionUID = -7870464439711224052L;
	
	private UserDetails userDetails;
	
    public AuthAuthentication(UserDetails userDetails) {
        super(null);
        this.userDetails = userDetails;
        super.setAuthenticated(true);
    }
    
	@Override
	public Object getCredentials() {
		return this.userDetails.getPassword();
	}

	@Override
	public Object getPrincipal() {
		return this.userDetails;
	}

}
