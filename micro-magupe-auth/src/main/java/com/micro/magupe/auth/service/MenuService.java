package com.micro.magupe.auth.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.micro.magupe.auth.dao.MenuDao;
import com.micro.magupe.auth.entity.Menu;
import com.micro.magupe.auth.entity.Menu.MenuBuilder;
import com.micro.magupe.common.bean.Query;
import com.micro.magupe.common.bean.Tree;
import com.micro.magupe.common.mysql.CrudService;
import com.micro.magupe.common.util.BuildTree;
import com.micro.magupe.common.vo.MenuVO;

@Service
@Transactional(readOnly = true)
public class MenuService extends CrudService<MenuDao, Menu>{

	@Autowired
	RoleMenuService roleMenuService;
	
	public Menu get(String id) {
		return super.get(id);
	}

	@Transactional(readOnly = false)
	public int save(Menu menu) {
		return super.save(menu);
	}

	@Transactional(readOnly = false)
	public int update(Menu menu) {
		return super.update(menu);
	}

	@Transactional(readOnly = false)
	public int remove(String id) {
		return super.remove(id);
	}

	@Transactional(readOnly = false)
	public int deleteByDelFlag(String id) {
		return super.deleteByDelFlag(id);
	}

	@Transactional(readOnly = false)
	public int batchRemove(String[] ids) {
		return super.batchRemove(ids);
	}

	public List<MenuVO> list(Menu menu) {
		return dao.list(menu);
	}
	
	public List<String> listUserPerms(String userId){
		return dao.listUserPerms(userId);
	}

	public List<MenuVO> listByRoleId(String roleId) {
		return dao.listByRoleId(new MenuBuilder().withRoleId(roleId).build());
	}
	
	public List<MenuVO> listQuery(Map<String, Object> params) {
		return dao.listQuery(params);
	}

	public int count(Query query) {
		return dao.count(query);
	}

	public List<MenuVO> listByUserId(String userId) {
		return dao.listByUserId(new MenuBuilder().withRoleId(userId).build());
	}
	
	public List<MenuVO> listByUserId2(String userId) {
		return dao.listByUserId2(new MenuBuilder().withRoleId(userId).build());
	}

	@Transactional(readOnly = false)
	public int save(MenuVO menuVO) {
		Menu menu = new Menu();
        BeanUtils.copyProperties(menuVO, menu);
        menu.setDelFlag(new Byte("0"));
        menu.setParent(new Menu(menuVO.getParentId()));
        int count = save(menu);
        
        return count;
	}

	@Transactional(readOnly = false)
	public int update(MenuVO menuVO) {
        Menu menu = new Menu();
        BeanUtils.copyProperties(menuVO, menu);
        menu.setParent(new Menu(menuVO.getParentId()));
        int count = update(menu);
        
        return count;
	}

	public MenuVO getById(String id) {
		MenuVO menuVO = new MenuVO();
		menuVO.setId(id);
		return dao.getById(menuVO);
	}

	public Tree<MenuVO> getTree() {
		List<Tree<MenuVO>> trees = new ArrayList<Tree<MenuVO>>();
		List<MenuVO> menuVOs = dao.list(new MenuBuilder().build());
		
		for (MenuVO menuVO : menuVOs) {
			Tree<MenuVO> tree = new Tree<MenuVO>();
			tree.setId(menuVO.getId());
			tree.setParentId(menuVO.getParent().getId());
			tree.setText(menuVO.getName());
			trees.add(tree);
		}
		
		// 默认顶级菜单为０，根据数据库实际情况调整
		Tree<MenuVO> t = BuildTree.build(trees);
		return t;
	}

	public Tree<MenuVO> getTree(String roleId) {
		List<MenuVO> menuVOs = dao.list(new MenuBuilder().build());
		List<String> menuIds = roleMenuService.listMenuIdByRoleId(roleId);
		
		List<String> temp = menuIds;
		for (MenuVO menu : menuVOs) {
			if (temp.contains(menu.getParent().getId())) {
				menuIds.remove(menu.getParent().getId());
			}
		}
		
		List<Tree<MenuVO>> trees = new ArrayList<Tree<MenuVO>>();
		for (MenuVO menuVO : menuVOs) {
			Tree<MenuVO> tree = new Tree<MenuVO>();
			tree.setId(menuVO.getId());
			tree.setParentId(menuVO.getParent().getId());
			tree.setText(menuVO.getName());
			
			Map<String, Object> state = new HashMap<>(16);
			String menuId = menuVO.getId();
			if (menuIds.contains(menuId)) {
				state.put("selected", true);
			} else {
				state.put("selected", false);
			}
			tree.setState(state);
			trees.add(tree);
		}
		
		// 默认顶级菜单为０，根据数据库实际情况调整
		Tree<MenuVO> t = BuildTree.build(trees);
		return t;
	}
}
