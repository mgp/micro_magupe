package com.micro.magupe.auth.security;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import com.micro.magupe.auth.vo.UserDetailsImpl;

public class AuthJwtAccessTokenConverter extends JwtAccessTokenConverter implements Serializable {

	private static final long serialVersionUID = 3250122573994317127L;
	
	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		String username = authentication.getUserAuthentication().getName();
		UserDetailsImpl user = (UserDetailsImpl) authentication.getUserAuthentication().getPrincipal();
				
		final Map<String, Object> additionalInformation = new HashMap<String, Object>();
		additionalInformation.put("username", username);
		additionalInformation.put("userId", user.getId());
        DefaultOAuth2AccessToken customAccessToken = new DefaultOAuth2AccessToken(accessToken);
        customAccessToken.setAdditionalInformation(additionalInformation);
		
		return super.enhance(customAccessToken, authentication);
	}

}
