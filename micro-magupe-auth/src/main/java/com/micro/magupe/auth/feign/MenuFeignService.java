package com.micro.magupe.auth.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.micro.magupe.auth.feign.fallback.MenuFeignFallback;
import com.micro.magupe.common.vo.MenuVO;

@FeignClient(value ="micro-magupe-user", fallback=MenuFeignFallback.class)
public interface MenuFeignService {

	@ResponseBody
	@RequestMapping(value="/microuser/menu/listByRoleId",method=RequestMethod.GET)
	List<MenuVO> listByRoleId(@RequestParam("id") String id);
}
