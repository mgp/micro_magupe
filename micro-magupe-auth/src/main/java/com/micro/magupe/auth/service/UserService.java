package com.micro.magupe.auth.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.micro.magupe.auth.dao.UserDao;
import com.micro.magupe.auth.dao.UserRoleDao;
import com.micro.magupe.auth.entity.User;
import com.micro.magupe.auth.entity.User.UserBuilder;
import com.micro.magupe.auth.entity.UserRole;
import com.micro.magupe.auth.entity.UserRole.UserRoleBuilder;
import com.micro.magupe.common.bean.Query;
import com.micro.magupe.common.mysql.CrudService;
import com.micro.magupe.common.util.CollectionsUtil;
import com.micro.magupe.common.util.IdGen;
import com.micro.magupe.common.vo.UserVO;

@Service
@Transactional(readOnly = true)
public class UserService extends CrudService<UserDao, User>{

    @Autowired
    UserRoleDao userRoleDao;
    
	public User get(String id) {
		return super.get(id);
	}

	@Transactional(readOnly = false)
	public int save(User user) {
		return super.save(user);
	}

	@Transactional(readOnly = false)
	public int update(User user) {
		return super.update(user);
	}

	@Transactional(readOnly = false)
	public int remove(String id) {
		return super.remove(id);
	}

	@Transactional(readOnly = false)
	public int deleteByDelFlag(String id) {
		return super.deleteByDelFlag(id);
	}

	@Transactional(readOnly = false)
	public int batchRemove(String[] ids) {
		return super.batchRemove(ids);
	}

	public List<UserVO> list(User user) {
		return dao.list(user);
	}

	public UserVO searchByUsername(String username) {
		List<UserVO> list = dao.searchByUsername(new UserBuilder().withUsername(username).build());
		if(!CollectionsUtil.isEmpty(list)){
			return list.get(0);
		}
		
		return null;
	}

	public List<UserVO> listQuery(Query query) {
		return dao.listQuery(query);
	}

	public int count(Query query) {
		return dao.count(query);
	}

	@Transactional(readOnly = false)
	public int save(UserVO userVO) {
        User user = new User();
        BeanUtils.copyProperties(userVO, user);
        user.setDelFlag(new Byte("0"));
        int count = save(user);
        
        // save role
        String[] roleIds = userVO.getRoleIds();
        userRoleDao.removeByUserId(user.getId());
        List<UserRole> list = new ArrayList<>();
        for (String roleId : roleIds) {
            list.add(new UserRoleBuilder().withId(IdGen.uuid()).withRoleId(roleId).withUserId(user.getId()).build());
        }
        if (list.size() > 0) {
        	userRoleDao.batchSave(list);
        }
        
		return count;
	}

	@Transactional(readOnly = false)
	public int update(UserVO userVO) {
        User user = new User();
        BeanUtils.copyProperties(userVO, user);
        user.setDelFlag(new Byte("0"));
        int count = update(user);
        
        // save role
        String[] roleIds = userVO.getRoleIds();
        userRoleDao.removeByUserId(user.getId());
        List<UserRole> list = new ArrayList<>();
        for (String roleId : roleIds) {
            list.add(new UserRoleBuilder().withId(IdGen.uuid()).withRoleId(roleId).withUserId(user.getId()).build());
        }
        if (list.size() > 0) {
        	userRoleDao.batchSave(list);
        }
        
		return count;
	}
	
	public boolean exitUsername(User user) {
        boolean exit;
        exit = dao.list(user).size() > 0;
        return exit;
	}

	public UserVO getById(String id) {
		UserVO userVO = new UserVO();
		userVO.setId(id);
		return dao.getById(userVO);
	}
}
