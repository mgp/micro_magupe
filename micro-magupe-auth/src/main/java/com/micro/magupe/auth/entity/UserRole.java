package com.micro.magupe.auth.entity;

import java.io.Serializable;

public class UserRole implements Serializable{
	
	private static final long serialVersionUID = -8324999674897348666L;

	private String id;

    private String userId;

    private String roleId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }
    
    @Override
    public String toString() {
        return "UserRole{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", roleId='" + roleId + '\'' +
                '}';
    }
    
    public static class UserRoleBuilder {
        private UserRole userRole = new UserRole();

        public UserRoleBuilder withId(String id) {
        	userRole.setId(id);
            return this;
        }
        
        public UserRoleBuilder withRoleId(String roleId) {
        	userRole.setRoleId(roleId);
        	return this;
        }

        public UserRoleBuilder withUserId(String userId) {
        	userRole.setUserId(userId);
            return this;
        }

        public UserRole build() {
            return userRole;
        }
    }
}