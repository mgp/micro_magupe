package com.micro.magupe.auth.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.micro.magupe.auth.dao.RoleMenuDao;
import com.micro.magupe.auth.entity.RoleMenu;
import com.micro.magupe.auth.entity.RoleMenu.RoleMenuBuilder;

@Service
@Transactional(readOnly = true)
public class RoleMenuService {

	@Autowired
	RoleMenuDao roleMenuDao;
	
	public RoleMenu get(String id) {
		return roleMenuDao.get(id);
	}

	@Transactional(readOnly = false)
	public void save(RoleMenu roleMenu) {
		roleMenuDao.save(roleMenu);
	}

	@Transactional(readOnly = false)
	public void update(RoleMenu roleMenu) {
		roleMenuDao.update(roleMenu);
	}

	@Transactional(readOnly = false)
	public void remove(String id) {
		roleMenuDao.remove(id);
	}

	@Transactional(readOnly = false)
	public void removeByRoleId(String roleId) {
		roleMenuDao.removeByRoleId(roleId);
	}

	public List<RoleMenu> list(RoleMenu roleMenu) {
		return roleMenuDao.list(roleMenu);
	}
	
	public List<String> listMenuIdByRoleId(RoleMenu roleMenu) {
		return roleMenuDao.listMenuIdByRoleId(roleMenu);
	}

	public List<String> listMenuIdByRoleId(String roleId) {
		return roleMenuDao.listMenuIdByRoleId(new RoleMenuBuilder().withRoleId(roleId).build());
	}

	public void batchSave(List<RoleMenu> list) {
		roleMenuDao.batchSave(list);
	}
}
