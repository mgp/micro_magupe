package com.micro.magupe.auth.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.micro.magupe.auth.feign.fallback.UserFeignFallback;
import com.micro.magupe.common.vo.UserVO;

@FeignClient(value ="micro-magupe-user", fallback=UserFeignFallback.class)
public interface UserFeignService {

	@ResponseBody
	@RequestMapping(value="/microuser/user/searchByUsername",method=RequestMethod.GET)
	UserVO searchByUsername(@RequestParam("username") String username);

}
