package com.micro.magupe.auth.feign.fallback;

import java.util.List;

import org.springframework.stereotype.Component;

import com.micro.magupe.auth.feign.MenuFeignService;
import com.micro.magupe.common.vo.MenuVO;

@Component
public class MenuFeignFallback implements MenuFeignService{

	@Override
	public List<MenuVO> listByRoleId(String id) {
		return null;
	}

}
