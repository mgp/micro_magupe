package com.micro.magupe.auth.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.micro.magupe.auth.dao.DeptDao;
import com.micro.magupe.auth.entity.Dept;
import com.micro.magupe.common.bean.Query;
import com.micro.magupe.common.bean.Tree;
import com.micro.magupe.common.mysql.CrudService;
import com.micro.magupe.common.util.BuildTree;
import com.micro.magupe.common.vo.DeptVO;

@Service
@Transactional(readOnly = true)
public class DeptService extends CrudService<DeptDao, Dept>{
	
	public Dept get(String id) {
		return super.get(id);
	}

	@Transactional(readOnly = false)
	public int save(Dept dept) {
		return super.save(dept);
	}

	@Transactional(readOnly = false)
	public int update(Dept dept) {
		return super.update(dept);
	}

	@Transactional(readOnly = false)
	public int remove(String id) {
		return super.remove(id);
	}

	@Transactional(readOnly = false)
	public int deleteByDelFlag(String id) {
		return super.deleteByDelFlag(id);
	}

	@Transactional(readOnly = false)
	public int batchRemove(String[] ids) {
		return super.batchRemove(ids);
	}

	public List<DeptVO> list(Dept dept) {
		return dao.list(dept);
	}
	
	public int count(Query query) {
		return dao.count(query);
	}

	public Tree<DeptVO> getTree() {
		List<Tree<DeptVO>> trees = new ArrayList<Tree<DeptVO>>();
		
		List<DeptVO> list = dao.list(new Dept(StringUtils.EMPTY, StringUtils.EMPTY));
		for (DeptVO dept : list) {
			Tree<DeptVO> tree = new Tree<DeptVO>();
			tree.setId(dept.getId());
			tree.setParentId(dept.getParent().getId());
			tree.setText(dept.getName());
			Map<String, Object> state = new HashMap<String, Object>(16);
			state.put("opened", true);
			tree.setState(state);
			trees.add(tree);
		}
		
		// 默认顶级菜单为０，根据数据库实际情况调整
		Tree<DeptVO> t = BuildTree.build(trees);
		return t;
	}

	public List<DeptVO> listQuery(LinkedHashMap<String, Object> query) {
		return dao.listQuery(query);
	}

	public boolean exitName(Dept dept) {
        boolean exit;
        exit = dao.list(dept).size() > 0;
        return exit;
	}

	@Transactional(readOnly = false)
	public int save(DeptVO deptVO) {
        Dept dept = new Dept();
        BeanUtils.copyProperties(deptVO, dept);
        dept.setDelFlag(new Byte("0"));
        
        dept.setParent(new Dept(deptVO.getParentId()));
        
        int count = save(dept);
        
        return count;
	}

	@Transactional(readOnly = false)
	public int update(DeptVO deptVO) {
        Dept dept = new Dept();
        BeanUtils.copyProperties(deptVO, dept);
        dept.setParent(new Dept(deptVO.getParentId()));
        dept.setDelFlag(new Byte("0"));
        int count = update(dept);
        
        return count;
	}

	public DeptVO getById(String id) {
		DeptVO deptVO = new DeptVO();
		deptVO.setId(id);
		return dao.getById(deptVO);
	}

}
