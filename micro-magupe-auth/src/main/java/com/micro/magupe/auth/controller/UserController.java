package com.micro.magupe.auth.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.micro.magupe.auth.entity.User;
import com.micro.magupe.auth.service.UserService;
import com.micro.magupe.common.bean.Query;
import com.micro.magupe.common.bean.Result;
import com.micro.magupe.common.util.PageUtils;
import com.micro.magupe.common.vo.UserVO;

@RequestMapping("/user")
@Controller
public class UserController {

	@Autowired
	UserService userService;
	
	@GetMapping("/list")
	@ResponseBody
	PageUtils list(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		List<UserVO> list = userService.listQuery(query);
		
		int total = userService.count(query);
		PageUtils pageUtil = new PageUtils(list, total);
		return pageUtil;
	}
	
	@PostMapping("/exitUsername")
	@ResponseBody
	boolean exitUsername(User user) {
		// 存在，不通过，false
		return !userService.exitUsername(user);
	}
	
	@PostMapping("/save")
	@ResponseBody
	Result save(UserVO user) {
		if (userService.save(user) > 0) {
			return Result.ok();
		}
		
		return Result.error();
	}
	
	@PostMapping("/update")
	@ResponseBody
	Result update(UserVO userVO) {
		if (userService.update(userVO) > 0) {
			return Result.ok();
		}
		
		return Result.error();
	}
	
	@ResponseBody
	@RequestMapping(value="/searchByUsername", method=RequestMethod.GET)
	UserVO searchByUsername(@RequestParam String username) {
		return userService.searchByUsername(username);
	}
	
	@ResponseBody
	@RequestMapping(value="/getById", method=RequestMethod.GET)
	UserVO getById(@RequestParam String id) {
		UserVO userVO = userService.getById(id);
		return userVO;
	}
}
