package com.micro.magupe.auth.dao;

import java.util.List;

import com.micro.magupe.auth.entity.UserRole;
import com.micro.magupe.common.annotation.MyBatisDao;

@MyBatisDao
public interface UserRoleDao {

	public UserRole get(String id);
	
	public int save(UserRole entity);
	
	public int update(UserRole entity);
	
	public int remove(String id);
	
	public int removeByRoleId(String roleId);
	
	public int removeByUserId(String id);
	
	public List<UserRole> list(UserRole entity);
	
	List<String> listRoleId(UserRole entity);

	public void batchSave(List<UserRole> list);
}