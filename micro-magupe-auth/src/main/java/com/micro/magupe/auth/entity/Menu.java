package com.micro.magupe.auth.entity;

import java.util.Date;

import com.micro.magupe.common.mysql.DataEntity;

public class Menu extends DataEntity<Menu> {
	
	private static final long serialVersionUID = 1L;

	private Menu parent;

    private String name;

    private String url;

    private String perms;

    private Integer type;

    private String icon;

    private Integer orderNum;
    
    private String roleId; // 非数据库字段

    public Menu() {
    	
	}
    
    public Menu(String id) {
    	this.id = id;
    }

	public Menu getParent() {
		return parent;
	}

	public void setParent(Menu parent) {
		this.parent = parent;
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    public String getPerms() {
        return perms;
    }

    public void setPerms(String perms) {
        this.perms = perms == null ? null : perms.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon == null ? null : icon.trim();
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
    
    public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	@Override
    public String toString() {
        return "Menu{" +
                "id='" + id + '\'' +
                ", parentId='" + parent.getId() + '\'' +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", perms=" + perms +
                ", type='" + type + '\'' +
                ", icon='" + icon + '\'' +
                ", orderNum='" + orderNum + '\'' +
                ", createTime='" + createTime + '\'' +
                ", modifiedTime='" + modifiedTime + '\'' +
                '}';
    }
    
    public static class MenuBuilder {
        private Menu menu = new Menu();

        public MenuBuilder withId(String id) {
        	menu.setId(id);
            return this;
        }
        
        public MenuBuilder withParentId(String parentId) {
        	menu.setParent(new Menu(parentId));
        	return this;
        }
        
        public MenuBuilder withRoleId(String roleId) {
        	menu.setRoleId(roleId);
        	return this;
        }

        public MenuBuilder withName(String name) {
        	menu.setName(name);
            return this;
        }

        public Menu build() {
            return menu;
        }
    }
}