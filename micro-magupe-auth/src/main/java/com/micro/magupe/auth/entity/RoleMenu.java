package com.micro.magupe.auth.entity;

import java.io.Serializable;

public class RoleMenu implements Serializable{

	private static final long serialVersionUID = -7028535755275990169L;

	private String id;
    
    private String roleId;

    private String menuId;

    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }
    
    @Override
    public String toString() {
        return "RoleMenu{" +
                "id='" + id + '\'' +
                ", roleId='" + roleId + '\'' +
                ", menuId='" + menuId + '\'' +
                '}';
    }
    
    public static class RoleMenuBuilder {
        private RoleMenu roleMenu = new RoleMenu();

        public RoleMenuBuilder withId(String id) {
        	roleMenu.setId(id);
            return this;
        }
        
        public RoleMenuBuilder withRoleId(String roleId) {
        	roleMenu.setRoleId(roleId);
        	return this;
        }

        public RoleMenuBuilder withMenuId(String menuId) {
        	roleMenu.setMenuId(menuId);
            return this;
        }

        public RoleMenu build() {
            return roleMenu;
        }
    }
}