package com.micro.magupe.auth.dao;

import java.util.List;
import java.util.Map;

import com.micro.magupe.auth.entity.Menu;
import com.micro.magupe.common.annotation.MyBatisDao;
import com.micro.magupe.common.bean.Query;
import com.micro.magupe.common.mysql.CrudDao;
import com.micro.magupe.common.vo.MenuVO;

@MyBatisDao
public interface MenuDao extends CrudDao<Menu>{
	
	List<String> listUserPerms(String userId);

	List<MenuVO> listByRoleId(Menu menu);
	
	List<MenuVO> list(Menu menu);
	
	List<MenuVO> listQuery(Map<String, Object> params);

	int count(Query query);

	List<MenuVO> listByUserId(Menu menu);
	
	List<MenuVO> listByUserId2(Menu menu);

	MenuVO getById(MenuVO menuVO);
}