package com.micro.magupe.auth.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.micro.magupe.auth.dao.UserRoleDao;
import com.micro.magupe.auth.entity.UserRole;

@Service
@Transactional(readOnly = true)
public class UserRoleService {

	@Autowired
	UserRoleDao userRoleDao;
	
	public UserRole get(String id) {
		return userRoleDao.get(id);
	}

	@Transactional(readOnly = false)
	public void save(UserRole userRole) {
		userRoleDao.save(userRole);
	}

	@Transactional(readOnly = false)
	public void update(UserRole userRole) {
		userRoleDao.update(userRole);
	}

	@Transactional(readOnly = false)
	public void remove(String id) {
		userRoleDao.remove(id);
	}

	@Transactional(readOnly = false)
	public void removeByRoleId(String roleId) {
		userRoleDao.removeByRoleId(roleId);
	}
	
	@Transactional(readOnly = false)
	public void removeByUserId(String userId) {
		userRoleDao.removeByUserId(userId);
	}

	public List<UserRole> list(UserRole userRole) {
		return userRoleDao.list(userRole);
	}
	
	public List<String> listRoleId(UserRole userRole) {
		return userRoleDao.listRoleId(userRole);
	}
}
