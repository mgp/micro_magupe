package com.micro.magupe.auth.feign.fallback;

import java.util.List;

import org.springframework.stereotype.Component;

import com.micro.magupe.auth.feign.RoleFeignService;
import com.micro.magupe.common.vo.RoleVO;

@Component
public class RoleFeignFallback implements RoleFeignService{

	@Override
	public List<RoleVO> listByUserId(String id) {
		return null;
	}

}
