package com.micro.magupe.auth.dao;

import java.util.List;

import com.micro.magupe.auth.entity.User;
import com.micro.magupe.common.annotation.MyBatisDao;
import com.micro.magupe.common.bean.Query;
import com.micro.magupe.common.mysql.CrudDao;
import com.micro.magupe.common.vo.UserVO;

@MyBatisDao
public interface UserDao extends CrudDao<User>{

	List<UserVO> searchByUsername(User user);
	
	List<UserVO> list(User user);

	List<UserVO> listQuery(Query query);

	int count(Query query);

	UserVO getById(UserVO userVO);
	
}