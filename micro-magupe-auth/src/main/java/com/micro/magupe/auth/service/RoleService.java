package com.micro.magupe.auth.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.micro.magupe.auth.dao.RoleDao;
import com.micro.magupe.auth.entity.Role;
import com.micro.magupe.auth.entity.Role.RoleBuilder;
import com.micro.magupe.auth.entity.RoleMenu;
import com.micro.magupe.auth.entity.RoleMenu.RoleMenuBuilder;
import com.micro.magupe.common.bean.Query;
import com.micro.magupe.common.mysql.CrudService;
import com.micro.magupe.common.util.CollectionsUtil;
import com.micro.magupe.common.util.IdGen;
import com.micro.magupe.common.vo.RoleVO;

@Service
@Transactional(readOnly = true)
public class RoleService extends CrudService<RoleDao, Role>{

	@Autowired
	RoleMenuService roleMenuService;
	
	public Role get(String id) {
		return super.get(id);
	}

	@Transactional(readOnly = false)
	public int save(Role role) {
		return super.save(role);
	}

	@Transactional(readOnly = false)
	public int update(Role role) {
		return super.update(role);
	}

	@Transactional(readOnly = false)
	public int remove(String id) {
		return super.remove(id);
	}

	@Transactional(readOnly = false)
	public int deleteByDelFlag(String id) {
		return super.deleteByDelFlag(id);
	}

	@Transactional(readOnly = false)
	public int batchRemove(String[] ids) {
		return super.batchRemove(ids);
	}

	public List<RoleVO> list(Role role) {
		return dao.list(role);
	}
	
	public List<RoleVO> listByUserId(String userId) {
		return dao.listByUserId(new RoleBuilder().withUserId(userId).build());
	}
	
	public List<RoleVO> listQuery(Query query) {
		return dao.listQuery(query);
	}

	public int count(Query query) {
		return dao.count(query);
	}

	public RoleVO getById(String id) {
		RoleVO roleVO = new RoleVO();
		roleVO.setId(id);
		return dao.getById(roleVO);
	}

	@Transactional(readOnly = false)
	public int save(RoleVO roleVO) {
        Role role = new Role();
        BeanUtils.copyProperties(roleVO, role);
        role.setDelFlag(new Byte("0"));
        int count = save(role);
        
        List<String> menuIds = roleVO.getMenuIds();
        if(!CollectionsUtil.isEmpty(menuIds)) {
        	List<RoleMenu> rms = new ArrayList<RoleMenu>();
        	for (String menuId : menuIds) {
        		rms.add(new RoleMenuBuilder().withId(IdGen.uuid()).withMenuId(menuId).withRoleId(role.getId()).build());
			}
        	
        	roleMenuService.removeByRoleId(role.getId());
        	roleMenuService.batchSave(rms);
        }
        
		return count;
	}

	@Transactional(readOnly = false)
	public int update(RoleVO roleVO) {
        Role role = new Role();
        BeanUtils.copyProperties(roleVO, role);
        int count = update(role);
        
        List<String> menuIds = roleVO.getMenuIds();
        if(!CollectionsUtil.isEmpty(menuIds)) {
        	List<RoleMenu> rms = new ArrayList<RoleMenu>();
        	for (String menuId : menuIds) {
        		if(!menuId.equals("-1")) {
        			rms.add(new RoleMenuBuilder().withId(IdGen.uuid()).withMenuId(menuId).withRoleId(role.getId()).build());
        		}
        	}
        	
        	roleMenuService.removeByRoleId(role.getId());
        	roleMenuService.batchSave(rms);
        }
        
		return count;
	}
}
