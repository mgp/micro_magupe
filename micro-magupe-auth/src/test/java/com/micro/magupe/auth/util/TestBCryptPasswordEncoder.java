package com.micro.magupe.auth.util;

import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class TestBCryptPasswordEncoder {

	@Test
	public void test() {
		String password = "admin";
        BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder();
        String hashPass = bcryptPasswordEncoder.encode(password);
        System.out.println(hashPass);

        boolean f = bcryptPasswordEncoder.matches("admin", hashPass);
        System.out.println(f);
	}
	
	public static void main(String[] args) {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encode = bCryptPasswordEncoder.encode("880204");
        System.out.println(encode);
	}
}
