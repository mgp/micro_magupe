package com.micro.magupe.auth.util;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;
import org.springframework.beans.BeanUtils;

import com.micro.magupe.auth.entity.Dept;
import com.micro.magupe.common.vo.DeptVO;

public class TestBeanUtil {

	@Test
	public void test() throws IllegalAccessException, InvocationTargetException {
		Dept dept = new Dept();
		dept.setId("2");
		dept.setName("人事部");
		dept.setParent(new Dept("1"));
		
		System.err.println(dept);
		System.err.println("-----------------------------");
		DeptVO deptVO = new DeptVO();
		
		BeanUtils.copyProperties(dept, deptVO);
		
		System.err.println(deptVO);
	}
}
