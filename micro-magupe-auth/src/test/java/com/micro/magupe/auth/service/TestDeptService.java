package com.micro.magupe.auth.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.micro.magupe.MicroMagupeAuthApplication;
import com.micro.magupe.auth.entity.Dept;
import com.micro.magupe.auth.entity.Dept.DeptBuilder;
import com.micro.magupe.common.vo.DeptVO;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes={MicroMagupeAuthApplication.class, TestDeptService.class})
public class TestDeptService {

	@Autowired
	DeptService deptService;
	
	@Test
	public void testList() {
		Dept dept = new DeptBuilder().withName("人事部").build();
		
		System.err.println(deptService.exitName(dept));
	}
	
	@Test
	public void testGetById() {
		DeptVO deptVO = deptService.getById("5");
		
		System.err.println(deptVO);
		System.err.println(deptVO.getParentId());
		System.err.println(deptVO.getParentName());
	}
}
