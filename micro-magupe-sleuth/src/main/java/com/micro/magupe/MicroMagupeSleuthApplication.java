package com.micro.magupe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import zipkin.server.EnableZipkinServer;

/**
 * curl -sSL https://zipkin.io/quickstart.sh | bash -s
 * java -jar zipkin.jar
 */
@EnableEurekaClient
@EnableZipkinServer
@SpringBootApplication
public class MicroMagupeSleuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroMagupeSleuthApplication.class, args);
	}
}
