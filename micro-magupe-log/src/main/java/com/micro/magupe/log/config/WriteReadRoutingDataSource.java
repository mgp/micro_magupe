package com.micro.magupe.log.config;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import com.micro.magupe.common.bean.DataSourceType;
import com.micro.magupe.common.thread.DbContextHolder;

public class WriteReadRoutingDataSource extends AbstractRoutingDataSource{

	@Override
	protected Object determineCurrentLookupKey() {
		String typeKey = DbContextHolder.getJdbcType();
        if (typeKey.equals(DataSourceType.write.getType())) {
        	return DataSourceType.write.getType();
        }
        
        if (typeKey.equals(DataSourceType.read.getType())) {
        	return DataSourceType.read.getType();
        }
        
    	return DataSourceType.read.getType();
	}

}
