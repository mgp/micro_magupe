package com.micro.magupe.log.config;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class DruidDBConfig {
	
    private Logger logger = LoggerFactory.getLogger(DruidDBConfig.class);
    
    @Primary
    @Bean(name="writeDataSource")
    @ConfigurationProperties(prefix = "druid.write")
    public DataSource writeDataSource() {
    	logger.info("Start Init writeDataSource");
        return DataSourceBuilder.create().type(com.alibaba.druid.pool.DruidDataSource.class).build();
    }
    
    @Bean(name="readDataSource")
    @ConfigurationProperties(prefix = "druid.read")
    public DataSource readDataSource() {
    	logger.info("Start Init readDataSource");
    	return DataSourceBuilder.create().type(com.alibaba.druid.pool.DruidDataSource.class).build();
    }
}

