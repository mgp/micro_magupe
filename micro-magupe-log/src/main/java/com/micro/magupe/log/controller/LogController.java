package com.micro.magupe.log.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.micro.magupe.common.bean.Query;
import com.micro.magupe.common.bean.Result;
import com.micro.magupe.common.util.PageUtils;
import com.micro.magupe.common.vo.LogVO;
import com.micro.magupe.log.entity.Log;
import com.micro.magupe.log.service.LogService;

@RequestMapping("/log")
@RestController
public class LogController {

	@Autowired
	LogService logService;
	
	@ResponseBody
	@GetMapping("/list")
	public PageUtils list(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		List<LogVO> logVOs = logService.listQuery(query);
		int total = logService.count(query);
		PageUtils pageUtils = new PageUtils(logVOs, total);
		return pageUtils;
	}
	
	@GetMapping("/save")
	@ResponseBody
	Result save() {
		Log log = new Log();
		log.setIp("127.0.0.1");
		log.setMethod("GET");
		log.setOperation("保存日志");
		log.setParams("");
		log.setUserId("1");
		log.setUsername("admin");
		if (logService.save(log) > 0) {
			return Result.ok();
		}
		
		return Result.error();
	}
}
