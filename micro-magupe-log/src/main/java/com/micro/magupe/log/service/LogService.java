package com.micro.magupe.log.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.micro.magupe.common.bean.Query;
import com.micro.magupe.common.mysql.MicroCrudService;
import com.micro.magupe.common.vo.LogVO;
import com.micro.magupe.log.dao.LogDao;
import com.micro.magupe.log.entity.Log;

@Service
@Transactional(readOnly = true)
public class LogService extends MicroCrudService<LogDao, Log>{

	public Log get(Long id) {
		return super.get(id);
	}

	@Transactional(readOnly = false)
	public int save(Log log) {
		return super.save(log);
	}
	
	@Transactional(readOnly = false)
	public int update(Log content) {
		return super.update(content);
	}

	@Transactional(readOnly = false)
	public int remove(String id) {
		return super.remove(id);
	}

	@Transactional(readOnly = false)
	public int deleteByDelFlag(String id) {
		return super.deleteByDelFlag(id);
	}

	@Transactional(readOnly = false)
	public int batchRemove(String[] ids) {
		return super.batchRemove(ids);
	}
	
	public List<LogVO> list(Log content) {
		return dao.list(content);
	}

	public List<LogVO> listQuery(Query query) {
		return dao.listQuery(query);
	}

	public int count(Query query) {
		return dao.count(query);
	}
}
