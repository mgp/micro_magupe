package com.micro.magupe.log.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.micro.magupe.log.entity.Log;

@Component
public class LogTask {

	@Autowired
	LogService logService;
	
	@Scheduled(cron = "0 25 * * * ?")
	public void task01() {
		Log log = new Log();
		log.setId(111111111111L);
		log.setCreateBy("111111111111");
		log.setCreateTime(new Date());
		log.setModifiedBy("111111111111");
		log.setModifiedTime(new Date());
		logService.save(log);
	}
}
