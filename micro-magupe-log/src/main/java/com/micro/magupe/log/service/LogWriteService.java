package com.micro.magupe.log.service;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.micro.magupe.common.vo.LogVO;
import com.micro.magupe.log.entity.Log;

@Component
public class LogWriteService {

	private Logger logger = LoggerFactory.getLogger(getClass());
    
	@Autowired
	LogService logService;
	
	private Gson gson = new GsonBuilder().create();
	
	//@KafkaListener(topics = {"micro-magupe-visit-log"})
    public void consumerLog(String content){
		LogVO logVO = gson.fromJson(content, LogVO.class);
		Log log = new Log();
		try {
			BeanUtils.copyProperties(log, logVO);
		} catch (IllegalAccessException | InvocationTargetException e) {
			logger.error(e.getMessage());
		} 
		logService.save(log);
    }
}
