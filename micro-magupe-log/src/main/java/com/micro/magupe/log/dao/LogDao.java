package com.micro.magupe.log.dao;

import java.util.List;

import com.micro.magupe.common.annotation.MyBatisDao;
import com.micro.magupe.common.bean.Query;
import com.micro.magupe.common.mysql.MicroCrudDao;
import com.micro.magupe.common.vo.LogVO;
import com.micro.magupe.log.entity.Log;

@MyBatisDao
public interface LogDao extends MicroCrudDao<Log>{

	List<LogVO> list(Log log);

	List<LogVO> listQuery(Query query);
	
	int count(Query query);
}