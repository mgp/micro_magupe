package com.micro.magupe.log.entity;

import com.micro.magupe.common.mysql.MicroDataEntity;

public class Log extends MicroDataEntity<Log> {

	private static final long serialVersionUID = -5328342435199352637L;
	
	private String userId;

	private String username;

	private String operation;

	private Integer time;

	private String method;

	private String params;

	private String ip;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Integer getTime() {
		return time;
	}

	public void setTime(Integer time) {
		this.time = time;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Override
	public String toString() {
		return "Log{" +
				", id='" + id + '\'' +
				", userId='" + userId + '\'' +
				", username='" + username + '\'' +
				", operation='" + operation + '\'' +
				", time='" + time + '\'' +
				", method='" + method + '\'' +
				", params=" + params +
				", ip=" + ip +
				'}';
	}
}
