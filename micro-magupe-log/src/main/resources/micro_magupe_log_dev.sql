SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sys_log_0`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log_0`;
CREATE TABLE `sys_log_0` (
  `id` bigint NOT NULL,
  `user_id` varchar(128) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(128) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(64) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(128) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` varchar(128) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `sys_log_1`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log_1`;
CREATE TABLE `sys_log_1` (
  `id` bigint NOT NULL,
  `user_id` varchar(128) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(128) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(64) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(128) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` varchar(128) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `sys_log_2`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log_2`;
CREATE TABLE `sys_log_2` (
  `id` bigint NOT NULL,
  `user_id` varchar(128) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(128) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(64) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(128) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` varchar(128) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `sys_log_3`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log_3`;
CREATE TABLE `sys_log_3` (
  `id` bigint NOT NULL,
  `user_id` varchar(128) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(128) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(64) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(128) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` varchar(128) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `sys_log_4`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log_4`;
CREATE TABLE `sys_log_4` (
  `id` bigint NOT NULL,
  `user_id` varchar(128) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(128) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(64) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(128) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` varchar(128) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `sys_log_5`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log_5`;
CREATE TABLE `sys_log_5` (
  `id` bigint NOT NULL,
  `user_id` varchar(128) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(128) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(64) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(128) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` varchar(128) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `sys_log_6`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log_6`;
CREATE TABLE `sys_log_6` (
  `id` bigint NOT NULL,
  `user_id` varchar(128) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(128) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(64) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(128) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` varchar(128) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `sys_log_7`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log_7`;
CREATE TABLE `sys_log_7` (
  `id` bigint NOT NULL,
  `user_id` varchar(128) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(128) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(64) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(128) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` varchar(128) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `sys_log_8`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log_8`;
CREATE TABLE `sys_log_8` (
  `id` bigint NOT NULL,
  `user_id` varchar(128) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(128) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(64) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(128) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` varchar(128) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `sys_log_9`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log_9`;
CREATE TABLE `sys_log_9` (
  `id` varchar(128) NOT NULL,
  `user_id` varchar(128) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(128) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(64) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `del_flag` tinyint(4) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` varchar(128) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` varchar(128) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
