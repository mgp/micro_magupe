package com.micro.magupe.sharding.connection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

public abstract class AbstractConnection extends AbstractUnsupportedOperationConnection{

	protected abstract Map<String, DataSource> getDataSourceMap();
	
	public final List<Connection> getConnections(final String dataSourceName, final int connectionSize) throws SQLException {
		DataSource dataSource = getDataSourceMap().get(dataSourceName);
		
		List<Connection> list = new ArrayList<Connection>();
		for (int i = 0; i < connectionSize; i++) {
			list.add(dataSource.getConnection());
		}
		
	    return list;
	}
	
	@Override
	public void setAutoCommit(boolean autoCommit) throws SQLException {
		
	}

	@Override
	public boolean getAutoCommit() throws SQLException {
		return false;
	}

	@Override
	public void commit() throws SQLException {
		
	}

	@Override
	public void rollback() throws SQLException {
		
	}
	
	@Override
	public void close() throws SQLException {
		
	}
	
	@Override
	public boolean isClosed() throws SQLException {
		return false;
	}

	@Override
	public void setReadOnly(boolean readOnly) throws SQLException {
		
	}

	@Override
	public boolean isReadOnly() throws SQLException {
		return false;
	}
	
	@Override
	public void setTransactionIsolation(int level) throws SQLException {
		
	}
	
	@Override
	public int getTransactionIsolation() throws SQLException {
		return 0;
	}
	
    // ------- Consist with MySQL driver implementation -------
    
    @Override
    public final SQLWarning getWarnings() {
        return null;
    }
    
    @Override
    public void clearWarnings() {
    	
    }
    
    @Override
    public final int getHoldability() {
        return ResultSet.CLOSE_CURSORS_AT_COMMIT;
    }
    
    @Override
    public final void setHoldability(final int holdability) {
    	
    }
}
