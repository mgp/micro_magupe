package com.micro.magupe.sharding.executor;

import java.util.List;

public class ShardingExecuteGroup<T> {

	private final List<T> inputs;
	
	public ShardingExecuteGroup(List<T> inputs) {
		this.inputs = inputs;
	}

	public List<T> getInputs() {
		return inputs;
	}
}
