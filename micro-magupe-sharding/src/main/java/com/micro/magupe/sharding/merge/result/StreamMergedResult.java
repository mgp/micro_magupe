package com.micro.magupe.sharding.merge.result;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.Calendar;

public abstract class StreamMergedResult implements MergedResult{

	private QueryResult currentQueryResult;
    
    private boolean wasNull;
    
    public void setCurrentQueryResult(QueryResult currentQueryResult) {
		this.currentQueryResult = currentQueryResult;
	}

	public void setWasNull(boolean wasNull) {
		this.wasNull = wasNull;
	}

	protected final QueryResult getCurrentQueryResult() throws SQLException {
        if (null == currentQueryResult) {
            throw new SQLException("Current ResultSet is null, ResultSet perhaps end of next.");
        }
        return currentQueryResult;
    }
    
    @Override
    public Object getValue(final int columnIndex, final Class<?> type) throws SQLException {
        Object result = getCurrentQueryResult().getValue(columnIndex, type);
        wasNull = getCurrentQueryResult().wasNull();
        return result;
    }
    
    @Override
    public Object getValue(final String columnLabel, final Class<?> type) throws SQLException {
        Object result = getCurrentQueryResult().getValue(columnLabel, type);
        wasNull = getCurrentQueryResult().wasNull();
        return result;
    }
    
    @Override
    public Object getCalendarValue(final int columnIndex, final Class<?> type, final Calendar calendar) throws SQLException {
        Object result = getCurrentQueryResult().getCalendarValue(columnIndex, type, calendar);
        wasNull = getCurrentQueryResult().wasNull();
        return result;
    }
    
    @Override
    public Object getCalendarValue(final String columnLabel, final Class<?> type, final Calendar calendar) throws SQLException {
        Object result = getCurrentQueryResult().getCalendarValue(columnLabel, type, calendar);
        wasNull = getCurrentQueryResult().wasNull();
        return result;
    }
    
    @Override
    public final InputStream getInputStream(final int columnIndex, final String type) throws SQLException {
        InputStream result = getCurrentQueryResult().getInputStream(columnIndex, type);
        wasNull = getCurrentQueryResult().wasNull();
        return result;
    }
    
    @Override
    public final InputStream getInputStream(final String columnLabel, final String type) throws SQLException {
        InputStream result = getCurrentQueryResult().getInputStream(columnLabel, type);
        wasNull = getCurrentQueryResult().wasNull();
        return result;
    }
    
    @Override
    public final boolean wasNull() {
        return wasNull;
    }

}
