package com.micro.magupe.sharding.resultset;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;
import java.util.List;

public abstract class AbstractResultSetAdapter extends AbstractUnsupportedOperationResultSet{

    private final List<ResultSet> resultSets;
    
    private final Statement statement;
    
    private boolean closed;
    
    public AbstractResultSetAdapter(List<ResultSet> resultSets, Statement statement) {
    	this.resultSets = resultSets;	
    	this.statement = statement;	
    }
    
    public List<ResultSet> getResultSets() {
		return resultSets;
	}
    
    public Statement getStatement() {
    	return statement;
    }
    
    @Override
    public final ResultSetMetaData getMetaData() throws SQLException {
    	return resultSets.get(0).getMetaData();
    }
    
    @Override
    public final int getType() throws SQLException {
        return resultSets.get(0).getType();
    }
    
    @Override
    public final int getConcurrency() throws SQLException {
        return resultSets.get(0).getConcurrency();
    }
    
    @Override
    public final SQLWarning getWarnings() throws SQLException {
        return resultSets.get(0).getWarnings();
    }
    
    @Override
    public final int getFetchDirection() throws SQLException {
        return resultSets.get(0).getFetchDirection();
    }
    
    @Override
    public final int getFetchSize() throws SQLException {
        return resultSets.get(0).getFetchSize();
    }
    
    @Override
    public final int findColumn(final String columnLabel) throws SQLException {
    	return resultSets.get(0).findColumn(columnLabel);
    }
    
    @Override
    public final void setFetchSize(final int rows) throws SQLException {
    	for (ResultSet resultSet : resultSets) {
    		resultSet.setFetchSize(rows);
		}
    }
    
    @Override
    public final void clearWarnings() throws SQLException {
    	for (ResultSet resultSet : resultSets) {
    		resultSet.clearWarnings();
		}
    }
    
    @Override
    public final void setFetchDirection(final int direction) throws SQLException {
    	for (ResultSet resultSet : resultSets) {
    		resultSet.setFetchDirection(direction);
		}
    }
    
    @Override
    public final void close() throws SQLException {
    	closed = true;
    	
    	for (ResultSet resultSet : resultSets) {
    		resultSet.close();
		}
    }
    
    @Override
    public final boolean isClosed() {
    	return closed;
    }
}
