package com.micro.magupe.sharding.merge.result;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.Calendar;

public interface QueryResult {

	boolean next() throws SQLException;
    
    int getColumnCount() throws SQLException;
    
    String getColumnLabel(int columnIndex) throws SQLException;
    
    Object getValue(int columnIndex, Class<?> type) throws SQLException;
    
    Object getValue(String columnLabel, Class<?> type) throws SQLException;
    
    Object getCalendarValue(int columnIndex, Class<?> type, Calendar calendar) throws SQLException;
    
    Object getCalendarValue(String columnLabel, Class<?> type, Calendar calendar) throws SQLException;
    
    InputStream getInputStream(int columnIndex, String type) throws SQLException;
    
    InputStream getInputStream(String columnLabel, String type) throws SQLException;

	boolean wasNull() throws SQLException;
}
