package com.micro.magupe.sharding.datasource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import javax.sql.DataSource;

import com.micro.magupe.sharding.connection.ShardingConnection;
import com.micro.magupe.sharding.rule.ShardingRule;


public class ShardingDataSource extends AbstractDataSource{

	private final ShardingRule rule;
	
	public ShardingDataSource(final Map<String, DataSource> dataSourceMap, final ShardingRule rule) throws SQLException {
		super(dataSourceMap);
		this.rule = rule;;
	}
	
	public ShardingRule getRule() {
		return rule;
	}
	
	@Override
	public Connection getConnection() throws SQLException {
		ShardingConnection connection = new ShardingConnection(getDataSourceMap(), rule);
		return connection;
	}

    @Override
    public final void close() {
        super.close();
    }
	
}
