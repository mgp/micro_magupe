package com.micro.magupe.sharding.router;

import java.util.Collection;
import java.util.LinkedHashSet;

import com.micro.magupe.sharding.parser.SQLStatement;

public final class SQLRouterResult {

	private final SQLStatement sqlStatement;
	private final Collection<SQLRouterUnit> routerUnits = new LinkedHashSet<SQLRouterUnit>();
	
    public SQLRouterResult(final SQLStatement sqlStatement) {
        this.sqlStatement = sqlStatement;
    }
    
    public SQLStatement getSqlStatement() {
		return sqlStatement;
	}
    
    public Collection<SQLRouterUnit> getRouterUnits() {
		return routerUnits;
	}
}
