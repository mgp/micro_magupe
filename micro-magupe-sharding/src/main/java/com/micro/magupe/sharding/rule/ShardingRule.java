package com.micro.magupe.sharding.rule;

import java.util.List;
import java.util.ArrayList;

public final class ShardingRule {

	private final ArrayList<String> dbs;
	private final ArrayList<String> tableAndStrategys;
	private final String dbStrategy;
	private final ArrayList<String> tableNames = new ArrayList<String>();
	private final ArrayList<ShardingTableRule> tableRules = new ArrayList<ShardingTableRule>();
	
	public ShardingRule(final ArrayList<String> dbs, final ArrayList<String> tableAndStrategys, final String dbStrategy) {
		this.dbs = dbs;
		this.tableAndStrategys = tableAndStrategys;
		this.dbStrategy = dbStrategy;
		getTableRules(tableAndStrategys);
	}

	public List<String> getDbs() {
		return dbs;
	}

	public List<String> getTableAndStrategys() {
		return tableAndStrategys;
	}

	public String getDbStrategy() {
		return dbStrategy;
	}

	public List<String> getTableNames() {
		return tableNames;
	}

	public List<ShardingTableRule> getTableRules() {
		return tableRules;
	}
	
	private void getTableRules(List<String> tableAndStrategys) {
		tableAndStrategys.forEach((ts) -> {
			String[] splits = ts.split(":");
			tableNames.add(splits[0]);
			ShardingTableRule tableRule = new ShardingTableRule(splits[0], splits[1], splits[2]);
			tableRules.add(tableRule);
		});
	}
}
