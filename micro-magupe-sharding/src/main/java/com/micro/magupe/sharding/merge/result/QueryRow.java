package com.micro.magupe.sharding.merge.result;

import java.util.List;

public final class QueryRow {

	private final List<Object> rowData;
	
	public QueryRow(List<Object> rowData) {
		this.rowData = rowData;
	}
	
	public List<Object> getRowData() {
		return rowData;
	}
	
    public Object getColumnValue(final int columnIndex) {
        return rowData.get(columnIndex - 1);
    }
}
