package com.micro.magupe.sharding.core.util;

import java.lang.reflect.Method;

public class ReflectiveUtil {

    /**
     * Find method using reflect.
     *
     * @param target target object
     * @param methodName method name
     * @param parameterTypes parameter types
     * @return method
     * @throws NoSuchMethodException No such method exception
     */
	@SuppressWarnings("unchecked")
    public static Method findMethod(final Object target, final String methodName, final Class<?>... parameterTypes) throws NoSuchMethodException {
        @SuppressWarnings("rawtypes")
		Class clazz = target.getClass();
        while (null != clazz) {
            try {
                return clazz.getDeclaredMethod(methodName, parameterTypes);
            } catch (NoSuchMethodException ignored) {
            }
            clazz = clazz.getSuperclass();
        }
        throw new NoSuchMethodException(String.format("Cannot find method '%s' in %s", methodName, target.getClass().getName()));
    }
}
