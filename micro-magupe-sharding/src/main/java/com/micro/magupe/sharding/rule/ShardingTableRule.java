package com.micro.magupe.sharding.rule;

public final class ShardingTableRule {

	private final String name;
	private final String counts;
	private final String strategy;
	
	public ShardingTableRule(String name, String counts, String strategy){
		this.name = name;
		this.counts = counts;
		this.strategy = strategy;
	}

	public String getName() {
		return name;
	}

	public String getCounts() {
		return counts;
	}

	public String getStrategy() {
		return strategy;
	}

	@Override
	public String toString() {
		return "ShardingTableRule{" + "name:" + name + ", counts:" + counts + ", strategy:" + strategy + "}";
	}
}
