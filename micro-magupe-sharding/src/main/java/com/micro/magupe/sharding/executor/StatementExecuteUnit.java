package com.micro.magupe.sharding.executor;

import java.sql.SQLException;
import java.sql.Statement;

import com.micro.magupe.sharding.router.SQLRouterUnit;

public class StatementExecuteUnit {

    private final SQLRouterUnit routerUnit;
    
    private final Statement statement;
    
    public StatementExecuteUnit(final SQLRouterUnit routerUnit, final Statement statement) throws SQLException {
        this.routerUnit = routerUnit;
        this.statement = statement;
    }

	public SQLRouterUnit getRouterUnit() {
		return routerUnit;
	}

	public Statement getStatement() {
		return statement;
	}
}
