package com.micro.magupe.sharding.router;

public final class SQLRouterUnit {

    private final String dataSourceName;
    
    private final String sql;
    
    public SQLRouterUnit(String dataSourceName, String sql) {
    	this.dataSourceName = dataSourceName;
    	this.sql = sql;
    }

    public String getDataSourceName() {
		return dataSourceName;
	}
    
    public String getSql() {
		return sql;
	}
    
	@Override
	public String toString() {
		return "SQLRouterUnit{" + "dataSourceName:" + dataSourceName + ", sql:" + sql + "}";
	}
}
