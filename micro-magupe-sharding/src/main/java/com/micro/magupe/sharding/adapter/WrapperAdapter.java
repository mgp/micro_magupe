package com.micro.magupe.sharding.adapter;

import java.sql.SQLException;
import java.sql.Wrapper;

public abstract class WrapperAdapter implements Wrapper{

    @SuppressWarnings("unchecked")
	@Override
    public final <T> T unwrap(final Class<T> iface) throws SQLException {
        if (isWrapperFor(iface)) {
            return (T) this;
        }
        throw new SQLException(String.format("[%s] cannot be unwrapped as [%s]", getClass().getName(), iface.getName()));
    }
    
    @Override
    public final boolean isWrapperFor(final Class<?> iface) {
        return iface.isInstance(this);
    }

}
