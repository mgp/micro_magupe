package com.micro.magupe.sharding.exception;

public class ShardingException extends RuntimeException {

	private static final long serialVersionUID = -1142922354671989372L;

	/**
     * Constructs an exception with formatted error message and arguments. 
     * 
     * @param errorMessage formatted error message
     * @param args arguments of error message
     */
    public ShardingException(final String errorMessage, final Object... args) {
        super(String.format(errorMessage, args));
    }
    
    /**
     * Constructs an exception with error message and cause.
     * 
     * @param message error message
     * @param cause error cause
     */
    public ShardingException(final String message, final Exception cause) {
        super(message, cause);
    }
    
    /**
     * Constructs an exception with cause.
     *
     * @param cause error cause
     */
    public ShardingException(final Exception cause) {
        super(cause);
    }
}
