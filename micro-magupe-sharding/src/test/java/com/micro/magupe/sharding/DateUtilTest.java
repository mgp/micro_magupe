package com.micro.magupe.sharding;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Test;

public class DateUtilTest {

	@Test
	public void test() throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		System.err.println(formatter.parse("2019-01-07 09:02:00").getTime());
		System.err.println(formatter.parse("2019-01-07 09:03:00").getTime());
		
		System.err.println(formatter.parse("2019-01-07 09:02:00").getTime() - formatter.parse("2019-01-07 09:01:00").getTime());
	}
}
